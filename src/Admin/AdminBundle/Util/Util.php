<?php

namespace Admin\AdminBundle\Util;


class Util
{
    private   $context;
    protected $em;
	
    public function __construct($entityManager) 
    {
	    $this->em=$entityManager;
    }    
   
    public function getPeriodo()
    {
       $consulta = $this->em->createQuery('
          SELECT u FROM AdminBundle:Empresa u WHERE u.estado = true');
       // $consulta->setParameter('id', $id);
        $consulta->setMaxResults(1);
        $resultado = $consulta->getResult();
        return $resultado;
    }
}
