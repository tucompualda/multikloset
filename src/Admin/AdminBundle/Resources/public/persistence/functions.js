// Inicializamos la base de datos 
persistence.store.websql.config(persistence, 'AppKloset','base de datos de appkloset', 5 * 1024 * 1024);
online = navigator.onLine;
var escala = 2;

areaLamina = 0;
valorLamina = 0;
valorCm = 0;

altoMaximo = 0;
anchoMaximo = 0;
areaTotalUsada = 0;
valorUsado = 0;
///////////////////// Agregar lámina al dibujo

$('#btnAgregarLamina').click(function(){
    if(confirm("Agregar una lámina borrará los cálculos realizados, ¿Está seguro?")){
       $.borrarTablero();
      var idLamina = $('#materialTablas').val();
      Lamina.findBy(persistence,null,'idBaseDatos',idLamina,function(lamina){
          $('#DivLamina').css({'width': ((lamina.ancho * escala) + 3), 'height': ((lamina.alto * escala) + 3)});
          $('#DivLamina').css('background-color', '#DF7401');
          $('#DivLamina').css('border', '1px solid #FFF');
          $('#areaTotal').text('' + lamina.ancho * lamina.alto + ' cm2 - $...' + lamina.valor);
          areaLamina = parseFloat(lamina.ancho) * parseFloat(lamina.alto);
          altoMaximo = parseFloat(lamina.alto);
          anchoMaximo = parseFloat(lamina.ancho);
          valorLamina = parseFloat(lamina.valor);
          valorCm = parseFloat(lamina.valorCm);

          console.log("Ancho máximo: " + anchoMaximo + ", Alto máximo: " + altoMaximo);
          $('#areaTotal').show();
          dibujarCuadricula(((lamina.ancho * escala) + 3),((lamina.alto * escala) + 3));
      });
    }else{
      return false;
    } 
     
});

$('#limpiarTablero').click(function(){
  if(confirm("¿Deseas eliminar todos los fragmentos?")){
    $.borrarTablero();
  }else{
    return false;
  }
});

$.borrarTablero = function(){
  $('#DivLamina').empty();
    areaLamina = 0;
    valorLamina = 0;
    valorCm = 0;

    altoMaximo = 0;
    anchoMaximo = 0;
    areaTotalUsada = 0;
    valorUsado = 0;
    $('#areaTotalDesperdiciada').text('');
    $('#areaTotalUsada').text('');
    $('#DivLamina').css({'background-color': 'rgb(248, 248, 248)'});
    $('#reglaInferior').empty();
    $('#reglaVertical').empty();
    $('#reglaSuperior').empty();
    $('#reglaVerticalDerecha').empty();
};

$('#DivAgregarCorte').dialog({
      autoOpen: false,
      show: {
        effect: "none",
        duration: 1000
      },
      hide: {
        effect: "none",
        duration: 1000
      },
      position: 
        { my: "left top", at: "right bottom" }
      
    });

$('#formularioAgregar').draggable('disabled');

$( ".fragmentos" ).click(function(){
  $(this).draggable();
});

$( "#AbrirDivAgregar" ).click(function() {
    $( "#DivAgregarCorte" ).dialog( "open" );
});

$( "#formularioAgregar" ).draggable({
   disabled: true
});



$('#btnAgregarNuevaForma').click(function(){
  if(confirm('¿Estás seguro?')){
      agregarNuevaForma();
      $.eventoDragable();

    }else{
      return false;
    }
});

function rotar(control){
    var ancho = $(control).parent().width() + 2;  
    var alto = $(control).parent().height() + 2;  
    
    var value = 0;
    $(control).parent().find('#medida').text(alto/escala +'x'+ancho/escala+'cm');
    $(control).parent().css({'width':alto, 'height': ancho})
    
}

function agregarNuevaForma(){
  $('#btnAgregarNuevaForma').attr('disabled','disabled');
  var ancho = parseFloat($('#anchoNuevo').val());
  var alto =  parseFloat($('#altoNuevo').val());
  var color =  $('#colorNuevo').val();

  console.log('Ancho usado: ' + ancho + ' - ancho máximo: ' + anchoMaximo + ' = Alto usado: ' + alto + ' - alto máximo: ' + altoMaximo + ' valorCm: ' + valorCm.toFixed(2));

  if(ancho <= anchoMaximo && alto <= altoMaximo){    
    areaTotalUsada = areaTotalUsada + alto * ancho;

    if(areaTotalUsada <= areaLamina){

       valorUsado = (valorCm * areaTotalUsada);

       $('#DivLamina').append('<div style="border: solid 1px;height: \n'
        + ((alto*escala)) + 'px;width: ' + ((ancho*escala)) + 'px; background-color: \n' 
        + color + '; position:absolute;top: -20px; left: -20px;" class="draggable ui-widget-content fragmentos">\n'
        +'<a class="btn btn-default btn-xs" style="margin: 3px; cursor: pointer;float:left;" class="myClickDisabledElm" ondblclick="return false;" onClick="rotar(this);return false;" >\n'
        +'<i class="fa fa-repeat fa-2x" ></i></a>\n'
        +'<a class="btn btn-default btn-xs" style="margin: 3px; cursor: pointer;float:right;" class="myClickDisabledElm" \n'
        +'ondblclick="return false;" onClick="elimina(this);return false;">\n'
        +'<i class="fa fa-remove fa-2x" ></i></a>\n'
        +'<div style="margin-bottom: 30%; margin: 5px;" id="medida">'+ancho+'x'+alto+'cm</div>\n'
        +'</div>'); 
       $('#areaTotalUsada').text('' + areaTotalUsada + ' cm2 -- $...' + valorUsado.toFixed(2));
       $('#areaTotalUsada').show();

       if(areaLamina > 0){
         var porcentajeDesperdicio = 100 - (areaTotalUsada/areaLamina)*100;
         var valorDesperdicio = valorLamina - valorUsado;
         $('#areaTotalDesperdiciada').text('' + porcentajeDesperdicio.toFixed(2) + ' % --$...' + valorDesperdicio.toFixed(2));
         $('#areaTotalDesperdiciada').show();
       }
       //$('#DivAgregarCorte').dialog('close');
       $('#btnAgregarNuevaForma').removeAttr('disabled');
    }else{
      areaTotalUsada = areaTotalUsada +  alto * ancho;
      $('#btnAgregarNuevaForma').removeAttr('disabled');
      alert('No puedes agregar mas fragmentos, porque excedes el área de la lámina');
    } 

  }else{
    $('#btnAgregarNuevaForma').removeAttr('disabled');
    if(ancho > anchoMaximo){
      alert("El ancho excede el ancho de la lámina");
      $('#anchoNuevo').focus();
    }
    if(alto > altoMaximo){
      alert("El alto excede el alto de la lámina");
      $('#altoNuevo').focus();
    }
  }  
}

$.eventoDragable = function(){
  $( ".fragmentos" ).draggable(
    { 
      containment: "#DivLamina", 
      scroll: false,
      revert: "valid",
      snap: true,
      snapTolerance: 10
    });
  $( ".fragmentos" ).droppable({
    tolerance: "pointer",
    activeClass: "ui-state-highlight"
  });
}

function elimina(control){
   if(confirm("¿Estás seguro de eliminar este fragmento?")){
        $(control).parent().hide();

        $(control).attr('disabled','disabled');
        
        var alto = parseFloat((($(control).parent().width())/escala) + 1);
        var ancho = parseFloat((($(control).parent().height())/escala) + 1);
         
         areaTotalUsada = parseFloat(areaTotalUsada) -  (alto * ancho);
         valorUsado = valorUsado - (valorCm * alto * ancho);
         
         $('#areaTotalUsada').text('' + areaTotalUsada + ' cm2 --$...' + valorUsado.toFixed(2));
         $('#areaTotalUsada').show();

          if(areaLamina > 0){
             var porcentajeDesperdicio = 100 - (areaTotalUsada/areaLamina)*100;
             var valorDesperdicio = valorLamina - valorUsado;
             $('#areaTotalDesperdiciada').text('' + porcentajeDesperdicio.toFixed(2) + ' % -- $...' + valorDesperdicio.toFixed(2));
             $('#areaTotalDesperdiciada').show();
          }
   }else{
     return false;
   }  
}

function dibujarCuadricula(anchoCuadricula,altoCuadricula){
    var totalEspaciosHorizontales = parseInt((anchoCuadricula/20));
    var totalEspaciosVerticales = parseInt((altoCuadricula/20));
    var celdas = '<table border=0 class="hidden-print" style="border-collapse: collapse; width: 40px; margin-top: 5px; margin-right: 0px;">';

    for(var i=0; i< totalEspaciosVerticales; i++){
      var dato = 10 + i*10;
      celdas += '<tr><td style="float:right;"><span style="font-size: 0.5em">'+ dato +' ___</span></td></tr>';
    }
    
    celdas += '</table>'

    $('#reglaVertical').empty();
    $('#reglaVerticalDerecha').empty();
    $('#reglaInferior').empty();
    $('#reglaSuperior').empty();

    $('#reglaVertical').append(celdas);
    $('#reglaVerticalDerecha').append(celdas);

    var reglaInferior = '<table border=1 style="border-collapse: collapse; margin-top: 0px; width:'+anchoCuadricula+'px; margin-left: 45px;"><tr>';
    for(var i=0; i< totalEspaciosHorizontales; i++){
      var dato = 10 + i*10;
      reglaInferior += '<td border=0 style="width:'+anchoCuadricula/totalEspaciosHorizontales+'px"><span style="font-size: 0.5em; float: right;"> '+ dato +'</span></td>';
    }
    reglaInferior += '</tr></table>';
    $('#reglaInferior').append(reglaInferior);
    $('#reglaSuperior').append(reglaInferior);
}


