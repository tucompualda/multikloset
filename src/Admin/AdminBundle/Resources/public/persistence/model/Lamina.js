var Lamina = persistence.define("Lamina", {
  idBaseDatos: "INTEGER",
  tipoLamina: "TEXT",
  descripcion: "TEXT",
  ancho: "DECIMAL",
  alto: "DECIMAL",
  valor: "DECIMAL",
  valorCm: "DECIMAL"
});

persistence.schemaSync();


function createLamina(IdBaseDatos,TipoLamina,Descripcion,Ancho,Alto,Valor,ValorCm){
  var lamina = new Lamina({
  idBaseDatos: IdBaseDatos,  
  tipoLamina : TipoLamina,
  descripcion: Descripcion,
  ancho : Ancho,
  alto : Alto,
  valor : Valor,
  valorCm : ValorCm
  });
  
  persistence.add(lamina);
  persistence.flush();
}

function DeleteAllLaminas(){
  Lamina.all().filter('id','<>', '').destroyAll(function(){
        persistence.flush();
  });
}

function ConsultarLamina(idLamina){
  var laminaConsultada = {};
  Lamina.findBy(persistence,null,'idBaseDatos',idLamina,function(lamina){
      laminaConsultada = lamina;
  });
  return laminaConsultada;
}