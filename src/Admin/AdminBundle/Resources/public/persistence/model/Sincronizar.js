 
 $.sincronizar = function(ruta){
 
   $.ajax({
	     url: ruta,
	     dataType: "text",
	     success: function(data) {
	         var json = $.parseJSON(data);
	         DeleteAllLaminas();

	         setTimeout(function() {
				for(var i=0 in json){
					var idBaseDatos = json[i]['id'];
					var tipoLamina = json[i]['tipoLamina'];
					var descripcion = json[i]['descripcion'];
					var ancho = json[i]['ancho'];
					var alto = json[i]['alto'];
					var valor = json[i]['valor'];
					var valorCm = json[i]['valorCm'];	

					createLamina(idBaseDatos,tipoLamina,descripcion,ancho,alto,valor,valorCm);
				}
			 }, 2000);		         
	      }
    });

	$.llenarMateriales();   
}

$.llenarMateriales = function(){
	datos = Lamina.all().list(null,function(results){
	   for(var i = 0 in results){
	   		$('#materialTablas').append("<option value='"+results[i]["idBaseDatos"]+"'>" + results[i]["descripcion"] + " -- " +results[i]["tipoLamina"]+ " ("+ results[i]["ancho"] +" x " + results[i]["alto"] +") $..."+ results[i]["valor"] +" Espesor "+  +" </option>");
	   }
	});
}