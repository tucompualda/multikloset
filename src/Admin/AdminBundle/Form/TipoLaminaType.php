<?php

namespace Admin\AdminBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class TipoLaminaType extends AbstractType
{
        /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('tipoLamina', 'choice',array(
                'empty_value'=>'Selecciona un tipo...',
                'choices'=>array('UNA CARA'=>'UNA CARA','DOS CARAS'=>'DOS CARAS','UREA'=>'UREA','FONDO'=>'Fondo','MDF'=>'MDF crudo')
            ))
            ->add('descripcion',null,array(
                'label'=>'Descripción'
            ))
            ->add('alto',null,array(
                'label'=>'Alto (cm)'
            ))
            ->add('ancho',null,array(
                'label'=>'Ancho (cm)'
            ))
            ->add('espesor',null,array(
                'label'=>'Espesor (mm)'
            ))
            ->add('valor',null,array(
                'label'=>'Valor ($)'
            ))
            ->add('isActive',null,array(
                'label'=>'Estado'
            ))
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Admin\AdminBundle\Entity\TipoLamina'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'admin_adminbundle_tipolamina';
    }
}
