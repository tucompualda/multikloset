<?php

namespace Admin\AdminBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Doctrine\ORM\EntityRepository;

class MueblesEditType extends AbstractType
{
        /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('ref',null,array(
                'label'=>'Referencia',
                'attr'=>array('placeholder'=>'Numero de modelo o documento del cliente')
            ))
            ->add('alto',null,array(
                'label'=>'Alto (cm)'
            ))
            ->add('clientes')
            ->add('ancho',null,array(
                'label'=>'Ancho (cm)'
            ))
            ->add('manoObraMetroCuadrado','number',array(
                'label'=>'Valor mano Obra x m2',
                'attr'=>array('type'=>'number')
            ))

            ->add('materialLateral') 
            ->add('flejeLateral')
            ->add('metrosFlejeLateral',null,array(
                'label'=>'Cm Fleje Lateral'
                ))


            ->add('materialTrasera',null,array(
                'label'=>'Material Fondo'
                )) 
            ->add('flejeTrasera',null,array(
                'label'=>'Fleje Fondo'
                ))  
            ->add('metrosFlejeTrasera',null,array(
                'label'=>'Cm Fleje Fondo'
                ))


            ->add('materialFondo',null,array(
                'label'=>'Material Piso'
                ))  
            ->add('flejeFondo',null,array(
                'label'=>'Fleje Piso'
                ))  
            ->add('metrosFlejeFondo',null,array(
                'label'=>'Cm Fleje Piso'
                ))  


            ->add('materialTapa')
            ->add('flejeTapa')     
            ->add('metrosFlejeTapa',null,array(
                'label'=>'Cm Fleje Tapa'
                ))  

            ->add('diVertical',null,array(
                'label'=>'Espacios verticales',
                'attr'=>array('min'=>1,'step'=>1),

            ))                                
            ->add('tipoPuerta','choice',array(
                'choices'=>array('Corrediza'=>'Corrediza','Fija'=>'Fija')
            ))
            ->add('profundidad','number',array(
                'label'=>'Profundidad (cm)'
            ))
            ->add('tipoMueble')

            /**->add('partes','collection',array(
                'type' => new MueblesPartesType(),
                'required' => true,
                'allow_delete'   => true,
                'allow_add'      => true,
                'by_reference'   => false,
                'options'=>array(
                    'required'=> true,
                )
            ))
            ->add('accesorios','collection',array(
                'type' => new MueblesAccesoriosType(),
                'required' => true,
                'allow_delete'   => true,
                'allow_add'      => true,
                'by_reference'   => false,
                'options'=>array(
                    'required'=> true,
                )
            ))**/
            ->add('tipo','choice',array(
                'choices'=>array('Linea'=>'Linea','Especial'=>'Especial'),
                'label'=>'Clase'
            ))
            ->add('estado')
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Admin\AdminBundle\Entity\Muebles'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'admin_adminbundle_muebles_edit';
    }
}
