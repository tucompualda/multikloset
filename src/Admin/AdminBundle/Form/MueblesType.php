<?php

namespace Admin\AdminBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Doctrine\ORM\EntityRepository;

class MueblesType extends AbstractType
{
        /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $self = $this;
        $builder
            ->add('ref',null,array(
                'label'=>'Referencia',
                'attr'=>array('placeholder'=>'Numero de modelo o documento del cliente')
            ))
            ->add('clientes')
            ->add('alto',null,array(
                'label'=>'Alto (cm)'
            ))
            ->add('ancho',null,array(
                'label'=>'Ancho (cm)'
            ))
            ->add('manoObraMetroCuadrado','number',array(
                'label'=>'Valor mano Obra x m2',
                'attr'=>array('type'=>'number')
            ))

            ->add('materialLateral','entity', array(
                'class' => 'AdminBundle:TipoLamina',
                'query_builder' => function(EntityRepository $er) use($self){
                    return $er->CreateQueryBuilder('tl')
                        ->where('tl.isActive = 1');
                },
                'empty_value'=>'Seleccione un material...'
            )) 
            ->add('flejeLateral','entity', array(
                'class' => 'AdminBundle:Fleje',
                'query_builder' => function(EntityRepository $er) use($self){
                    return $er->CreateQueryBuilder('f')
                        ->where('f.estado = 1');
                },
                'empty_value'=>'Seleccione un fleje...',
                'label'=>'Fleje lateral'
            )) 
           

            ->add('materialTrasera','entity', array(
                'class' => 'AdminBundle:TipoLamina',
                'query_builder' => function(EntityRepository $er) use($self){
                    return $er->CreateQueryBuilder('tl')
                        ->where('tl.isActive = 1');
                },
                'empty_value'=>'Seleccione un material...',
                'label'=>'Material fondo'
            )) 
            ->add('flejeTrasera','entity', array(
                'class' => 'AdminBundle:Fleje',
                'query_builder' => function(EntityRepository $er) use($self){
                    return $er->CreateQueryBuilder('f')
                        ->where('f.estado = 1');
                },
                'empty_value'=>'Seleccione un fleje...',
                'label'=>'Fleje fondo'
            )) 
            

            ->add('materialFondo','entity', array(
                'class' => 'AdminBundle:TipoLamina',
                'query_builder' => function(EntityRepository $er) use($self){
                    return $er->CreateQueryBuilder('tl')
                        ->where('tl.isActive = 1');
                },
                'empty_value'=>'Seleccione un material...',
                'label'=>'Material piso'
            )) 
            ->add('flejeFondo','entity', array(
                'class' => 'AdminBundle:Fleje',
                'query_builder' => function(EntityRepository $er) use($self){
                    return $er->CreateQueryBuilder('f')
                        ->where('f.estado = 1');
                },
                'empty_value'=>'Seleccione un fleje...',
                'label'=>'Fleje piso'
            )) 
            


            ->add('materialTapa','entity', array(
                'class' => 'AdminBundle:TipoLamina',
                'query_builder' => function(EntityRepository $er) use($self){
                    return $er->CreateQueryBuilder('tl')
                        ->where('tl.isActive = 1');
                },
                'empty_value'=>'Seleccione un material...',
                'label'=>'Material tapa'
            )) 
            ->add('flejeTapa','entity', array(
                'class' => 'AdminBundle:Fleje',
                'query_builder' => function(EntityRepository $er) use($self){
                    return $er->CreateQueryBuilder('f')
                        ->where('f.estado = 1');
                },
                'empty_value'=>'Seleccione un fleje...',
                'label'=>'Fleje tapa'
            ))      
          

            ->add('diVertical',null,array(
                'label'=>'Espacios verticales',
                'attr'=>array('min'=>1,'step'=>1),

            ))                                
            ->add('tipoPuerta','choice',array(
                'choices'=>array('Corrediza'=>'Corrediza','Abrir'=>'Abrir')
            ))
            ->add('profundidad','number',array(
                'label'=>'Profundidad (cm)'
            ))
            ->add('tipoMueble')

            /**->add('partes','collection',array(
                'type' => new MueblesPartesType(),
                'required' => true,
                'allow_delete'   => true,
                'allow_add'      => true,
                'by_reference'   => false,
                'options'=>array(
                    'required'=> true,
                )
            ))
            ->add('accesorios','collection',array(
                'type' => new MueblesAccesoriosType(),
                'required' => true,
                'allow_delete'   => true,
                'allow_add'      => true,
                'by_reference'   => false,
                'options'=>array(
                    'required'=> true,
                )
            ))**/
            ->add('tipo','choice',array(
                'choices'=>array('Linea'=>'Linea','Especial'=>'Especial'),
                'label'=>'Clase'
            ))
            ->add('estado',null,array(
                'attr'=>array('checked'=>'checked')
            ))
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Admin\AdminBundle\Entity\Muebles'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'admin_adminbundle_muebles';
    }
}
