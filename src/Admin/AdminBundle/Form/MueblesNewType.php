<?php

namespace Admin\AdminBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class MueblesType extends AbstractType
{
        /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('ref')
            ->add('alto')
            ->add('ancho')
            ->add('diVertical')
            ->add('valor')
            ->add('tipoPuerta')
            ->add('profundidad')
            ->add('altoLateralReal')
            ->add('anchoLateralReal')
            ->add('anchoTechoReal')
            ->add('profundidadTechoReal')
            ->add('anchoFondoReal')
            ->add('profundidadFondoReal')
            ->add('anchoTraseraReal')
            ->add('altoTraseraReal')
            ->add('tipo')
            ->add('tipoMueble')
            ->add('materialLateral')
            ->add('materialTrasera')
            ->add('materialFondo')
            ->add('materialTapa')
            ->add('estado')
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Admin\AdminBundle\Entity\Muebles'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'admin_adminbundle_muebles';
    }
}
