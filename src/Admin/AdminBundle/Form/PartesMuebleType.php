<?php

namespace Admin\AdminBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class PartesMuebleType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
             ->add('descripcion')
             ->add('isActive',null,array('label'=>'Estado'))
            ->add('materialFrente',null,array(
                'empty_value'=>false
            ))    
            ->add('anchoFrente')
            ->add('altoFrente')
            ->add('flejeFrente',null,array(
                'empty_value'=>false
            ))    
            //->add('cmFlejeFrente')

            ->add('materialLateral',null,array(
                'empty_value'=>false
            ))
            ->add('anchoLateral')
            ->add('altoLateral') 
            ->add('flejeLateral',null,array(
                'empty_value'=>false
            ))    
            //->add('cmFlejeLateral')

            ->add('materialTrasera',null,array(
                'empty_value'=>false
            ))    
            ->add('anchoTrasera')
            ->add('altoTrasera')
            ->add('flejeTrasera',null,array(
                'empty_value'=>false
            ))    
            //->add('cmFlejeTrasera')

            ->add('materialFondo',null,array(
                'empty_value'=>false
            )) 
            ->add('valorManoObra')

            /**->add('accesorios','collection',array(
                'type' => new accesoriosPartesType(),
                'required'=>true,
                'allow_delete'   => true,
                'allow_add'      => true,
                'by_reference'   => false,
                'options'=>array(
                    'required'=> true,
                )
            ))**/
           
            //->add('fechaCreacion')
            //->add('fechaEdicion')
            ->add('tipoParte')

        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Admin\AdminBundle\Entity\PartesMueble'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'admin_adminbundle_partesmueble';
    }
}
