<?php

namespace Admin\AdminBundle\Twig;
use Symfony\Component\Security\Core\SecurityContext;
use Admin\AdminBundle\Entity\Empresa;
use Admin\AdminBundle\Entity\RepresentanteLegal;
use Admin\AdminBundle\Entity\InfoPago;
use Admin\AdminBundle\Entity\Pagos;
use Symfony\Component\HttpKernel\KernelInterface;


class EmpresaExtension extends \Twig_Extension
{
	private   $context;
	protected $em;
	
        public function __construct($entityManager, $container, SecurityContext $context) 
        {
            $this->em=$entityManager;
            $this->container=$container;
            $this->context = $context;  //,SecurityContext $context		
        }
	
    public function getGlobals()
    {
		
		//$estado = $this->context->getToken()->getUser();
		$datos = "";
        $responsables = "";
        $infoPago = "";
        $pagos = "";
        $pagosTotales = "";

        //if (is_object($usuarioActivo = $this->context->getToken()->getUser())){
		//}
        $datos = $this->em->getRepository('AdminBundle:Empresa')->findOneByEstado(true);  
          
        $responsables = $this->em->getRepository('AdminBundle:RepresentanteLegal')->findBy(array(
                'empresa'=>$datos,
                'estado'=>true
        ));

        $infoPago = $this->em->getRepository('AdminBundle:InfoPago')->findOneByEstado(true);            
        
        $pagosQuery = $this->em->createQuery('
            SELECT p FROM AdminBundle:Pagos p 
            WHERE p.pagoInfo = :infoPago
            ORDER BY p.fechaPago DESC
            ');
        $pagosQuery->setParameter('infoPago',$infoPago);
        $pagosQuery->setMaxResults(1);
        $pagos = $pagosQuery->getResult();

        $pagosTotales = $this->em->getRepository('AdminBundle:Pagos')->findBy(array(
            'pagoInfo'=>$infoPago
        ));

        if($datos == null){
            //$datos = ;
            $datos = new Empresa();
            $datos->setRazonSocial("AppKloset");
        }
        
        if($responsables == null){
            $responsables = new RepresentanteLegal();

        }

        if($infoPago == null){
            $infoPago = new InfoPago();
        }

        if($pagos == null){
            $pagos = new Pagos();
        }

        if($pagosTotales == null){
            $pagosTotales = new Pagos();
        }

        $diaPago = $infoPago->getDia();
        $hoy = new \DateTime();

        $mesActual = date('m');
        $anoActual = date('Y');

        $diaPagoFecha = \DateTime::createFromFormat('d', $diaPago);
        $fechaCal = $diaPagoFecha->format('Y-m-d') . PHP_EOL;

        return array(
            'datos'    => $datos,
            'representantesLegales'=>$responsables,
            'infoPago'=>$infoPago,
            'pagos'=>$pagos,
            'pagosTotales'=>$pagosTotales,
            'fechaPagoProxima'=>$fechaCal
        );


    }
	
	
     public function getName()
    {
        return 'empresa_extension';
    }
}
