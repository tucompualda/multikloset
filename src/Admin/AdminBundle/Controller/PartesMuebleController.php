<?php

namespace Admin\AdminBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Admin\AdminBundle\Entity\PartesMueble;
use Admin\AdminBundle\Form\PartesMuebleType;
use Admin\AdminBundle\Form\PartesMuebleEditType;

/**
 * PartesMueble controller.
 *
 */
class PartesMuebleController extends Controller
{

    /**
     * Lists all PartesMueble entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entitiesQuery = $em->createQuery('SELECT pm FROM AdminBundle:PartesMueble pm
                ORDER BY pm.isActive DESC
            ');
        $entities = $entitiesQuery->getResult();
        return $this->render('AdminBundle:PartesMueble:index.html.twig', array(
            'entities' => $entities,
        ));
    }
    /**
     * Creates a new PartesMueble entity.
     *
     */
    public function createAction(Request $request)
    {
        $entity = new PartesMueble();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);
        $username = $this->get('security.context')->getToken()->getUser();
        $entity->setUsuarioCreador($username);
        $entity->setUsuarioUltimaModificacion($username);
        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();
            $this->get('session')->getFlashBag()->add(
            'notice',
            'Creado correctamente!');
            return $this->redirect($this->generateUrl('partesmueble_show', array('id' => $entity->getId())));
        }

        return $this->render('AdminBundle:PartesMueble:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Creates a form to create a PartesMueble entity.
     *
     * @param PartesMueble $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(PartesMueble $entity)
    {
        $form = $this->createForm(new PartesMuebleType(), $entity, array(
            'action' => $this->generateUrl('partesmueble_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Crear'));

        return $form;
    }

    /**
     * Displays a form to create a new PartesMueble entity.
     *
     */
    public function newAction()
    {
        $entity = new PartesMueble();
        $form   = $this->createCreateForm($entity);

        return $this->render('AdminBundle:PartesMueble:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Finds and displays a PartesMueble entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AdminBundle:PartesMueble')->find($id);

        $accesorios = $em->getRepository('AdminBundle:accesorios')->findAll();

        $accesoriosParte = $em->getRepository('AdminBundle:accesoriosPartes')->findByPartesMueble($entity);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find PartesMueble entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        $valorInicialAccesorios = 0;
        foreach ($accesoriosParte as $key) {
            $valorParcial = $key->getCantidad()*$key->getAccesorios()->getValor();
            $valorInicialAccesorios = $valorInicialAccesorios + $valorParcial;
        }
        $this->get('session')->getFlashBag()->add(
            'notice',
            'Valor accesorios! '.$valorInicialAccesorios);

        $entity->setValorAccesorios($valorInicialAccesorios);
        $em->flush();

        return $this->render('AdminBundle:PartesMueble:show.html.twig', array(
            'accesorios'  => $accesorios,
            'accesoriosPartes'=>$accesoriosParte,
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing PartesMueble entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AdminBundle:PartesMueble')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find PartesMueble entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('AdminBundle:PartesMueble:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
    * Creates a form to edit a PartesMueble entity.
    *
    * @param PartesMueble $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(PartesMueble $entity)
    {
        $form = $this->createForm(new PartesMuebleEditType(), $entity, array(
            'action' => $this->generateUrl('partesmueble_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Actualizar'));

        return $form;
    }
    /**
     * Edits an existing PartesMueble entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AdminBundle:PartesMueble')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find PartesMueble entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);
        $username = $this->get('security.context')->getToken()->getUser();
        $entity->setUsuarioUltimaModificacion($username);
        if ($editForm->isValid()) {
            $em->flush();
            $this->get('session')->getFlashBag()->add(
            'notice',
            'Actualizado correctamente!');
            return $this->redirect($this->generateUrl('partesmueble_edit', array('id' => $id)));
        }

        return $this->render('AdminBundle:PartesMueble:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }
    /**
     * Deletes a PartesMueble entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('AdminBundle:PartesMueble')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find PartesMueble entity.');
            }

            $em->remove($entity);
            $em->flush();
            $this->get('session')->getFlashBag()->add(
            'notice',
            'Borrado correctamente!');
        }

        return $this->redirect($this->generateUrl('partesmueble'));
    }

    /**
     * Creates a form to delete a PartesMueble entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('partesmueble_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Borrar'))
            ->getForm()
        ;
    }
}
