<?php

namespace Admin\AdminBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Admin\AdminBundle\Entity\Muebles;
use Admin\AdminBundle\Form\MueblesType;
use Admin\AdminBundle\Form\MueblesEditType;

/**
 * Muebles controller.
 *
 */
class MueblesController extends Controller
{

    /**
     * Lists all Muebles entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        //$entities = $em->getRepository('AdminBundle:Muebles')->findAll();
        $entitiesQuery = $em->createQuery('SELECT m, t FROM AdminBundle:Muebles m JOIN 
            m.tipoMueble t
            ORDER BY t.ref ASC, m.estado DESC,m.id DESC
        ');
        $entities = $entitiesQuery->getResult();
        return $this->render('AdminBundle:Muebles:index.html.twig', array(
            'entities' => $entities,
        ));
    }
    /**
     * Creates a new Muebles entity.
     *
     */
    public function createAction(Request $request)
    {
        $entity = new Muebles();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);
        
        $username = $this->get('security.context')->getToken()->getUser();
        $entity->setUsuarioCreador($username);
        $entity->setUsuarioUltimaModificacion($username);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();
            $this->get('session')->getFlashBag()->add(
            'notice',
            'Creado correctamente!');
            return $this->redirect($this->generateUrl('muebles_ajuste', array('id' => $entity->getId())));
        }

        return $this->render('AdminBundle:Muebles:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Creates a form to create a Muebles entity.
     *
     * @param Muebles $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Muebles $entity)
    {
        $entity->setDiVertical(1);
        $entity->setManoObraMetroCuadrado(20000);
        $form = $this->createForm(new MueblesType(), $entity, array(
            'action' => $this->generateUrl('muebles_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Crear'));

        return $form;
    }

    /**
     * Displays a form to create a new Muebles entity.
     *
     */
    public function newAction($clientes)
    {
        $em = $this->getDoctrine()->getManager();
        $entity = new Muebles();
        if($clientes==null){

        }else{
            $cliente = $em->getRepository('AdminBundle:Clientes')->find($clientes);
            $entity->setClientes($cliente);
 
        }
        
        $form   = $this->createCreateForm($entity);

        return $this->render('AdminBundle:Muebles:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Finds and displays a Muebles entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AdminBundle:Muebles')->find($id);
        $partes = $em->getRepository('AdminBundle:MueblesPartes')->findPartesTotal($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Muebles entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('AdminBundle:Muebles:show.html.twig', array(
            'partes'    => $partes,
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        ));
    }

       /**
     * Finds and displays a Muebles entity.
     *
     */
    public function ajusteAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AdminBundle:Muebles')->find($id);
        $partes = $em->getRepository('AdminBundle:MueblesPartes')->findPartesTotal($id);
        
        //$partesEstandar = $em->getRepository('AdminBundle:PartesMueble')->findAll();
        $accesorios = $em->getRepository('AdminBundle:Accesorios')->findByIsActive(true);
        $laminas = $em->getRepository('AdminBundle:TipoLamina')->findByIsActive(true);
        $flejes = $em->getRepository('AdminBundle:Fleje')->findByEstado(true);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Muebles entity.');
        }

        $clientes = $em->getRepository('AdminBundle:Clientes')->findAll();
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('AdminBundle:Muebles:ajuste.html.twig', array(
            'accesorios'  => $accesorios,
            'laminas'     => $laminas,
            'clientes'    => $clientes,
            'partes'      => $partes,
            'flejes'      => $flejes,
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        ));
    }
    
    public function clonarAction($mueble)
    {
        $em = $this->getDoctrine()->getManager();
        $username = $this->get('security.context')->getToken()->getUser();
      
        $mueble = $em->getRepository('AdminBundle:Muebles')->find($mueble);
        
        $muebleNuevo = clone $mueble;
        $muebleNuevo->setTipo("Especial");
        $muebleNuevo->setRef($muebleNuevo->getRef()."--COPIA");
        $muebleNuevo->setUsuarioCreador($username);
        $muebleNuevo->setUsuarioUltimaModificacion($username);
        $muebleNuevo->setEstado(false);

        $em->persist($muebleNuevo);
        $em->flush();

        $partes = $em->getRepository('AdminBundle:MueblesPartes')->findBy(
            array('muebles'=>$mueble)
        );
        $accesorios = $em->getRepository('AdminBundle:MueblesAccesorios')->findByMuebles($mueble);

        foreach ($partes as $parte ) {
            $idParte = $parte->getId();
            $parteAClonar = $em->getRepository('AdminBundle:MueblesPartes')->find($idParte);
            $parteNueva = clone $parteAClonar;
            $parteNueva->setMuebles($muebleNuevo);
            $em->persist($parteNueva);
            $em->flush();# code...
        }

        foreach ($accesorios as $accesorio ) {
            $idAccesorio = $accesorio->getId();
            $accesorioAClonar = $em->getRepository('AdminBundle:MueblesAccesorios')->find($idAccesorio);
            $accesorioNuevo = clone $accesorioAClonar;
            $accesorioNuevo->setMuebles($muebleNuevo);
            $em->persist($accesorioNuevo);
            $em->flush();# code...
        }

        $this->get('session')->getFlashBag()->add(
            'notice',
            'Mueble clonado correctamente, con todos sus elementos, por defecto, el Mueble ha quedado en estado deshabilitado!');

        return $this->redirect($this->generateUrl('muebles_edit', array('id' => $muebleNuevo->getId())));
    }
    /**
     * Displays a form to edit an existing Muebles entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AdminBundle:Muebles')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Muebles entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('AdminBundle:Muebles:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
    * Creates a form to edit a Muebles entity.
    *
    * @param Muebles $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(Muebles $entity)
    {
        $form = $this->createForm(new MueblesEditType(), $entity, array(
            'action' => $this->generateUrl('muebles_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Actualizar'));

        return $form;
    }
    /**
     * Edits an existing Muebles entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AdminBundle:Muebles')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Muebles entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);
        $username = $this->get('security.context')->getToken()->getUser();
        $entity->setUsuarioUltimaModificacion($username);
        if ($editForm->isValid()) {
            $em->flush();
            $this->get('session')->getFlashBag()->add(
            'notice',
            'Actualizado correctamente!');

            return $this->redirect($this->generateUrl('muebles_edit', array('id' => $id)));
        }

        return $this->render('AdminBundle:Muebles:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }
    /**
     * Deletes a Muebles entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('AdminBundle:Muebles')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Muebles entity.');
            }

            $em->remove($entity);
            $em->flush();
            $this->get('session')->getFlashBag()->add(
            'notice',
            'Borrado correctamente!');
        }

        return $this->redirect($this->generateUrl('muebles'));
    }

    /**
     * Creates a form to delete a Muebles entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('muebles_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Borrar'))
            ->getForm()
        ;
    }
}
