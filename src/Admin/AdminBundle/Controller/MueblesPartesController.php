<?php

namespace Admin\AdminBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Admin\AdminBundle\Entity\MueblesPartes;
use Admin\AdminBundle\Form\MueblesPartesType;

/**
 * MueblesPartes controller.
 *
 */
class MueblesPartesController extends Controller
{

    /**
     * Lists all MueblesPartes entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('AdminBundle:MueblesPartes')->findAll();

        return $this->render('AdminBundle:MueblesPartes:index.html.twig', array(
            'entities' => $entities,
        ));
    }
    /**
     * Creates a new MueblesPartes entity.
     *
     */
    public function createAction(Request $request)
    {
        $entity = new MueblesPartes();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        $username = $this->get('security.context')->getToken()->getUser();
        $entity->setUsuarioCreador($username);
        $entity->setUsuarioUltimaModificacion($username);
        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();
            $this->get('session')->getFlashBag()->add(
            'notice',
            'Creado correctamente!');
            return $this->redirect($this->generateUrl('mueblespartes_show', array('id' => $entity->getId())));
        }

        return $this->render('AdminBundle:MueblesPartes:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Creates a form to create a MueblesPartes entity.
     *
     * @param MueblesPartes $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(MueblesPartes $entity)
    {
        $form = $this->createForm(new MueblesPartesType(), $entity, array(
            'action' => $this->generateUrl('mueblespartes_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Crear'));

        return $form;
    }

    /**
     * Displays a form to create a new MueblesPartes entity.
     *
     */
    public function newAction()
    {
        $entity = new MueblesPartes();
        $form   = $this->createCreateForm($entity);

        return $this->render('AdminBundle:MueblesPartes:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Finds and displays a MueblesPartes entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AdminBundle:MueblesPartes')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find MueblesPartes entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('AdminBundle:MueblesPartes:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing MueblesPartes entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AdminBundle:MueblesPartes')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find MueblesPartes entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('AdminBundle:MueblesPartes:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
    * Creates a form to edit a MueblesPartes entity.
    *
    * @param MueblesPartes $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(MueblesPartes $entity)
    {
        $form = $this->createForm(new MueblesPartesType(), $entity, array(
            'action' => $this->generateUrl('mueblespartes_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Actualizar'));

        return $form;
    }
    /**
     * Edits an existing MueblesPartes entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AdminBundle:MueblesPartes')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find MueblesPartes entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);
        $username = $this->get('security.context')->getToken()->getUser();
        $entity->setUsuarioUltimaModificacion($username);
        if ($editForm->isValid()) {
            $em->flush();
            $this->get('session')->getFlashBag()->add(
            'notice',
            'Actualizado correctamente!');
            return $this->redirect($this->generateUrl('mueblespartes_edit', array('id' => $id)));
        }

        return $this->render('AdminBundle:MueblesPartes:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }
    /**
     * Deletes a MueblesPartes entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('AdminBundle:MueblesPartes')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find MueblesPartes entity.');
            }

            $em->remove($entity);
            $em->flush();
            $this->get('session')->getFlashBag()->add(
            'notice',
            'Eliminado correctamente!');
        }

        return $this->redirect($this->generateUrl('mueblespartes'));
    }

    /**
     * Creates a form to delete a MueblesPartes entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('mueblespartes_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Borrar'))
            ->getForm()
        ;
    }
}
