<?php

namespace Admin\AdminBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Admin\AdminBundle\Entity\MueblesAccesorios;
use Admin\AdminBundle\Form\MueblesAccesoriosType;

/**
 * MueblesAccesorios controller.
 *
 */
class MueblesAccesoriosController extends Controller
{

    /**
     * Lists all MueblesAccesorios entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('AdminBundle:MueblesAccesorios')->findAll();

        return $this->render('AdminBundle:MueblesAccesorios:index.html.twig', array(
            'entities' => $entities,
        ));
    }
    /**
     * Creates a new MueblesAccesorios entity.
     *
     */
    public function createAction(Request $request)
    {
        $entity = new MueblesAccesorios();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);
        $username = $this->get('security.context')->getToken()->getUser();
        $entity->setUsuarioCreador($username);
        $entity->setUsuarioUltimaModificacion($username);
        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();
            $this->get('session')->getFlashBag()->add(
            'notice',
            'Creado correctamente!');
            return $this->redirect($this->generateUrl('mueblesaccesorios_show', array('id' => $entity->getId())));
        }

        return $this->render('AdminBundle:MueblesAccesorios:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Creates a form to create a MueblesAccesorios entity.
     *
     * @param MueblesAccesorios $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(MueblesAccesorios $entity)
    {
        $form = $this->createForm(new MueblesAccesoriosType(), $entity, array(
            'action' => $this->generateUrl('mueblesaccesorios_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Crear'));

        return $form;
    }

    /**
     * Displays a form to create a new MueblesAccesorios entity.
     *
     */
    public function newAction()
    {
        $entity = new MueblesAccesorios();
        $form   = $this->createCreateForm($entity);

        return $this->render('AdminBundle:MueblesAccesorios:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Finds and displays a MueblesAccesorios entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AdminBundle:MueblesAccesorios')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find MueblesAccesorios entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('AdminBundle:MueblesAccesorios:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing MueblesAccesorios entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AdminBundle:MueblesAccesorios')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find MueblesAccesorios entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('AdminBundle:MueblesAccesorios:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
    * Creates a form to edit a MueblesAccesorios entity.
    *
    * @param MueblesAccesorios $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(MueblesAccesorios $entity)
    {
        $form = $this->createForm(new MueblesAccesoriosType(), $entity, array(
            'action' => $this->generateUrl('mueblesaccesorios_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Actualizar'));

        return $form;
    }
    /**
     * Edits an existing MueblesAccesorios entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AdminBundle:MueblesAccesorios')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find MueblesAccesorios entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);
         $username = $this->get('security.context')->getToken()->getUser();
        $entity->setUsuarioUltimaModificacion($username);
        if ($editForm->isValid()) {
            $em->flush();
            $this->get('session')->getFlashBag()->add(
            'notice',
            'Actualizado!');
            return $this->redirect($this->generateUrl('mueblesaccesorios_edit', array('id' => $id)));
        }

        return $this->render('AdminBundle:MueblesAccesorios:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }
    /**
     * Deletes a MueblesAccesorios entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('AdminBundle:MueblesAccesorios')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find MueblesAccesorios entity.');
            }

            $em->remove($entity);
            $em->flush();
            $this->get('session')->getFlashBag()->add(
            'notice',
            'Eliminado correctamente!');
        }

        return $this->redirect($this->generateUrl('mueblesaccesorios'));
    }

    /**
     * Creates a form to delete a MueblesAccesorios entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('mueblesaccesorios_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Eliminar'))
            ->getForm()
        ;
    }
}
