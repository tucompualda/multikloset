<?php

namespace Admin\AdminBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Admin\AdminBundle\Entity\Clientes;
use Admin\AdminBundle\Form\ClientesType;

/**
 * Clientes controller.
 *
 */
class ClientesDefaultController extends Controller
{
     /**
     * Creates a new Clientes entity.
     *
     */
    public function createAction(Request $request)
    {
        $entity = new Clientes();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();
            $this->get('session')->getFlashBag()->add(
            'notice',
            'Creado correctamente!');
            return $this->redirect($this->generateUrl('muebles_new', array('clientes' => $entity->getId())));
        }

        return $this->render('AdminBundle:ClientesDefault:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Creates a form to create a Clientes entity.
     *
     * @param Clientes $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Clientes $entity)
    {
        $form = $this->createForm(new ClientesType(), $entity, array(
            'action' => $this->generateUrl('clientes_createdefault'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Crear'));

        return $form;
    }

    /**
     * Displays a form to create a new Clientes entity.
     *
     */
    public function newAction()
    {
        $entity = new Clientes();
        $form   = $this->createCreateForm($entity);

        return $this->render('AdminBundle:ClientesDefault:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

  
}
