<?php

namespace Admin\AdminBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Admin\AdminBundle\Entity\TipoLamina;
use Admin\AdminBundle\Form\TipoLaminaType;

/**
 * TipoLamina controller.
 *
 */
class TipoLaminaController extends Controller
{

    /**
     * Lists all TipoLamina entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('AdminBundle:TipoLamina')->findAll();

        return $this->render('AdminBundle:TipoLamina:index.html.twig', array(
            'entities' => $entities,
        ));
    }
    /**
     * Creates a new TipoLamina entity.
     *
     */
    public function createAction(Request $request)
    {
        $entity = new TipoLamina();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);
        $username = $this->get('security.context')->getToken()->getUser();
        $entity->setUsuarioCreador($username);
        $entity->setUsuarioUltimaModificacion($username);
        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();
            $this->get('session')->getFlashBag()->add(
            'notice',
            'Creado correctamente!');
            return $this->redirect($this->generateUrl('tipolamina_show', array('id' => $entity->getId())));
        }

        return $this->render('AdminBundle:TipoLamina:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Creates a form to create a TipoLamina entity.
     *
     * @param TipoLamina $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(TipoLamina $entity)
    {
        $form = $this->createForm(new TipoLaminaType(), $entity, array(
            'action' => $this->generateUrl('tipolamina_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Crear'));

        return $form;
    }

    /**
     * Displays a form to create a new TipoLamina entity.
     *
     */
    public function newAction()
    {
        $entity = new TipoLamina();
        $form   = $this->createCreateForm($entity);

        return $this->render('AdminBundle:TipoLamina:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Finds and displays a TipoLamina entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AdminBundle:TipoLamina')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find TipoLamina entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('AdminBundle:TipoLamina:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing TipoLamina entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AdminBundle:TipoLamina')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find TipoLamina entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('AdminBundle:TipoLamina:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
    * Creates a form to edit a TipoLamina entity.
    *
    * @param TipoLamina $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(TipoLamina $entity)
    {
        $form = $this->createForm(new TipoLaminaType(), $entity, array(
            'action' => $this->generateUrl('tipolamina_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Actualizar'));

        return $form;
    }
    /**
     * Edits an existing TipoLamina entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AdminBundle:TipoLamina')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find TipoLamina entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);
        $username = $this->get('security.context')->getToken()->getUser();
        $entity->setUsuarioUltimaModificacion($username);
        if ($editForm->isValid()) {
            $em->flush();
            $this->get('session')->getFlashBag()->add(
            'notice',
            'Actualizado correctamente!');

            return $this->redirect($this->generateUrl('tipolamina_edit', array('id' => $id)));
        }

        return $this->render('AdminBundle:TipoLamina:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }
    /**
     * Deletes a TipoLamina entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('AdminBundle:TipoLamina')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find TipoLamina entity.');
            }

            $em->remove($entity);
            $em->flush();
             $em->flush();
            $this->get('session')->getFlashBag()->add(
            'notice',
            'Borrado correctamente!');
        }

        return $this->redirect($this->generateUrl('tipolamina'));
    }

    /**
     * Creates a form to delete a TipoLamina entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('tipolamina_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Borrar'))
            ->getForm()
        ;
    }
}
