<?php

namespace Admin\AdminBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Admin\AdminBundle\Entity\accesoriosPartes;
use Admin\AdminBundle\Form\accesoriosPartesType;

/**
 * accesoriosPartes controller.
 *
 */
class accesoriosPartesController extends Controller
{

    /**
     * Lists all accesoriosPartes entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('AdminBundle:accesoriosPartes')->findAll();

        return $this->render('AdminBundle:accesoriosPartes:index.html.twig', array(
            'entities' => $entities,
        ));
    }
    /**
     * Creates a new accesoriosPartes entity.
     *
     */
    public function createAction(Request $request)
    {
        $entity = new accesoriosPartes();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);
        $username = $this->get('security.context')->getToken()->getUser();
        $entity->setUsuarioCreador($username);
        $entity->setUsuarioUltimaModificacion($username);
        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();
            $this->get('session')->getFlashBag()->add(
            'notice',
            'Creado correctamente!');
            return $this->redirect($this->generateUrl('accesoriospartes_show', array('id' => $entity->getId())));
        }

        return $this->render('AdminBundle:accesoriosPartes:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Creates a form to create a accesoriosPartes entity.
     *
     * @param accesoriosPartes $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(accesoriosPartes $entity)
    {
        $form = $this->createForm(new accesoriosPartesType(), $entity, array(
            'action' => $this->generateUrl('accesoriospartes_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Crear'));

        return $form;
    }

    /**
     * Displays a form to create a new accesoriosPartes entity.
     *
     */
    public function newAction()
    {
        $entity = new accesoriosPartes();
        $form   = $this->createCreateForm($entity);

        return $this->render('AdminBundle:accesoriosPartes:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Finds and displays a accesoriosPartes entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AdminBundle:accesoriosPartes')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find accesoriosPartes entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('AdminBundle:accesoriosPartes:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing accesoriosPartes entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AdminBundle:accesoriosPartes')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find accesoriosPartes entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('AdminBundle:accesoriosPartes:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
    * Creates a form to edit a accesoriosPartes entity.
    *
    * @param accesoriosPartes $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(accesoriosPartes $entity)
    {
        $form = $this->createForm(new accesoriosPartesType(), $entity, array(
            'action' => $this->generateUrl('accesoriospartes_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Actualizar'));

        return $form;
    }
    /**
     * Edits an existing accesoriosPartes entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AdminBundle:accesoriosPartes')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find accesoriosPartes entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);
        $username = $this->get('security.context')->getToken()->getUser();
        $entity->setUsuarioUltimaModificacion($username);
        if ($editForm->isValid()) {
            $em->flush();
            $this->get('session')->getFlashBag()->add(
            'notice',
            'Actualizado correctamente!');
            return $this->redirect($this->generateUrl('accesoriospartes_edit', array('id' => $id)));
        }

        return $this->render('AdminBundle:accesoriosPartes:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }
    /**
     * Deletes a accesoriosPartes entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('AdminBundle:accesoriosPartes')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find accesoriosPartes entity.');
            }

            $em->remove($entity);
            $em->flush();
            $this->get('session')->getFlashBag()->add(
            'notice',
            'Borrado correctamente!');
        }

        return $this->redirect($this->generateUrl('accesoriospartes'));
    }

    /**
     * Creates a form to delete a accesoriosPartes entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('accesoriospartes_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Borrar'))
            ->getForm()
        ;
    }
}
