<?php

namespace Admin\AdminBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Admin\AdminBundle\Entity\TipoParte;
use Admin\AdminBundle\Form\TipoParteType;

/**
 * TipoParte controller.
 *
 */
class TipoParteController extends Controller
{

    /**
     * Lists all TipoParte entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('AdminBundle:TipoParte')->findAll();

        return $this->render('AdminBundle:TipoParte:index.html.twig', array(
            'entities' => $entities,
        ));
    }
    /**
     * Creates a new TipoParte entity.
     *
     */
    public function createAction(Request $request)
    {
        $entity = new TipoParte();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);
        $username = $this->get('security.context')->getToken()->getUser();
        $entity->setUsuarioCreador($username);
        $entity->setUsuarioUltimaModificacion($username);
        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();
            $this->get('session')->getFlashBag()->add(
            'notice',
            'Creado correctamente!');
            return $this->redirect($this->generateUrl('tipoparte_show', array('id' => $entity->getId())));
        }

        return $this->render('AdminBundle:TipoParte:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Creates a form to create a TipoParte entity.
     *
     * @param TipoParte $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(TipoParte $entity)
    {
        $form = $this->createForm(new TipoParteType(), $entity, array(
            'action' => $this->generateUrl('tipoparte_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Crear'));

        return $form;
    }

    /**
     * Displays a form to create a new TipoParte entity.
     *
     */
    public function newAction()
    {
        $entity = new TipoParte();
        $form   = $this->createCreateForm($entity);

        return $this->render('AdminBundle:TipoParte:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Finds and displays a TipoParte entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AdminBundle:TipoParte')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find TipoParte entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('AdminBundle:TipoParte:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing TipoParte entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AdminBundle:TipoParte')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find TipoParte entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('AdminBundle:TipoParte:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
    * Creates a form to edit a TipoParte entity.
    *
    * @param TipoParte $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(TipoParte $entity)
    {
        $form = $this->createForm(new TipoParteType(), $entity, array(
            'action' => $this->generateUrl('tipoparte_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Actualizar'));

        return $form;
    }
    /**
     * Edits an existing TipoParte entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AdminBundle:TipoParte')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find TipoParte entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);
         $username = $this->get('security.context')->getToken()->getUser();
        $entity->setUsuarioUltimaModificacion($username);
        if ($editForm->isValid()) {
            $em->flush();
            $this->get('session')->getFlashBag()->add(
            'notice',
            'Actualizado correctamente!');
            return $this->redirect($this->generateUrl('tipoparte_edit', array('id' => $id)));
        }

        return $this->render('AdminBundle:TipoParte:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }
    /**
     * Deletes a TipoParte entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('AdminBundle:TipoParte')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find TipoParte entity.');
            }

            $em->remove($entity);
            $em->flush();
            $this->get('session')->getFlashBag()->add(
            'notice',
            'Borrado correctamente!');
        }

        return $this->redirect($this->generateUrl('tipoparte'));
    }

    /**
     * Creates a form to delete a TipoParte entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('tipoparte_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Borrar'))
            ->getForm()
        ;
    }
}
