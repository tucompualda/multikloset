<?php

namespace Admin\AdminBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Admin\AdminBundle\Entity\TipoMueble;
use Admin\AdminBundle\Form\TipoMuebleType;

/**
 * TipoMueble controller.
 *
 */
class TipoMuebleController extends Controller
{

    /**
     * Lists all TipoMueble entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('AdminBundle:TipoMueble')->findAll();

        return $this->render('AdminBundle:TipoMueble:index.html.twig', array(
            'entities' => $entities,
        ));
    }
    /**
     * Creates a new TipoMueble entity.
     *
     */
    public function createAction(Request $request)
    {
        $entity = new TipoMueble();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);
        $username = $this->get('security.context')->getToken()->getUser();
        $entity->setUsuarioCreador($username);
        $entity->setUsuarioUltimaModificacion($username);
        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();
            $this->get('session')->getFlashBag()->add(
            'notice',
            'Creado correctamente!');
            return $this->redirect($this->generateUrl('tipomueble_show', array('id' => $entity->getId())));
        }

        return $this->render('AdminBundle:TipoMueble:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Creates a form to create a TipoMueble entity.
     *
     * @param TipoMueble $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(TipoMueble $entity)
    {
        $form = $this->createForm(new TipoMuebleType(), $entity, array(
            'action' => $this->generateUrl('tipomueble_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Crear'));

        return $form;
    }

    /**
     * Displays a form to create a new TipoMueble entity.
     *
     */
    public function newAction()
    {
        $entity = new TipoMueble();
        $form   = $this->createCreateForm($entity);

        return $this->render('AdminBundle:TipoMueble:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Finds and displays a TipoMueble entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AdminBundle:TipoMueble')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find TipoMueble entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('AdminBundle:TipoMueble:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing TipoMueble entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AdminBundle:TipoMueble')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find TipoMueble entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('AdminBundle:TipoMueble:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
    * Creates a form to edit a TipoMueble entity.
    *
    * @param TipoMueble $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(TipoMueble $entity)
    {
        $form = $this->createForm(new TipoMuebleType(), $entity, array(
            'action' => $this->generateUrl('tipomueble_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Actualizar'));

        return $form;
    }
    /**
     * Edits an existing TipoMueble entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AdminBundle:TipoMueble')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find TipoMueble entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);
        $username = $this->get('security.context')->getToken()->getUser();
        $entity->setUsuarioUltimaModificacion($username);
        if ($editForm->isValid()) {
            $em->flush();
            $this->get('session')->getFlashBag()->add(
            'notice',
            'Actualizado correctamente!');
            return $this->redirect($this->generateUrl('tipomueble_edit', array('id' => $id)));
        }

        return $this->render('AdminBundle:TipoMueble:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }
    /**
     * Deletes a TipoMueble entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('AdminBundle:TipoMueble')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find TipoMueble entity.');
            }

            $em->remove($entity);
            $em->flush();
            $this->get('session')->getFlashBag()->add(
            'notice',
            'Borrado correctamente!');
        }

        return $this->redirect($this->generateUrl('tipomueble'));
    }

    /**
     * Creates a form to delete a TipoMueble entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('tipomueble_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Borrar'))
            ->getForm()
        ;
    }
}
