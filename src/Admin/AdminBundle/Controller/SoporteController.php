<?php

namespace Admin\AdminBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Admin\AdminBundle\Entity\Soporte;
use Admin\AdminBundle\Entity\Empresa;
use Admin\AdminBundle\Form\SoporteType;

/**
 * Soporte controller.
 *
 */
class SoporteController extends Controller
{

    /**
     * Lists all Soporte entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('AdminBundle:Soporte')->findAll();

        return $this->render('AdminBundle:Soporte:index.html.twig', array(
            'entities' => $entities,
        ));
    }
    /**
     * Creates a new Soporte entity.
     *
     */
    public function createAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $empresa = $em->getRepository('AdminBundle:Empresa')->findOneByEstado(true);
        if($empresa == null){
            $empresa = new Empresa();
        }
        
        $username = $this->get('security.context')->getToken()->getUser();
        $entity = new Soporte();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);
        
        $entity->setEstado(true);
        $entity->setUsuarioCreador($username);
        $entity->setUsuarioEdicion($username);
        $entity->setUsuarioRespuesta($username);
        $entity->setRespuesta("");

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();
            $this->get('session')->getFlashBag()->add(
            'notice',
            'Creado correctamente!');

             $message = \Swift_Message::newInstance()
             ->setSubject('Soporte: '.$entity->getTitulo())
             ->setFrom('app.kloset@tucompualdia.net')
             ->setTo('na-tis23@hotmail.com')
             ->setCc($empresa->getEmail())
             ->setBody('<h1>Título: '.$entity->getTitulo()
                .'</h1><br/>Se ha recibido correctamente la solicitud de soporte No. '.$entity->getId().', <br/>'
                .'por parte del usuario '.$username->getFirstname().' '.$username->getLastName()
                .', <br/>de la empresa: "'.$empresa->getRazonSocial().'"<br/><br/>'
                .'Estaremos atentos a tu solicitud','text/html');
             $this->get('mailer')->send($message);


            return $this->redirect($this->generateUrl('soporte_show', array('id' => $entity->getId())));
        }

        return $this->render('AdminBundle:Soporte:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Creates a form to create a Soporte entity.
     *
     * @param Soporte $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Soporte $entity)
    {
        $form = $this->createForm(new SoporteType(), $entity, array(
            'action' => $this->generateUrl('soporte_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Crear'));

        return $form;
    }

    /**
     * Displays a form to create a new Soporte entity.
     *
     */
    public function newAction()
    {
        $entity = new Soporte();
        $form   = $this->createCreateForm($entity);

        return $this->render('AdminBundle:Soporte:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Finds and displays a Soporte entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AdminBundle:Soporte')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Soporte entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('AdminBundle:Soporte:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing Soporte entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AdminBundle:Soporte')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Soporte entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('AdminBundle:Soporte:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
    * Creates a form to edit a Soporte entity.
    *
    * @param Soporte $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(Soporte $entity)
    {
        $form = $this->createForm(new SoporteType(), $entity, array(
            'action' => $this->generateUrl('soporte_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Editar'));

        return $form;
    }
    /**
     * Edits an existing Soporte entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AdminBundle:Soporte')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Soporte entity.');
        }

        $username = $this->get('security.context')->getToken()->getUser();
        $entity->setEstado(true);
        $entity->setUsuarioEdicion($username);

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();
            $this->get('session')->getFlashBag()->add(
            'notice',
            'Datos actualizados correctamente!');
            return $this->redirect($this->generateUrl('soporte_edit', array('id' => $id)));
        }

        return $this->render('AdminBundle:Soporte:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }
    /**
     * Deletes a Soporte entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('AdminBundle:Soporte')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Soporte entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('soporte'));
    }

    /**
     * Creates a form to delete a Soporte entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('soporte_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Eliminar'))
            ->getForm()
        ;
    }
}
