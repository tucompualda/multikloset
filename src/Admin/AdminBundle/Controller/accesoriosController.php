<?php

namespace Admin\AdminBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Admin\AdminBundle\Entity\accesorios;
use Admin\AdminBundle\Form\accesoriosType;

/**
 * accesorios controller.
 *
 */
class accesoriosController extends Controller
{

    /**
     * Lists all accesorios entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('AdminBundle:accesorios')->findAll();

        return $this->render('AdminBundle:accesorios:index.html.twig', array(
            'entities' => $entities,
        ));
    }
    /**
     * Creates a new accesorios entity.
     *
     */
    public function createAction(Request $request)
    {
        $entity = new accesorios();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);
        $username = $this->get('security.context')->getToken()->getUser();
        $entity->setUsuarioCreador($username);
        $entity->setUsuarioUltimaModificacion($username);
        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();
            $this->get('session')->getFlashBag()->add(
            'notice',
            'Creado correctamente!');
            return $this->redirect($this->generateUrl('accesorios_show', array('id' => $entity->getId())));
        }

        return $this->render('AdminBundle:accesorios:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Creates a form to create a accesorios entity.
     *
     * @param accesorios $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(accesorios $entity)
    {
        $form = $this->createForm(new accesoriosType(), $entity, array(
            'action' => $this->generateUrl('accesorios_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Crear'));

        return $form;
    }

    /**
     * Displays a form to create a new accesorios entity.
     *
     */
    public function newAction()
    {
        $entity = new accesorios();
        $form   = $this->createCreateForm($entity);

        return $this->render('AdminBundle:accesorios:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Finds and displays a accesorios entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AdminBundle:accesorios')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find accesorios entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('AdminBundle:accesorios:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing accesorios entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AdminBundle:accesorios')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find accesorios entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('AdminBundle:accesorios:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
    * Creates a form to edit a accesorios entity.
    *
    * @param accesorios $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(accesorios $entity)
    {
        $form = $this->createForm(new accesoriosType(), $entity, array(
            'action' => $this->generateUrl('accesorios_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Actualizar'));

        return $form;
    }
    /**
     * Edits an existing accesorios entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AdminBundle:accesorios')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find accesorios entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);
         $username = $this->get('security.context')->getToken()->getUser();
        $entity->setUsuarioUltimaModificacion($username);
        if ($editForm->isValid()) {
            $em->flush();
            $this->get('session')->getFlashBag()->add(
            'notice',
            'Actualizado correctamente!');
            return $this->redirect($this->generateUrl('accesorios_edit', array('id' => $id)));
        }

        return $this->render('AdminBundle:accesorios:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }
    /**
     * Deletes a accesorios entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('AdminBundle:accesorios')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find accesorios entity.');
            }

            $em->remove($entity);
            $em->flush();
            $this->get('session')->getFlashBag()->add(
            'notice',
            'Borrado correctamente!');
        }

        return $this->redirect($this->generateUrl('accesorios'));
    }

    /**
     * Creates a form to delete a accesorios entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('accesorios_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Borrar'))
            ->getForm()
        ;
    }
}
