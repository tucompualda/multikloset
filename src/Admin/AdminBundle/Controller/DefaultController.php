<?php

namespace Admin\AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Admin\AdminBundle\Entity\MueblesPartes;
use Admin\AdminBundle\Entity\accesoriosPartes;
use Admin\AdminBundle\Form\EmpresaType;
use Symfony\Component\HttpFoundation\Request;


class DefaultController extends Controller
{
    public function indexAction()
    {
       
        return $this->render('AdminBundle:Default:index.html.twig');
    }

    public function calculadoraAction()
    {
       $em = $this->getDoctrine()->getManager();
       $entity = $em->getRepository('AdminBundle:TipoLamina')->findByIsActive(true);
       
        return $this->render('AdminBundle:Default:calculadora.html.twig',array(
            'entity'=>$entity
          ));
    }
    
    public function inicioAction()
    {
        return $this->render('AdminBundle:Default:inicio.html.twig');
    }

    public function configuracionesAction(Request $request)
    {
       $em = $this->getDoctrine()->getManager();
       $username = $this->get('security.context')->getToken()->getUser();
       $datosEmpresa = $em->getRepository('AdminBundle:Empresa')->findOneByEstado(true);
       $form = $this->createForm(new EmpresaType,$datosEmpresa);
       $datosEmpresa->setUsuarioUltimaModificacion($username);
        if ($request->isMethod('POST')) {
        $form->bind($request);
        //$form = $this->createCreateForm($entity);
        if ($form->isValid()) {

            $em->flush();
            $this->get('session')->getFlashBag()->add(
            'notice',
            'Datos actualizados correctamente!');
            return $this->redirect($this->generateUrl('admin_configuraciones'));
        }
    }

       return $this->render('AdminBundle:Default:configuraciones.html.twig',array(
            'form'=>$form->createView(),
       ));  
    }

    public function actualizarCatalogoAction($tipo)
    {
       $username = $this->get('security.context')->getToken()->getUser();
       $mensaje = "Actualizando....";
       if($tipo == "partes"){
          $em = $this->getDoctrine()->getManager();
          $partes = $em->getRepository('AdminBundle:PartesMueble')->findByIsActive(true);
            foreach ($partes as $parte) {
                $accesoriosParte = $em->getRepository('AdminBundle:accesoriosPartes')->findByPartesMueble($parte);
                $valorInicialAccesorios = 0;
                foreach ($accesoriosParte as $key) {
                    $valorParcial = $key->getCantidad()*$key->getAccesorios()->getValor();
                    $valorInicialAccesorios = $valorInicialAccesorios + $valorParcial;
                } 
                $parte->setValorAccesorios($valorInicialAccesorios);
                $parte->setUsuarioUltimaModificacion($username);
                $parte->calcularFlejes();
                $parte->setValor();
                $em->flush();            
            }  
         $mensaje = "Actualizadas todas las partes Mueble del catálogo...";   
         
       }

       if($tipo == "modelos"){
            
            $muebles = $em->getRepository('AdminBundle:Muebles')->findByEstado(true);
            
            foreach ($muebles as $mueble) {
               $partes = $em->getRepository('AdminBundle:MueblesPartes')->findBy(
                  array('muebles'=>$mueble)
               );
                
                foreach ($partes as $parte) {
                    $parteMueblePadre = $parte->getTipoParte();
                    $accesoriosParte = $em->getRepository('AdminBundle:accesoriosPartes')->findByPartesMueble($parteMueblePadre);
                    $valorInicialAccesorios = 0;
                    foreach ($accesoriosParte as $key) {
                        $valorParcial = $key->getCantidad()*$key->getAccesorios()->getValor();
                        $valorInicialAccesorios = $valorInicialAccesorios + $valorParcial;
                    } 
                    $parte->setValorAccesorios($valorInicialAccesorios);
                    $parte->setUsuarioUltimaModificacion($username);
                    $parte->calcularFlejes();
                    $parte->setValor();
                    $em->flush();            
                }  

              // $accesorios = $em->getRepository('AdminBundle:MueblesAccesorios')->findByMuebles($mueble);
              // foreach ($accesorios as $accesorio) {
                   
              // }
              //$mueble->set
            }

            $mensaje = "Actualizadas todas los modelos de Mueble del catálogo..."; 

       }

       $jsonp = new JsonResponse($mensaje);
       //$jsonp->setCallback('myCallback');
       return $jsonp;
    }

    /**
    * Controlador que lista los tipos de laminas
    */
    public function catalogoAction()
    {
       $em = $this->getDoctrine()->getManager();
       //$entity = $em->getRepository('AdminBundle:TipoLamina')->findAll();
       $entityQuery = $em->createQuery('SELECT t FROM AdminBundle:TipoLamina t WHERE t.isActive = true');
       $entity = $entityQuery->getArrayResult();
       $jsonp = new JsonResponse($entity);
       //$jsonp->setCallback('myCallback');
       return $jsonp;
    }

	/// Web services ///

    /**
    * Controlador que devuelve las partes Muebles y es recogido a través de ajax
    */
    public function PartesAction()
    {
       $em = $this->getDoctrine()->getManager();
       $entity = $em->getRepository('AdminBundle:PartesMueble')->findPartesTotal();
       
       $jsonp = new JsonResponse($entity);
       //$jsonp->setCallback('myCallback');
       return $jsonp;
    }
        


     /**
    * Controlador que devuelve las partes de un Mueble en específico en Json agrupadas
    */
    public function partesMuebleAction($mueble)
    {
       $em = $this->getDoctrine()->getManager();
       $entity = $em->getRepository('AdminBundle:MueblesPartes')->findPartesMueblesTotal($mueble);
       
       $jsonp = new JsonResponse($entity);
       //$jsonp->setCallback('myCallback');
       return $jsonp;
    }
  

     /**
    * Controlador que devuelve las partes de un Mueble en específico en Json sueltas
    */
    public function recargarPartesAction($mueble)
    {
       $em = $this->getDoctrine()->getManager();
       $entity = $em->getRepository('AdminBundle:MueblesPartes')->findPartesTotal($mueble);
       
       $jsonp = new JsonResponse($entity);
       //$jsonp->setCallback('myCallback');
       return $jsonp;
    }
    
      /**
    * Controlador que devuelve borra las partes de un Mueble en específico en Json
    */
    public function partesBorrarAction($parte)
    {
       $em = $this->getDoctrine()->getManager();
       $entity = $em->getRepository('AdminBundle:MueblesPartes')->find($parte);
       $em->remove($entity);
       $em->flush();
       
        $respuesta = "Eliminado";
        $response = new Response($respuesta);
        return $response;
    }
    
    
        /**
    * Controlador que devuelve borra las partes de un Mueble en específico en Json
    */
    public function partesActualizarAction($parte,$positionx,$positiony)
    {
       $em = $this->getDoctrine()->getManager();
       $entity = $em->getRepository('AdminBundle:MueblesPartes')->find($parte);
       $entity->setPositionX($positionx);
       $entity->setPositionY($positiony);
       $em->flush();
       
        $respuesta = "Actualizado";
        $response = new Response($respuesta);
         return $response;
    }
    
         /**
    * Controlador que devuelve borra las partes de un Mueble en específico en Json
    */
    public function actualizarAction($mueble,$ancho,$alto)
    {
       $em = $this->getDoctrine()->getManager();
       $entity = $em->getRepository('AdminBundle:Muebles')->find($mueble);
       $entity->setAncho($ancho);
       $entity->setAlto($alto);
       $em->flush();
       
        $respuesta = "Actualizado el mueble";
        $response = new Response($respuesta);
         return $response;
    }
    
    /**
    * Agregar partes a un Mueble
    */ 
    public function AgregarAction($idmueble,$idparte,$cantidad, $positionx, $positiony)
    {
        $em = $this->getDoctrine()->getManager();
        $muebles = $em->getRepository('AdminBundle:Muebles')->find($idmueble);
        $partesMueble = $em->getRepository('AdminBundle:PartesMueble')->find($idparte);
        
        
        $parte = $partesMueble->getTipoParte();
        $materialFrente = $partesMueble->getMaterialFrente();
        $anchoFrente = $partesMueble->getAnchoFrente();
        $altoFrente = $partesMueble->getAltoFrente();
        $materialLateral = $partesMueble->getMaterialLateral();
        $anchoLateral = $partesMueble->getAnchoLateral();
        $altoLateral = $partesMueble->getAltoLateral();
        $materialTrasera = $partesMueble->getMaterialTrasera();
        $anchoTrasera = $partesMueble->getAnchoTrasera();
        $altoTrasera = $partesMueble->getAltoTrasera();
        $materialFondo = $partesMueble->getMaterialFondo();
        $accesorios = $partesMueble->getAccesorios();
        $descripcion = $partesMueble->getDescripcion();
        $valor = $partesMueble->getValor();

        $valorManoObra = $partesMueble->getValorManoObra();
        $flejeFrente   = $partesMueble->getFlejeFrente();
        $flejeLateral  = $partesMueble->getFlejeLateral();
        $flejeTrasera  = $partesMueble->getFlejeTrasera();

        $cmFlejeFrente = $partesMueble->getCmFlejeFrente();
        $cmFlejeLateral = $partesMueble->getCmFlejeLateral();
       
        $cmFlejeTrasera = $partesMueble->getCmFlejeTrasera();

        $cantidadFrente = $partesMueble->getCantidadFrente();
        $cantidadTrasera= $partesMueble->getCantidadTrasera();
        $cantidadFondo = $partesMueble->getCantidadFondo();
        $cantidadLateral = $partesMueble->getCantidadLateral();

        $valorFrente = $partesMueble->getValorFrente();
        $valorTrasera= $partesMueble->getValorTrasera();
        $valorFondo = $partesMueble->getValorFondo();
        $valorLateral = $partesMueble->getValorLateral();
        
        $valorAccesorios = $partesMueble->getValorAccesorios();

       /** $accesorios2 = $partesMueble->getAccesorios();
        $valorAccesorios=0;
        foreach ($accesorios2 as $accesorios){
            $valor1 = $accesorios->getCantidad();
            $acces1  = $accesorios->getAccesorios()->getValor();
            $valorAccesoriosInicial = $valor1*$acces1;
            $valorAccesorios = $valorAccesorios + $valorAccesoriosInicial;
        }**/



        $insersion = new MueblesPartes();
        $insersion->setMuebles($muebles);
        $insersion->setPartesMueble($partesMueble);
        $insersion->setCantidad($cantidad);
        $insersion->setPositionX($positionx);
        $insersion->setPositionY($positiony);
        
        $insersion->setValor($valor);
        $insersion->setTipoParte($parte);
        $insersion->setMaterialFrente($materialFrente);
        $insersion->setAnchoFrente($anchoFrente);
        $insersion->setAltoFrente($altoFrente);
        $insersion->setMaterialLateral($materialLateral);
        $insersion->setAnchoLateral($anchoLateral);
        $insersion->setAltoLateral($altoLateral);
        $insersion->setMaterialTrasera($materialTrasera);
        $insersion->setAnchoTrasera($anchoTrasera);
        $insersion->setAltoTrasera($altoTrasera);
        $insersion->setMaterialFondo($materialFondo);
        //$insersion->setAccesorios($accesorios);
        $insersion->setDescripcion($descripcion);
        $insersion->setValorManoObra($valorManoObra);
        $insersion->setFlejeFrente($flejeFrente);
        $insersion->setFlejeLateral($flejeLateral);
        $insersion->setFlejeTrasera($flejeTrasera);

        $insersion->setCmFlejeFrente($cmFlejeFrente);
        $insersion->setCmFlejeLateral($cmFlejeLateral);
        $insersion->setCmFlejeTrasera($cmFlejeTrasera);

        $insersion->setCantidadFrente($cantidadFrente);
        $insersion->setCantidadLateral($cantidadLateral);
        $insersion->setCantidadFondo($cantidadFondo);
        $insersion->setCantidadTrasera($cantidadTrasera);

        $insersion->setValorFrente($valorFrente);
        $insersion->setValorLateral($valorLateral);
        $insersion->setValorFondo($valorFondo);
        $insersion->setValorTrasera($valorTrasera);

        $insersion->setValorAccesorios($valorAccesorios);

        $hoy = new \DateTime('now');
        $insersion->setFechaCreacion($hoy);
        $insersion->setFechaEdicion($hoy);
        
        
         $em->persist($insersion);
         
         $em->flush();
            
         $respuesta = $insersion->getId();
         $response = new Response($respuesta);
         return $response;
    }
    

    public function actualizarPartesAction($muebleParte,$ancho,$alto)
    {
        $em = $this->getDoctrine()->getManager();
        $mueblePart = $em->getRepository('AdminBundle:MueblesPartes')->find($muebleParte);

        $deltaAncho = $mueblePart->getAnchoFrente() - $ancho;
        $deltaAlto  = $mueblePart->getAltoFrente() - $alto;        


        if($mueblePart->getAnchoTrasera()==0){

        }else{
          $anchoTraseraNuevo = $mueblePart->getAnchoTrasera() - $deltaAncho;
          $mueblePart->setAnchoTrasera($anchoTraseraNuevo);
        }
                
        if($mueblePart->getAltoTrasera()==0){

        }else{
          $altoTraseraNuevo  = $mueblePart->getAltoTrasera() - $deltaAlto;
          $mueblePart->setAltoTrasera($altoTraseraNuevo);
        }       

        if($mueblePart->getAltoLateral()==0){

        }else{
          $altoLateralNuevo = $mueblePart->getAltoLateral() - $deltaAlto;
          $mueblePart->setAltoLateral($altoLateralNuevo);
        }       

        $mueblePart->setAltoFrente($alto);
        $mueblePart->setAnchoFrente($ancho);
        $em->flush();
        $respuesta = "OK Actualizado, deltaAlto: ".$deltaAlto. " deltaAncho: ".$deltaAncho;
        $response = new Response($respuesta);
        return $response;
    }
   
    
     /**
    * calcula el valor de un  Mueble
    */ 
    public function CalcularMuebleAction($mueble)
    {
        //$valorTotal = 0;
        $em = $this->getDoctrine()->getManager();
        $muebles = $em->getRepository('AdminBundle:Muebles')->find($mueble);
        $valorTotal = $muebles->getValor();
                
        $partes = $em->getRepository('AdminBundle:MueblesPartes')->findValorPartes($mueble);
        
        $valorInicial = 0;  
        $valorParcial = 0;    
        foreach ( $partes as $Valorparte )
        {                
        $valorParcial = $Valorparte["cantidad"]*$Valorparte["valor"]-$Valorparte["valorManoObra"];
        $valorInicial = $valorParcial + $valorInicial;
        }
         
        $accesorios = $em->getRepository('AdminBundle:MueblesAccesorios')->findAccesoriosMueblesTotal($muebles);

        $valorInicialAccesorios = 0;
        foreach ( $accesorios as $accesorio )
        {
        $valorParcialAccesorios = $accesorio["cantidad"]*$accesorio["valor"];
        $valorInicialAccesorios = $valorParcialAccesorios + $valorInicialAccesorios;
        }
        
        //$valortotal = $areaLateral+$areaTecho+$areaTrasera+$areaFondo + $valorInicial + $valorInicialAccesorios; 
        $valortotal = $valorTotal + $valorInicial + $valorInicialAccesorios; 
        
        $muebles->setValorTotal($valortotal);
        $muebles->setValorTotalAccesorios($valorInicialAccesorios);
        $muebles->setValorTotalPartes($valorInicial);
        $em->flush();
        
         $respuesta = "$...".number_format($valortotal,1,",",".");
         $response = new Response($respuesta);
         return $response;
    }

    ////////////////////////////////////////// CONTROLADOR DE ACCESORIOS //////////////////////////////////////////////////
    
     /**
    * Controlador que devuelve los accesorios Muebles y es recogido a través de ajax
    */
    public function accesoriosAction()
    {
       $em = $this->getDoctrine()->getManager();
       $entity = $em->getRepository('AdminBundle:accesorios')->findAccesoriosTotal();
       
       $jsonp = new JsonResponse($entity);
       //$jsonp->setCallback('myCallback');
       return $jsonp;
    }
    
  /**
    * Controlador que devuelve los Accesorios de un Mueble en específico en Json
    */
    public function mueblesAccesoriosAction($mueble)
    {
       $em = $this->getDoctrine()->getManager();
       $entity = $em->getRepository('AdminBundle:MueblesAccesorios')->findAccesoriosMueblesTotal($mueble);
       
       $jsonp = new JsonResponse($entity);
       //$jsonp->setCallback('myCallback');
       return $jsonp;
    }
    
    public function accesoriosInsertarAction($accesorio,$mueble,$cantidad)
    {
        $em = $this->getDoctrine()->getManager();
        $accesorios = $em->getRepository('AdminBundle:accesorios')->find($accesorio);
        $muebles = $em->getRepository('AdminBundle:Muebles')->find($mueble);
        
        $accesoriosMueble = new \Admin\AdminBundle\Entity\MueblesAccesorios();
        $accesoriosMueble->setAccesorios($accesorios);
        $accesoriosMueble->setCantidad($cantidad);
        $accesoriosMueble->setMuebles($muebles);
        
        
        $em->persist($accesoriosMueble);
        $em->flush();
        
         $respuesta = "recibido";
         $response = new Response($respuesta);
         return $response;
    }
    
    ///servicio que calcula el valor de mano de obra de una parteMueble
    public function valorParteMuebleAction($parteMueble)
    {
        $em = $this->getDoctrine()->getManager();
        if($parteMueble==null){
          $valorManoObra = 0;
        }else{
          $partesMueble = $em->getRepository('AdminBundle:TipoParte')->findInfo($parteMueble);
          
        }

        $response = new JsonResponse($partesMueble);
        return $response;
    }

    public function crearAction()
    {
       return $this->render('AdminBundle:Default:crear.html.twig');
    }

 ///////////servicios para crear accesorios para una parte///////////////////////////////

    public function accesorioPartesAction($partemueble,$accesorio,$cantidad)
    {
        $em = $this->getDoctrine()->getManager();
        $parteMueble = $em->getRepository('AdminBundle:PartesMueble')->findOneById($partemueble);
        $accesorios = $em->getRepository('AdminBundle:accesorios')->find($accesorio);
        
        $partesaccesorios = new accesoriosPartes();
        $partesaccesorios->setAccesorios($accesorios);
        $partesaccesorios->setPartesMueble($parteMueble);
        $partesaccesorios->setCantidad($cantidad);

        $em->persist($partesaccesorios);
        $em->flush();

        $accesoriosParteConsulta = $em->getRepository('AdminBundle:accesoriosPartes')->findByPartesMueble($partemueble);

        $valorInicialAccesorios = 0;
        foreach ($accesoriosParteConsulta as $key) {
            $valorParcial = $key->getCantidad()*$key->getAccesorios()->getValor();
            $valorInicialAccesorios = $valorInicialAccesorios + $valorParcial;
        }
        $this->get('session')->getFlashBag()->add(
            'notice',
            'Creado correctamente! '.$valorInicialAccesorios);

       // $partemueble->setValorAccesorios($valorInicialAccesorios);
       // $em->flush();

        $response = new Response($valorInicialAccesorios);
        return $response;
    }

    public function accesorioactualizarPartesAction($accesoriopartemueble,$accesorio,$cantidad)
    {
        $em = $this->getDoctrine()->getManager();

        $accesoriosPartes = $em->getRepository('AdminBundle:accesoriosPartes')->find($accesoriopartemueble);
        $accesorios = $em->getRepository('AdminBundle:accesorios')->find($accesorio);

        $accesoriosPartes ->setAccesorios($accesorios);
        $accesoriosPartes ->setCantidad($cantidad);

        $em->flush();
        $this->get('session')->getFlashBag()->add(
            'notice',
            'Actualizado correctamente!');
        $response = new Response("OK");
        return $response;
    }

    public function eliminaraccesoriosPartesAction($accesoriopartemueble)
    {
        $em = $this->getDoctrine()->getManager();

        $accesoriosPartes = $em->getRepository('AdminBundle:accesoriosPartes')->find($accesoriopartemueble);
       
        $em->remove($accesoriosPartes);
        $em->flush();
        $this->get('session')->getFlashBag()->add(
            'notice',
            'Eliminado el accesorio correctamente!');
        $response = new Response("OK");
        return $response;
    }

     public function eliminaraccesoriosMueblesAction($accesoriomueble)
    {
        $em = $this->getDoctrine()->getManager();

        $accesoriosBorrados = $em->getRepository('AdminBundle:MueblesAccesorios')->find($accesoriomueble);
       
        $em->remove($accesoriosBorrados);
        $em->flush();
       
        $response = new Response("OK");
        return $response;
    }

    /*
    * Metodo q actualiza las partes de un mueble en especifico
    **/
     public function actualizarMueblesPartesAction($parte,$materialfrente,$anchofrente,$altofrente,$materiallateral,$ancholateral,$altolateral,$materialtrasera,$anchotrasera,$altotrasera,$materialfondo,$flejeFrente,$flejeLateral,$flejeTrasera)
    {
        $em = $this->getDoctrine()->getManager();

        
        $materialLateralReal = $em->getRepository('AdminBundle:TipoLamina')->find($materiallateral);
        $materialfrenteReal = $em->getRepository('AdminBundle:TipoLamina')->find($materialfrente);
        $materialTraseraReal = $em->getRepository('AdminBundle:TipoLamina')->find($materialtrasera);        
        $materialfondoReal = $em->getRepository('AdminBundle:TipoLamina')->find($materialfondo);
        
        $flejeFrenteReal = $em->getRepository('AdminBundle:Fleje')->find($flejeFrente);
        $flejeLateralReal = $em->getRepository('AdminBundle:Fleje')->find($flejeLateral);
        $flejeTraseraReal = $em->getRepository('AdminBundle:Fleje')->find($flejeTrasera);

        $muebleParte = $em->getRepository('AdminBundle:MueblesPartes')->find($parte);
        
        $muebleParte->setMaterialFrente($materialfrenteReal);
        $muebleParte->setAnchoFrente($anchofrente);
        $muebleParte->setAltoFrente($altofrente);
        $muebleParte->setMaterialLateral($materialLateralReal);
        $muebleParte->setAnchoLateral($ancholateral);
        $muebleParte->setAltoLateral($altolateral);
        $muebleParte->setMaterialTrasera($materialTraseraReal);
        $muebleParte->setAnchoTrasera($anchotrasera);
        $muebleParte->setAltoTrasera($altotrasera);
        $muebleParte->setMaterialFondo($materialfondoReal);
        $muebleParte->setFlejeFrente($flejeFrenteReal);
        $muebleParte->setFlejeLateral($flejeLateralReal);
        $muebleParte->setFlejeTrasera($flejeTraseraReal);
        
        $em->flush();
        $valor = '($...'.number_format($muebleParte->getValor(),1,",",".").')';
        $response = new Response($valor);
        return $response;
    }

    public function duplicarAction($parte)
    {
        $em = $this->getDoctrine()->getManager();
        $parte = $em->getRepository('AdminBundle:MueblesPartes')->find($parte);
        
        $parteCopiada = clone $parte;
        $em->persist($parteCopiada);
        $em->flush();
        $response = new Response($parteCopiada->getId());
        return $response;
    }

     public function consultarParteAction($partemueble)
    {
        $em = $this->getDoctrine()->getManager();
        $parte = $em->getRepository('AdminBundle:MueblesPartes')->find($partemueble);
        $parteQuery = $em->createQuery('SELECT pm.id,pm.descripcion,tl.id as materialFrente,pm.anchoFrente,pm.altoFrente, ml.id as materialLateral, 
              pm.anchoLateral, pm.altoLateral, mt.id as materialTrasera, pm.anchoTrasera, pm.altoTrasera,mf.id as materialFondo,
              ff.id as idFlejeFrente, fl.id as idFlejeLateral, ft.id as idFlejeTrasera 
              FROM AdminBundle:MueblesPartes pm 
              JOIN pm.materialFrente tl JOIN pm.materialLateral ml JOIN pm.materialTrasera mt JOIN pm.materialFondo mf
              JOIN pm.flejeFrente ff JOIN pm.flejeLateral fl JOIN pm.flejeTrasera ft
              WHERE pm.id = :partemueble
          ');
        $parteQuery->setParameter("partemueble",$partemueble);
        $parte = $parteQuery->getArrayResult();
        $response = new JsonResponse($parte);
        return $response;
    }
}
