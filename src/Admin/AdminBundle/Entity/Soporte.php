<?php

namespace Admin\AdminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Soporte
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Admin\AdminBundle\Entity\SoporteRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class Soporte
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="titulo", type="string", length=255)
     */
    private $titulo;

    /**
     * @var string
     *
     * @ORM\Column(name="descripcion", type="text")
     */
    private $descripcion;

    /**
     * @var string
     *
     * @ORM\Column(name="como", type="text")
     */
    private $como;

    /**
     * @var string
     *
     * @ORM\Column(name="deseo", type="text")
     */
    private $deseo;

    /**
     * @var string
     *
     * @ORM\Column(name="para", type="text")
     */
    private $para;

    /**
     * @var string
     *
     * @ORM\ManyToOne(targetEntity="Twinpeaks\UserBundle\Entity\User")
     */
    private $usuarioCreador;

    /**
     * @var string
     *
     * @ORM\ManyToOne(targetEntity="Twinpeaks\UserBundle\Entity\User")
     */
    private $usuarioEdicion;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fechaCreacion", type="datetime")
     */
    private $fechaCreacion;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fechaEdicion", type="datetime")
     */
    private $fechaEdicion;

    /**
     * @var boolean
     *
     * @ORM\Column(name="estado", type="boolean")
     */
    private $estado;

    /**
     * @var string
     *
     * @ORM\ManyToOne(targetEntity="Twinpeaks\UserBundle\Entity\User")
     */
    private $usuarioRespuesta;

    /**
     * @var string
     *
     * @ORM\Column(name="respuesta", type="text")
     */
    private $respuesta;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set titulo
     *
     * @param string $titulo
     * @return Soporte
     */
    public function setTitulo($titulo)
    {
        $this->titulo = $titulo;

        return $this;
    }

    /**
     * Get titulo
     *
     * @return string 
     */
    public function getTitulo()
    {
        return $this->titulo;
    }

    /**
     * Set descripcion
     *
     * @param string $descripcion
     * @return Soporte
     */
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    /**
     * Get descripcion
     *
     * @return string 
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }

    /**
     * Set como
     *
     * @param string $como
     * @return Soporte
     */
    public function setComo($como)
    {
        $this->como = $como;

        return $this;
    }

    /**
     * Get como
     *
     * @return string 
     */
    public function getComo()
    {
        return $this->como;
    }

    /**
     * Set deseo
     *
     * @param string $deseo
     * @return Soporte
     */
    public function setDeseo($deseo)
    {
        $this->deseo = $deseo;

        return $this;
    }

    /**
     * Get deseo
     *
     * @return string 
     */
    public function getDeseo()
    {
        return $this->deseo;
    }

    /**
     * Set para
     *
     * @param string $para
     * @return Soporte
     */
    public function setPara($para)
    {
        $this->para = $para;

        return $this;
    }

    /**
     * Get para
     *
     * @return string 
     */
    public function getPara()
    {
        return $this->para;
    }

 
    /**
     * Set fechaCreacion
     *
     * @param \DateTime $fechaCreacion
     * @return Soporte
     */
    public function setFechaCreacion($fechaCreacion)
    {
        $this->fechaCreacion = $fechaCreacion;

        return $this;
    }

    /**
     * Get fechaCreacion
     *
     * @return \DateTime 
     */
    public function getFechaCreacion()
    {
        return $this->fechaCreacion;
    }

    /**
     * Set fechaEdicion
     *
     * @param \DateTime $fechaEdicion
     * @return Soporte
     */
    public function setFechaEdicion($fechaEdicion)
    {
        $this->fechaEdicion = $fechaEdicion;

        return $this;
    }

    /**
     * Get fechaEdicion
     *
     * @return \DateTime 
     */
    public function getFechaEdicion()
    {
        return $this->fechaEdicion;
    }

    /**
     * Set estado
     *
     * @param boolean $estado
     * @return Soporte
     */
    public function setEstado($estado)
    {
        $this->estado = $estado;

        return $this;
    }

    /**
     * Get estado
     *
     * @return boolean 
     */
    public function getEstado()
    {
        return $this->estado;
    }

    /**
     * Set respuesta
     *
     * @param string $respuesta
     * @return Soporte
     */
    public function setRespuesta($respuesta)
    {
        $this->respuesta = $respuesta;

        return $this;
    }

    /**
     * Get respuesta
     *
     * @return string 
     */
    public function getRespuesta()
    {
        return $this->respuesta;
    }

    /**
     * Set usuarioCreador
     *
     * @param \Twinpeaks\UserBundle\Entity\User $usuarioCreador
     * @return Soporte
     */
    public function setUsuarioCreador(\Twinpeaks\UserBundle\Entity\User $usuarioCreador = null)
    {
        $this->usuarioCreador = $usuarioCreador;

        return $this;
    }

    /**
     * Get usuarioCreador
     *
     * @return \Twinpeaks\UserBundle\Entity\User 
     */
    public function getUsuarioCreador()
    {
        return $this->usuarioCreador;
    }

    /**
     * Set usuarioEdicion
     *
     * @param \Twinpeaks\UserBundle\Entity\User $usuarioEdicion
     * @return Soporte
     */
    public function setUsuarioEdicion(\Twinpeaks\UserBundle\Entity\User $usuarioEdicion = null)
    {
        $this->usuarioEdicion = $usuarioEdicion;

        return $this;
    }

    /**
     * Get usuarioEdicion
     *
     * @return \Twinpeaks\UserBundle\Entity\User 
     */
    public function getUsuarioEdicion()
    {
        return $this->usuarioEdicion;
    }

    /**
     * Set usuarioRespuesta
     *
     * @param \Twinpeaks\UserBundle\Entity\User $usuarioRespuesta
     * @return Soporte
     */
    public function setUsuarioRespuesta(\Twinpeaks\UserBundle\Entity\User $usuarioRespuesta = null)
    {
        $this->usuarioRespuesta = $usuarioRespuesta;

        return $this;
    }

    /**
     * Get usuarioRespuesta
     *
     * @return \Twinpeaks\UserBundle\Entity\User 
     */
    public function getUsuarioRespuesta()
    {
        return $this->usuarioRespuesta;
    }

      /**
     * @ORM\PrePersist
     */
    public function setCreatedValue()
    {
        $this->setFechaCreacion(new \DateTime());
        $this->setFechaEdicion(new \DateTime());
    }

    /**
     * @ORM\PreUpdate
     */
    public function setUpdateValue()
    {
        $this->setFechaEdicion(new \DateTime());
    }

    public function __toString()
    {
        return $this->setTitulo();
    }
}
