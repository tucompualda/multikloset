<?php

namespace Admin\AdminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Pagos
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Admin\AdminBundle\Entity\PagosRepository")
 */
class Pagos
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\ManyToOne(targetEntity="Admin\AdminBundle\Entity\InfoPago")
     */
    private $pagoInfo;

    /**
     * @var string
     *
     * @ORM\ManyToOne(targetEntity="Admin\AdminBundle\Entity\Empresa")
     */
    private $empresa;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fechaPago", type="datetime")
     */
    private $fechaPago;

    /**
     * @var integer
     *
     * @ORM\Column(name="valor", type="integer")
     */
    private $valor;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set fechaPago
     *
     * @param \DateTime $fechaPago
     * @return Pagos
     */
    public function setFechaPago($fechaPago)
    {
        $this->fechaPago = $fechaPago;

        return $this;
    }

    /**
     * Get fechaPago
     *
     * @return \DateTime 
     */
    public function getFechaPago()
    {
        return $this->fechaPago;
    }

    /**
     * Set valor
     *
     * @param integer $valor
     * @return Pagos
     */
    public function setValor($valor)
    {
        $this->valor = $valor;

        return $this;
    }

    /**
     * Get valor
     *
     * @return integer 
     */
    public function getValor()
    {
        return $this->valor;
    }

    /**
     * Set pagoInfo
     *
     * @param \Admin\AdminBundle\Entity\InfoPago $pagoInfo
     * @return Pagos
     */
    public function setPagoInfo(\Admin\AdminBundle\Entity\InfoPago $pagoInfo = null)
    {
        $this->pagoInfo = $pagoInfo;

        return $this;
    }

    /**
     * Get pagoInfo
     *
     * @return \Admin\AdminBundle\Entity\InfoPago 
     */
    public function getPagoInfo()
    {
        return $this->pagoInfo;
    }

    /**
     * Set empresa
     *
     * @param \Admin\AdminBundle\Entity\Empresa $empresa
     * @return Pagos
     */
    public function setEmpresa(\Admin\AdminBundle\Entity\Empresa $empresa = null)
    {
        $this->empresa = $empresa;

        return $this;
    }

    /**
     * Get empresa
     *
     * @return \Admin\AdminBundle\Entity\Empresa 
     */
    public function getEmpresa()
    {
        return $this->empresa;
    }

    public function __toString()
    {
        return "$...".$this->getValor()."";
    }
}
