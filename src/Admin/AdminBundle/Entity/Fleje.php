<?php

namespace Admin\AdminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Fleje
 *
 * @ORM\Table()
 * @ORM\HasLifecycleCallbacks()
 * @ORM\Entity(repositoryClass="Admin\AdminBundle\Entity\FlejeRepository")
 */
class Fleje
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nombre", type="string", length=255)
     */
    private $nombre;

    /**
     * @var float
     *
     * @ORM\Column(name="espesor", type="float")
     */
    private $espesor;

    /**
     * @var float
     *
     * @ORM\Column(name="metros", type="float")
     */
    private $metros;

   /**
     * @var float
     *
     * @ORM\Column(name="valor", type="float")
     */
    private $valor;


    /**
     * @var float
     *
     * @ORM\Column(name="valormetroslineal", type="float", nullable=true)
     */
    private $valorMetrosLineal;

    /**
     * @var boolean
     *
     * @ORM\Column(name="estado", type="boolean", nullable=true)
     */
    private $estado;

    /**
    * @var datetime
    * @ORM\Column(name="fechacreacion", type="datetime", nullable=true)
    */
    private $fechaCreacion;

    /**
    * @var string
    *
    * @ORM\ManyToOne(targetEntity="Twinpeaks\UserBundle\Entity\User")
    */
    private $usuarioCreador;

    /**
    * @var string
    *
    * @ORM\ManyToOne(targetEntity="Twinpeaks\UserBundle\Entity\User")
    */
    private $UsuarioUltimaModificacion;

   /**
    * @var datetime
    * @ORM\Column(name="fechaultimaedicion", type="datetime", nullable=true)
    */
    private $fechaUltimaEdicion;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     * @return Fleje
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string 
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set espesor
     *
     * @param float $espesor
     * @return Fleje
     */
    public function setEspesor($espesor)
    {
        $this->espesor = $espesor;

        return $this;
    }

    /**
     * Get espesor
     *
     * @return float 
     */
    public function getEspesor()
    {
        return $this->espesor;
    }

    /**
     * Set metros
     *
     * @param float $metros
     * @return Fleje
     */
    public function setMetros($metros)
    {
        $this->metros = $metros;

        return $this;
    }

    /**
     * Get metros
     *
     * @return float 
     */
    public function getMetros()
    {
        return $this->metros;
    }

    /**
     * Set estado
     *
     * @param boolean $estado
     * @return Fleje
     */
    public function setEstado($estado)
    {
        $this->estado = $estado;

        return $this;
    }

    /**
     * Get estado
     *
     * @return boolean 
     */
    public function getEstado()
    {
        return $this->estado;
    }



    /**
     * Set valor
     *
     * @param float $valor
     * @return Fleje
     */
    public function setValor($valor)
    {
        $this->valor = $valor;

        return $this;
    }

    /**
     * Get valor
     *
     * @return float 
     */
    public function getValor()
    {
        return $this->valor;
    }

    public function __toString() 
    {
        return $this->getNombre();
    }

    /**
    * @ORM\PrePersist
    * @ORM\preUpdate
    */
    public function calcular()
    {
        $this->setValorMetrosLineal($this->getValor()/$this->getMetros());
    }

    /**
     * Set valorMetrosLineal
     *
     * @param float $valorMetrosLineal
     * @return Fleje
     */
    public function setValorMetrosLineal($valorMetrosLineal)
    {
        $this->valorMetrosLineal = $valorMetrosLineal;

        return $this;
    }

    /**
     * Get valorMetrosLineal
     *
     * @return float 
     */
    public function getValorMetrosLineal()
    {
        return $this->valorMetrosLineal;
    }

    /**
     * Set fechaCreacion
     *
     * @param \DateTime $fechaCreacion
     * @return Fleje
     */
    public function setFechaCreacion($fechaCreacion)
    {
        $this->fechaCreacion = $fechaCreacion;

        return $this;
    }

    /**
     * Get fechaCreacion
     *
     * @return \DateTime 
     */
    public function getFechaCreacion()
    {
        return $this->fechaCreacion;
    }

    /**
     * Set fechaUltimaEdicion
     *
     * @param \DateTime $fechaUltimaEdicion
     * @return Fleje
     */
    public function setFechaUltimaEdicion($fechaUltimaEdicion)
    {
        $this->fechaUltimaEdicion = $fechaUltimaEdicion;

        return $this;
    }

    /**
     * Get fechaUltimaEdicion
     *
     * @return \DateTime 
     */
    public function getFechaUltimaEdicion()
    {
        return $this->fechaUltimaEdicion;
    }

    /**
     * Set usuarioCreador
     *
     * @param \Twinpeaks\UserBundle\Entity\User $usuarioCreador
     * @return Fleje
     */
    public function setUsuarioCreador(\Twinpeaks\UserBundle\Entity\User $usuarioCreador = null)
    {
        $this->usuarioCreador = $usuarioCreador;

        return $this;
    }

    /**
     * Get usuarioCreador
     *
     * @return \Twinpeaks\UserBundle\Entity\User 
     */
    public function getUsuarioCreador()
    {
        return $this->usuarioCreador;
    }

    /**
     * Set UsuarioUltimaModificacion
     *
     * @param \Twinpeaks\UserBundle\Entity\User $usuarioUltimaModificacion
     * @return Fleje
     */
    public function setUsuarioUltimaModificacion(\Twinpeaks\UserBundle\Entity\User $usuarioUltimaModificacion = null)
    {
        $this->UsuarioUltimaModificacion = $usuarioUltimaModificacion;

        return $this;
    }

    /**
     * Get UsuarioUltimaModificacion
     *
     * @return \Twinpeaks\UserBundle\Entity\User 
     */
    public function getUsuarioUltimaModificacion()
    {
        return $this->UsuarioUltimaModificacion;
    }

      /**
     * @ORM\PrePersist
     */
    public function setCreatedValue()
    {
        $this->setFechaCreacion(new \DateTime());
        $this->setFechaUltimaEdicion(new \DateTime());
    }

    /**
     * @ORM\PreUpdate
     */
    public function setUpdateValue()
    {
        $this->setFechaUltimaEdicion(new \DateTime());
    }
}
