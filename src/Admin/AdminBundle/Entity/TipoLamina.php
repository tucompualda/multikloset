<?php

namespace Admin\AdminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * TipoLamina
 *
 * @ORM\Table()
 * @ORM\HasLifecycleCallbacks()
 * @ORM\Entity(repositoryClass="Admin\AdminBundle\Entity\TipoLaminaRepository")
 */
class TipoLamina
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="tipoLamina", type="string", length=255)
     */
    private $tipoLamina;

    /**
     * @var string
     *
     * @ORM\Column(name="descripcion", type="string", length=100, nullable=true)
     */
    private $descripcion;

    /**
     * @var float
     *
     * @ORM\Column(name="alto", type="float")
     */
    private $alto;

    /**
     * @var float
     *
     * @ORM\Column(name="ancho", type="float")
     */
    private $ancho;

    /**
     * @var float
     *
     * @ORM\Column(name="espesor", type="float")
     */
    private $espesor;

    /**
     * @var float
     *
     * @ORM\Column(name="valor", type="float")
     */
    private $valor;

    /**
     * @var float
     *
     * @ORM\Column(name="area", type="float")
     */
    private $area;

    
    /**
     * @var float
     *
     * @ORM\Column(name="valorcm", type="float")
     */
    private $valorCm;

    /**
     * @var boolean
     *
     * @ORM\Column(name="isActive", type="boolean", nullable=true)
     */
    private $isActive = true;

  /**
    * @var datetime
    * @ORM\Column(name="fechacreacion", type="datetime", nullable=true)
    */
    private $fechaCreacion;

    /**
    * @var string
    *
    * @ORM\ManyToOne(targetEntity="Twinpeaks\UserBundle\Entity\User")
    */
    private $usuarioCreador;

    /**
    * @var string
    *
    * @ORM\ManyToOne(targetEntity="Twinpeaks\UserBundle\Entity\User")
    */
    private $UsuarioUltimaModificacion;

   /**
    * @var datetime
    * @ORM\Column(name="fechaultimaedicion", type="datetime", nullable=true)
    */
    private $fechaUltimaEdicion;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set tipoLamina
     *
     * @param string $tipoLamina
     * @return TipoLamina
     */
    public function setTipoLamina($tipoLamina)
    {
        $this->tipoLamina = $tipoLamina;

        return $this;
    }

    /**
     * Get tipoLamina
     *
     * @return string 
     */
    public function getTipoLamina()
    {
        return $this->tipoLamina;
    }

    /**
     * Set alto
     *
     * @param float $alto
     * @return TipoLamina
     */
    public function setAlto($alto)
    {
        $this->alto = $alto;

        return $this;
    }

    /**
     * Get alto
     *
     * @return float 
     */
    public function getAlto()
    {
        return $this->alto;
    }

    /**
     * Set ancho
     *
     * @param float $ancho
     * @return TipoLamina
     */
    public function setAncho($ancho)
    {
        $this->ancho = $ancho;

        return $this;
    }

    /**
     * Get ancho
     *
     * @return float 
     */
    public function getAncho()
    {
        return $this->ancho;
    }

    /**
     * Set espesor
     *
     * @param float $espesor
     * @return TipoLamina
     */
    public function setEspesor($espesor)
    {
        $this->espesor = $espesor;

        return $this;
    }

    /**
     * Get espesor
     *
     * @return float 
     */
    public function getEspesor()
    {
        return $this->espesor;
    }

    /**
     * Set valor
     *
     * @param float $valor
     * @return TipoLamina
     */
    public function setValor($valor)
    {
        $this->valor = $valor;

        return $this;
    }

    /**
     * Get valor
     *
     * @return float 
     */
    public function getValor()
    {
        return $this->valor;
    }

    /**
     * Set area
     *
     * @param float $area
     * @return TipoLamina
     */
    public function setArea($area)
    {
        $this->area = $area;

        return $this;
    }

    /**
     * Get area
     *
     * @return float 
     */
    public function getArea()
    {
        return $this->area;
    }
    
    public function __toString() {
        return $this->getDescripcion()." - ".$this->getTipoLamina()." (".$this->getAncho()."x".$this->getAlto().") $...".$this->getValor()." Espesor: ".$this->getEspesor();
    }

    /**
     * Set valorCm
     *
     * @param float $valorCm
     * @return TipoLamina
     */
    public function setValorCm($valorCm)
    {
        $this->valorCm = $valorCm;

        return $this;
    }

    /**
     * Get valorCm
     *
     * @return float 
     */
    public function getValorCm()
    {
        return $this->valorCm;
    }

    /**
     * Set isActive
     *
     * @param boolean $isActive
     * @return TipoLamina
     */
    public function setIsActive($isActive)
    {
        $this->isActive = $isActive;

        return $this;
    }

    /**
     * Get isActive
     *
     * @return boolean 
     */
    public function getIsActive()
    {
        return $this->isActive;
    }

    /**
     * @ORM\PrePersist
     * @ORM\PreUpdate
     * metodo que calcula el area
     */
    public function calculaArea()
    {
       $area = $this->getAlto()*$this->getAncho();
       $this->setArea($area);
       if($area==0){
           
       }else{
            $precio = $this->getValor()/$area;
            $this->setValorCm($precio);
       }       
       return true;               
    }

    /**
     * Set fechaCreacion
     *
     * @param \DateTime $fechaCreacion
     * @return TipoLamina
     */
    public function setFechaCreacion($fechaCreacion)
    {
        $this->fechaCreacion = $fechaCreacion;

        return $this;
    }

    /**
     * Get fechaCreacion
     *
     * @return \DateTime 
     */
    public function getFechaCreacion()
    {
        return $this->fechaCreacion;
    }

    /**
     * Set fechaUltimaEdicion
     *
     * @param \DateTime $fechaUltimaEdicion
     * @return TipoLamina
     */
    public function setFechaUltimaEdicion($fechaUltimaEdicion)
    {
        $this->fechaUltimaEdicion = $fechaUltimaEdicion;

        return $this;
    }

    /**
     * Get fechaUltimaEdicion
     *
     * @return \DateTime 
     */
    public function getFechaUltimaEdicion()
    {
        return $this->fechaUltimaEdicion;
    }

    /**
     * Set usuarioCreador
     *
     * @param \Twinpeaks\UserBundle\Entity\User $usuarioCreador
     * @return TipoLamina
     */
    public function setUsuarioCreador(\Twinpeaks\UserBundle\Entity\User $usuarioCreador = null)
    {
        $this->usuarioCreador = $usuarioCreador;

        return $this;
    }

    /**
     * Get usuarioCreador
     *
     * @return \Twinpeaks\UserBundle\Entity\User 
     */
    public function getUsuarioCreador()
    {
        return $this->usuarioCreador;
    }

    /**
     * Set UsuarioUltimaModificacion
     *
     * @param \Twinpeaks\UserBundle\Entity\User $usuarioUltimaModificacion
     * @return TipoLamina
     */
    public function setUsuarioUltimaModificacion(\Twinpeaks\UserBundle\Entity\User $usuarioUltimaModificacion = null)
    {
        $this->UsuarioUltimaModificacion = $usuarioUltimaModificacion;

        return $this;
    }

    /**
     * Get UsuarioUltimaModificacion
     *
     * @return \Twinpeaks\UserBundle\Entity\User 
     */
    public function getUsuarioUltimaModificacion()
    {
        return $this->UsuarioUltimaModificacion;
    }

      /**
     * @ORM\PrePersist
     */
    public function setCreatedValue()
    {
        $this->setFechaCreacion(new \DateTime());
        $this->setFechaUltimaEdicion(new \DateTime());
    }

    /**
     * @ORM\PreUpdate
     */
    public function setUpdateValue()
    {
        $this->setFechaUltimaEdicion(new \DateTime());
    }

    /**
     * Set descripcion
     *
     * @param string $descripcion
     * @return TipoLamina
     */
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    /**
     * Get descripcion
     *
     * @return string 
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }
}
