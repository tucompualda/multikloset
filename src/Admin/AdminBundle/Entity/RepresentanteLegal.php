<?php

namespace Admin\AdminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * RepresentanteLegal
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Admin\AdminBundle\Entity\RepresentanteLegalRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class RepresentanteLegal
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\ManyToOne(targetEntity="Admin\AdminBundle\Entity\Empresa")
     */
    private $empresa;

    /**
     * @var string
     *
     * @ORM\Column(name="nombres", type="string", length=255)
     */
    private $nombres;

    /**
     * @var string
     *
     * @ORM\Column(name="apellidos", type="string", length=255)
     */
    private $apellidos;

    /**
     * @var string
     *
     * @ORM\ManyToOne(targetEntity="Twinpeaks\UserBundle\Entity\User")
     */
    private $usuarioAsociado;

    /**
     * @var string
     *
     * @ORM\Column(name="telefono", type="string", length=255)
     */
    private $telefono;

    /**
     * @var string
     *
     * @ORM\Column(name="tipoDocumento", type="string", length=255)
     */
    private $tipoDocumento;

    /**
     * @var string
     *
     * @ORM\Column(name="numeroDocumento", type="string", length=255)
     */
    private $numeroDocumento;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=255)
     */
    private $email;

    /**
     * @var boolean
     *
     * @ORM\Column(name="estado", type="boolean")
     */
    private $estado;

  /**
    * @var datetime
    * @ORM\Column(name="fechacreacion", type="datetime", nullable=true)
    */
    private $fechaCreacion;

    /**
    * @var string
    *
    * @ORM\ManyToOne(targetEntity="Twinpeaks\UserBundle\Entity\User")
    */
    private $usuarioCreador;

    /**
    * @var string
    *
    * @ORM\ManyToOne(targetEntity="Twinpeaks\UserBundle\Entity\User")
    */
    private $UsuarioUltimaModificacion;

   /**
    * @var datetime
    * @ORM\Column(name="fechaultimaedicion", type="datetime", nullable=true)
    */
    private $fechaUltimaEdicion;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nombres
     *
     * @param string $nombres
     * @return RepresentanteLegal
     */
    public function setNombres($nombres)
    {
        $this->nombres = $nombres;

        return $this;
    }

    /**
     * Get nombres
     *
     * @return string 
     */
    public function getNombres()
    {
        return $this->nombres;
    }

    /**
     * Set apellidos
     *
     * @param string $apellidos
     * @return RepresentanteLegal
     */
    public function setApellidos($apellidos)
    {
        $this->apellidos = $apellidos;

        return $this;
    }

    /**
     * Get apellidos
     *
     * @return string 
     */
    public function getApellidos()
    {
        return $this->apellidos;
    }

  
    /**
     * Set telefono
     *
     * @param string $telefono
     * @return RepresentanteLegal
     */
    public function setTelefono($telefono)
    {
        $this->telefono = $telefono;

        return $this;
    }

    /**
     * Get telefono
     *
     * @return string 
     */
    public function getTelefono()
    {
        return $this->telefono;
    }

    /**
     * Set tipoDocumento
     *
     * @param string $tipoDocumento
     * @return RepresentanteLegal
     */
    public function setTipoDocumento($tipoDocumento)
    {
        $this->tipoDocumento = $tipoDocumento;

        return $this;
    }

    /**
     * Get tipoDocumento
     *
     * @return string 
     */
    public function getTipoDocumento()
    {
        return $this->tipoDocumento;
    }

    /**
     * Set numeroDocumento
     *
     * @param string $numeroDocumento
     * @return RepresentanteLegal
     */
    public function setNumeroDocumento($numeroDocumento)
    {
        $this->numeroDocumento = $numeroDocumento;

        return $this;
    }

    /**
     * Get numeroDocumento
     *
     * @return string 
     */
    public function getNumeroDocumento()
    {
        return $this->numeroDocumento;
    }

    /**
     * Set email
     *
     * @param string $email
     * @return RepresentanteLegal
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string 
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set estado
     *
     * @param boolean $estado
     * @return RepresentanteLegal
     */
    public function setEstado($estado)
    {
        $this->estado = $estado;

        return $this;
    }

    /**
     * Get estado
     *
     * @return boolean 
     */
    public function getEstado()
    {
        return $this->estado;
    }

            /**
     * Set fechaCreacion
     *
     * @param \DateTime $fechaCreacion
     * @return Clientes
     */
    public function setFechaCreacion($fechaCreacion)
    {
        $this->fechaCreacion = $fechaCreacion;

        return $this;
    }

    /**
     * Get fechaCreacion
     *
     * @return \DateTime 
     */
    public function getFechaCreacion()
    {
        return $this->fechaCreacion;
    }

    /**
     * Set fechaUltimaEdicion
     *
     * @param \DateTime $fechaUltimaEdicion
     * @return Clientes
     */
    public function setFechaUltimaEdicion($fechaUltimaEdicion)
    {
        $this->fechaUltimaEdicion = $fechaUltimaEdicion;

        return $this;
    }

    /**
     * Get fechaUltimaEdicion
     *
     * @return \DateTime 
     */
    public function getFechaUltimaEdicion()
    {
        return $this->fechaUltimaEdicion;
    }

    /**
     * Set usuarioCreador
     *
     * @param \Twinpeaks\UserBundle\Entity\User $usuarioCreador
     * @return Clientes
     */
    public function setUsuarioCreador(\Twinpeaks\UserBundle\Entity\User $usuarioCreador = null)
    {
        $this->usuarioCreador = $usuarioCreador;

        return $this;
    }

    /**
     * Get usuarioCreador
     *
     * @return \Twinpeaks\UserBundle\Entity\User 
     */
    public function getUsuarioCreador()
    {
        return $this->usuarioCreador;
    }

    /**
     * Set UsuarioUltimaModificacion
     *
     * @param \Twinpeaks\UserBundle\Entity\User $usuarioUltimaModificacion
     * @return Clientes
     */
    public function setUsuarioUltimaModificacion(\Twinpeaks\UserBundle\Entity\User $usuarioUltimaModificacion = null)
    {
        $this->UsuarioUltimaModificacion = $usuarioUltimaModificacion;

        return $this;
    }

    /**
     * Get UsuarioUltimaModificacion
     *
     * @return \Twinpeaks\UserBundle\Entity\User 
     */
    public function getUsuarioUltimaModificacion()
    {
        return $this->UsuarioUltimaModificacion;
    }

    /**
     * @ORM\PrePersist
     */
    public function setCreatedValue()
    {
        $this->setFechaCreacion(new \DateTime());
        $this->setFechaUltimaEdicion(new \DateTime());
    }

    /**
     * @ORM\PreUpdate
     */
    public function setUpdateValue()
    {
        $this->setFechaUltimaEdicion(new \DateTime());
    }

    public function __toString()
    {
        return $this->getNombres()." ".$this->getApellidos();
    }

    /**
     * Set empresa
     *
     * @param \Admin\AdminBundle\Entity\Empresa $empresa
     * @return RepresentanteLegal
     */
    public function setEmpresa(\Admin\AdminBundle\Entity\Empresa $empresa = null)
    {
        $this->empresa = $empresa;

        return $this;
    }

    /**
     * Get empresa
     *
     * @return \Admin\AdminBundle\Entity\Empresa 
     */
    public function getEmpresa()
    {
        return $this->empresa;
    }

    /**
     * Set usuarioAsociado
     *
     * @param \Twinpeaks\UserBundle\Entity\User $usuarioAsociado
     * @return RepresentanteLegal
     */
    public function setUsuarioAsociado(\Twinpeaks\UserBundle\Entity\User $usuarioAsociado = null)
    {
        $this->usuarioAsociado = $usuarioAsociado;

        return $this;
    }

    /**
     * Get usuarioAsociado
     *
     * @return \Twinpeaks\UserBundle\Entity\User 
     */
    public function getUsuarioAsociado()
    {
        return $this->usuarioAsociado;
    }
}
