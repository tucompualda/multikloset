<?php

namespace Admin\AdminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Empresa
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Admin\AdminBundle\Entity\EmpresaRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class Empresa
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * @Assert\NotBlank()
     * @ORM\Column(name="razonSocial", type="string", length=255)
     */
    private $razonSocial;

    /**
     * @var string
     * @Assert\NotBlank()
     * @ORM\Column(name="nit", type="string", length=255)
     */
    private $nit;

    /**
     * @var string
     * @Assert\NotBlank()
     * @ORM\Column(name="direccion", type="string", length=255)
     */
    private $direccion;

    /**
     * @var string
     * 
     * @ORM\Column(name="paginaWeb", type="string", length=500, nullable=true)
     */
    private $paginaWeb;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=255)
     */
    private $email;

    /**
     * @var boolean
     *
     * @ORM\Column(name="estado", type="boolean")
     */
    private $estado;

   /**
    * @var datetime
    * @ORM\Column(name="fechacreacion", type="datetime", nullable=true)
    */
    private $fechaCreacion;

    /**
    * @var string
    *
    * @ORM\ManyToOne(targetEntity="Twinpeaks\UserBundle\Entity\User")
    */
    private $usuarioCreador;

    /**
    * @var string
    *
    * @ORM\ManyToOne(targetEntity="Twinpeaks\UserBundle\Entity\User")
    */
    private $UsuarioUltimaModificacion;

   /**
    * @var datetime
    * @ORM\Column(name="fechaultimaedicion", type="datetime", nullable=true)
    */
    private $fechaUltimaEdicion;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set razonSocial
     *
     * @param string $razonSocial
     * @return Empresa
     */
    public function setRazonSocial($razonSocial)
    {
        $this->razonSocial = $razonSocial;

        return $this;
    }

    /**
     * Get razonSocial
     *
     * @return string 
     */
    public function getRazonSocial()
    {
        return $this->razonSocial;
    }

    /**
     * Set nit
     *
     * @param string $nit
     * @return Empresa
     */
    public function setNit($nit)
    {
        $this->nit = $nit;

        return $this;
    }

    /**
     * Get nit
     *
     * @return string 
     */
    public function getNit()
    {
        return $this->nit;
    }

    /**
     * Set direccion
     *
     * @param string $direccion
     * @return Empresa
     */
    public function setDireccion($direccion)
    {
        $this->direccion = $direccion;

        return $this;
    }

    /**
     * Get direccion
     *
     * @return string 
     */
    public function getDireccion()
    {
        return $this->direccion;
    }

    /**
     * Set paginaWeb
     *
     * @param string $paginaWeb
     * @return Empresa
     */
    public function setPaginaWeb($paginaWeb)
    {
        $this->paginaWeb = $paginaWeb;

        return $this;
    }

    /**
     * Get paginaWeb
     *
     * @return string 
     */
    public function getPaginaWeb()
    {
        return $this->paginaWeb;
    }

    /**
     * Set email
     *
     * @param string $email
     * @return Empresa
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string 
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set estado
     *
     * @param boolean $estado
     * @return Empresa
     */
    public function setEstado($estado)
    {
        $this->estado = $estado;

        return $this;
    }

    /**
     * Get estado
     *
     * @return boolean 
     */
    public function getEstado()
    {
        return $this->estado;
    }

       /**
     * Set fechaCreacion
     *
     * @param \DateTime $fechaCreacion
     * @return Clientes
     */
    public function setFechaCreacion($fechaCreacion)
    {
        $this->fechaCreacion = $fechaCreacion;

        return $this;
    }

    /**
     * Get fechaCreacion
     *
     * @return \DateTime 
     */
    public function getFechaCreacion()
    {
        return $this->fechaCreacion;
    }

    /**
     * Set fechaUltimaEdicion
     *
     * @param \DateTime $fechaUltimaEdicion
     * @return Clientes
     */
    public function setFechaUltimaEdicion($fechaUltimaEdicion)
    {
        $this->fechaUltimaEdicion = $fechaUltimaEdicion;

        return $this;
    }

    /**
     * Get fechaUltimaEdicion
     *
     * @return \DateTime 
     */
    public function getFechaUltimaEdicion()
    {
        return $this->fechaUltimaEdicion;
    }

    /**
     * Set usuarioCreador
     *
     * @param \Twinpeaks\UserBundle\Entity\User $usuarioCreador
     * @return Clientes
     */
    public function setUsuarioCreador(\Twinpeaks\UserBundle\Entity\User $usuarioCreador = null)
    {
        $this->usuarioCreador = $usuarioCreador;

        return $this;
    }

    /**
     * Get usuarioCreador
     *
     * @return \Twinpeaks\UserBundle\Entity\User 
     */
    public function getUsuarioCreador()
    {
        return $this->usuarioCreador;
    }

    /**
     * Set UsuarioUltimaModificacion
     *
     * @param \Twinpeaks\UserBundle\Entity\User $usuarioUltimaModificacion
     * @return Clientes
     */
    public function setUsuarioUltimaModificacion(\Twinpeaks\UserBundle\Entity\User $usuarioUltimaModificacion = null)
    {
        $this->UsuarioUltimaModificacion = $usuarioUltimaModificacion;

        return $this;
    }

    /**
     * Get UsuarioUltimaModificacion
     *
     * @return \Twinpeaks\UserBundle\Entity\User 
     */
    public function getUsuarioUltimaModificacion()
    {
        return $this->UsuarioUltimaModificacion;
    }

    /**
     * @ORM\PrePersist
     */
    public function setCreatedValue()
    {
        $this->setFechaCreacion(new \DateTime());
        $this->setFechaUltimaEdicion(new \DateTime());
    }

    /**
     * @ORM\PreUpdate
     */
    public function setUpdateValue()
    {
        $this->setFechaUltimaEdicion(new \DateTime());
    }

    public function __toString()
    {
        return $this->getRazonSocial();
    }
}
