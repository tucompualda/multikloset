<?php

namespace Admin\AdminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Muebles
 *
 * @ORM\Table()
 * @ORM\HasLifecycleCallbacks()
 * @ORM\Entity(repositoryClass="Admin\AdminBundle\Entity\MueblesRepository")
 */
class Muebles
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="ref", type="string", length=255)
     */
    private $ref;

    /**
     * @var string
     *
     * @ORM\ManyToOne(targetEntity="Admin\AdminBundle\Entity\TipoMueble")
     */
    private $tipoMueble;
    
    /**
     * @var string
     *
     * @ORM\ManyToOne(targetEntity="Admin\AdminBundle\Entity\Clientes")
     */
    private $clientes;

    /**
     * @var float
     *
     * @ORM\Column(name="alto", type="float")
     */
    private $alto;

    /**
     * @var float
     *
     * @ORM\Column(name="ancho", type="float")
     */
    private $ancho;

    /**
     * @var float
     * @Assert\Range(
     *      min = "1",
     *      max = "180",
     *      minMessage = "Debe tener un espacio al menos",
     * )
     * @ORM\Column(name="diVertical", type="float")
     */
    private $diVertical;
    
    /**
     * @var float
     *
     * @ORM\Column(name="valor", type="float")
     */
    private $valor;    
    
      /**
     * @var float
     *
     * @ORM\Column(name="valorTotal", type="float", nullable=true)
     */
    private $valorTotal;

    /**
     * @var float
     *
     * @ORM\Column(name="valorTotalAccesorios", type="float", nullable=true)
     */
    private $valorTotalAccesorios;

    /**
     * @var float
     *
     * @ORM\Column(name="valorTotalPartes", type="float", nullable=true)
     */
    private $valorTotalPartes;

    /**
     * @var float
     *
     * @ORM\Column(name="manoobra", type="float")
     */
    private $manoObra; 


    /**
     * @var float
     *
     * @ORM\Column(name="manoobrametro", type="float")
     */
    private $manoObraMetroCuadrado; 

    /**
     * @var string
     *
     * @ORM\Column(name="tipoPuerta", type="string", length=255)
     */
    private $tipoPuerta;

    /**
     * @var string
     *
     * @ORM\Column(name="profundidad", type="float")
     */
    private $profundidad;

    /**
     * @var string
     * 
     * @ORM\OneToMany(targetEntity="Admin\AdminBundle\Entity\MueblesPartes", mappedBy="muebles", cascade={"persist","remove"})
     * @Assert\Valid()
     * @Assert\NotNull() 
     */
    private $partes;

    /**
     * @var string
     * 
     * @ORM\OneToMany(targetEntity="Admin\AdminBundle\Entity\MueblesAccesorios", mappedBy="muebles", cascade={"persist","remove"})
     * @Assert\Valid()
     * @Assert\NotNull() 
     */
    private $accesorios;
    
     /**
     * @var string
     *
     * @ORM\ManyToOne(targetEntity="Admin\AdminBundle\Entity\TipoLamina")
     */
    private $materialLateral;
   
   /**
     * @var string
     *
     * @ORM\ManyToOne(targetEntity="Admin\AdminBundle\Entity\Fleje")
     */
    private $flejeLateral;

    /**
     * @var float
     *
     * @ORM\Column(name="metrosFlejeLateral", type="float", nullable=true)
     */
    private $metrosFlejeLateral;

   /**
     * @var string
     *
     * @ORM\ManyToOne(targetEntity="Admin\AdminBundle\Entity\TipoLamina")
     */
    private $materialTrasera;

       /**
     * @var string
     *
     * @ORM\ManyToOne(targetEntity="Admin\AdminBundle\Entity\Fleje")
     */
    private $flejeTrasera;
    
    /**
     * @var float
     *
     * @ORM\Column(name="metrosFlejeTrasera", type="float", nullable=true)
     */
    private $metrosFlejeTrasera;

    /**
     * @var string
     *
     * @ORM\ManyToOne(targetEntity="Admin\AdminBundle\Entity\TipoLamina")
     */
    private $materialFondo;

       /**
     * @var string
     *
     * @ORM\ManyToOne(targetEntity="Admin\AdminBundle\Entity\Fleje")
     */
    private $flejeFondo;
    
    /**
     * @var float
     *
     * @ORM\Column(name="metrosFlejeFondo", type="float", nullable=true)
     */
    private $metrosFlejeFondo;

    /**
     * @var string
     *
     * @ORM\ManyToOne(targetEntity="Admin\AdminBundle\Entity\TipoLamina")
     */
    private $materialTapa;

       /**
     * @var string
     *
     * @ORM\ManyToOne(targetEntity="Admin\AdminBundle\Entity\Fleje")
     */
    private $flejeTapa;
    
    /**
     * @var float
     *
     * @ORM\Column(name="metrosFlejeTapa", type="float", nullable=true)
     */
    private $metrosFlejeTapa;

    /**
     * @var float
     *
     * @ORM\Column(name="altolateralreal", type="float")
     */
    private $altoLateralReal;
    
    /**
     * @var float
     *
     * @ORM\Column(name="ancholateralreal", type="float")
     */
    private $anchoLateralReal; 
   
  /**
     * @var float
     *
     * @ORM\Column(name="cantidadLateral", type="float")
     */
    private $cantidadLateral;

    /**
     * @var float
     *
     * @ORM\Column(name="valorLateral", type="float")
     */
    private $valorLateral;

    /**
     * @var float
     *
     * @ORM\Column(name="anchotechoreal", type="float")
     */
    private $anchoTechoReal;
    
    /**
     * @var float
     *
     * @ORM\Column(name="profundidadtechoreal", type="float")
     */
    private $profundidadTechoReal;   
    
    /**
     * @var float
     *
     * @ORM\Column(name="cantidadTecho", type="float")
     */
    private $cantidadTecho;

    /**
     * @var float
     *
     * @ORM\Column(name="valorTecho", type="float")
     */
    private $valorTecho;

        /**
     * @var float
     *
     * @ORM\Column(name="anchofondoreal", type="float")
     */
    private $anchoFondoReal;
    
    /**
     * @var float
     *
     * @ORM\Column(name="profundidadfondoreal", type="float")
     */
    private $profundidadFondoReal; 
    

      /**
     * @var float
     *
     * @ORM\Column(name="cantidadFondo", type="float")
     */
    private $cantidadFondo;

    /**
     * @var float
     *
     * @ORM\Column(name="valorFondo", type="float")
     */
    private $valorFondo;

        /**
     * @var float
     *
     * @ORM\Column(name="anchotraserareal", type="float")
     */
    private $anchoTraseraReal;
    
    /**
     * @var float
     *
     * @ORM\Column(name="altotraserareal", type="float")
     */
    private $altoTraseraReal; 
    
      /**
     * @var float
     *
     * @ORM\Column(name="cantidadTrasera", type="float")
     */
    private $cantidadTrasera;

    /**
     * @var float
     *
     * @ORM\Column(name="valorTrasera", type="float")
     */
    private $valorTrasera;
    
    
    /**
     * Array's construcction for the Entity
     */
    
    /**
     * @var string
     *
     * @ORM\Column(name="tipo", type="string", length=255)
     */
    private $tipo;
    
      /**
    * @var datetime
    * @ORM\Column(name="fechacreacion", type="datetime", nullable=true)
    */
    private $fechaCreacion;

    /**
    * @var string
    *
    * @ORM\ManyToOne(targetEntity="Twinpeaks\UserBundle\Entity\User")
    */
    private $usuarioCreador;

    /**
    * @var string
    *
    * @ORM\ManyToOne(targetEntity="Twinpeaks\UserBundle\Entity\User")
    */
    private $UsuarioUltimaModificacion;

   /**
    * @var datetime
    * @ORM\Column(name="fechaultimaedicion", type="datetime", nullable=true)
    */
    private $fechaUltimaEdicion;


   /**
     * @var boolean
     *
     * @ORM\Column(name="estado", type="boolean", nullable=true)
     */
    private $estado;

    public function __construct() 
    {
        $this->partes = new ArrayCollection();
        $this->accesorios = new ArrayCollection();
    }

        /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set ref
     *
     * @param string $ref
     * @return Muebles
     */
    public function setRef($ref)
    {
        $this->ref = $ref;

        return $this;
    }

    /**
     * Get ref
     *
     * @return string 
     */
    public function getRef()
    {
        return $this->ref;
    }

 /**
     * Set tipoMueble
     *
     * @param string $tipoMueble
     * @return Muebles
     */
    public function setTipoMueble(\Admin\AdminBundle\Entity\TipoMueble $tipoMueble)
    {
        $this->tipoMueble = $tipoMueble;

        return $this;
    }

    /**
     * Get tipoMueble
     *
     * @return string 
     */
    public function getTipoMueble()
    {
        return $this->tipoMueble;
    }

     /**
     * Set clientes
     *
     * @param string $clientes
     * @return Muebles
     */
    public function setClientes(\Admin\AdminBundle\Entity\Clientes $clientes)
    {
        $this->clientes = $clientes;

        return $this;
    }

    /**
     * Get clientes
     *
     * @return string 
     */
    public function getClientes()
    {
        return $this->clientes;
    }
    
    
    /**
     * Set alto
     *
     * @param float $alto
     * @return Muebles
     */
    public function setAlto($alto)
    {
        $this->alto = $alto;

        return $this;
    }

    /**
     * Get alto
     *
     * @return float 
     */
    public function getAlto()
    {
        return $this->alto;
    }

    /**
     * Set ancho
     *
     * @param float $ancho
     * @return Muebles
     */
    public function setAncho($ancho)
    {
        $this->ancho = $ancho;

        return $this;
    }

    /**
     * Get ancho
     *
     * @return float 
     */
    public function getAncho()
    {
        return $this->ancho;
    }


    
    /**
     * Set tipoPuerta
     *
     * @param string $tipoPuerta
     * @return Muebles
     */
    public function setTipoPuerta($tipoPuerta)
    {
        $this->tipoPuerta = $tipoPuerta;

        return $this;
    }

    /**
     * Get tipoPuerta
     *
     * @return string 
     */
    public function getTipoPuerta()
    {
        return $this->tipoPuerta;
    }

    /**
     * Set profundidad
     *
     * @param string $profundidad
     * @return Muebles
     */
    public function setProfundidad($profundidad)
    {
        $this->profundidad = $profundidad;

        return $this;
    }

    /**
     * Get profundidad
     *
     * @return string 
     */
    public function getProfundidad()
    {
        return $this->profundidad;
    }
    
    /**
     * set partes
     * @return \Admin\AdminBundle\Entity\MueblesPartes
     */
    public function setPartes(\Doctrine\Common\Collections\Collection $partes)
    {
        $this->partes = $partes;
        foreach ($partes as $parte){
            $parte->setMuebles($this);
        }
    }
    
    /**
     * get partes
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPartes()
    {
        return $this->partes;
    }
    
    /** set accesorios
     * @return \Admin\AdminBundle\Entity\MueblesAccesorios
     */
    public function setAccesorios(\Doctrine\Common\Collections\Collection $accesorios)
    {
        $this->accesorios = $accesorios;
        foreach ($accesorios as $accesorio){
            $accesorio->setMuebles($this);
        }
    }
    
    /**
     * get accesorios
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getAccesorios()
    {
        return $this->accesorios;
    }
    
    /**
     * Set tipo
     *
     * @param string $tipo
     * @return Muebles
     */
    public function setTipo($tipo)
    {
        $this->tipo = $tipo;

        return $this;
    }

    /**
     * Get tipo
     *
     * @return string 
     */
    public function getTipo()
    {
        return $this->tipo;
    }
    
    public function __toString() {
        return $this->getRef();
    }

      
    /**
     * Set valor
     * @param float $valor
     * @return Muebles
     */
    public function setValor($valor)
    {
       $this->valor = $valor;

        return $this;
        
    }

    /**
     * Get valor
     *
     * @return float 
     */
    public function getValor()
    {
        return $this->valor;
    }

   /**
     * Set manoObra
     * @param float $manoObra
     * @return Muebles
     */
    public function setManoObra($manoObra)
    {
       $this->manoObra = $manoObra;

        return $this;
        
    }

    /**
     * Get manoObra
     *
     * @return float 
     */
    public function getManoObra()
    {
        return $this->manoObra;
    }

   /**
     * Set manoObraMetroCuadrado
     * @param float $manoObraMetroCuadrado
     * @return Muebles
     */
    public function setManoObraMetroCuadrado($manoObraMetroCuadrado)
    {
       $this->manoObraMetroCuadrado = $manoObraMetroCuadrado;

        return $this;
        
    }

    /**
     * Get manoObraMetroCuadrado
     *
     * @return float 
     */
    public function getManoObraMetroCuadrado()
    {
        return $this->manoObraMetroCuadrado;
    }

    /**
     * Set materialLateral
     *
     * @param \Admin\AdminBundle\Entity\TipoLamina $materialLateral
     * @return Muebles
     */
    public function setMaterialLateral(\Admin\AdminBundle\Entity\TipoLamina $materialLateral = null)
    {
        $this->materialLateral = $materialLateral;

        return $this;
    }

    /**
     * Get materialLateral
     *
     * @return \Admin\AdminBundle\Entity\TipoLamina 
     */
    public function getMaterialLateral()
    {
        return $this->materialLateral;
    }

    /**
     * Set materialTrasera
     *
     * @param \Admin\AdminBundle\Entity\TipoLamina $materialTrasera
     * @return Muebles
     */
    public function setMaterialTrasera(\Admin\AdminBundle\Entity\TipoLamina $materialTrasera = null)
    {
        $this->materialTrasera = $materialTrasera;

        return $this;
    }

    /**
     * Get materialTrasera
     *
     * @return \Admin\AdminBundle\Entity\TipoLamina 
     */
    public function getMaterialTrasera()
    {
        return $this->materialTrasera;
    }

    /**
     * Set materialFondo
     *
     * @param \Admin\AdminBundle\Entity\TipoLamina $materialFondo
     * @return Muebles
     */
    public function setMaterialFondo(\Admin\AdminBundle\Entity\TipoLamina $materialFondo = null)
    {
        $this->materialFondo = $materialFondo;

        return $this;
    }

    /**
     * Get materialFondo
     *
     * @return \Admin\AdminBundle\Entity\TipoLamina 
     */
    public function getMaterialFondo()
    {
        return $this->materialFondo;
    }

    /**
     * Set materialTapa
     *
     * @param \Admin\AdminBundle\Entity\TipoLamina $materialTapa
     * @return Muebles
     */
    public function setMaterialTapa(\Admin\AdminBundle\Entity\TipoLamina $materialTapa = null)
    {
        $this->materialTapa = $materialTapa;

        return $this;
    }

    /**
     * Get materialTapa
     *
     * @return \Admin\AdminBundle\Entity\TipoLamina 
     */
    public function getMaterialTapa()
    {
        return $this->materialTapa;
    }

    /**
     * Set diVertical
     *
     * @param float $diVertical
     * @return Muebles
     */
    public function setDiVertical($diVertical)
    {
        $this->diVertical = $diVertical;

        return $this;
    }

    /**
     * Get diVertical
     *
     * @return float 
     */
    public function getDiVertical()
    {
        return $this->diVertical;
    }

    /**
     * Set altoLateralReal
     * @ORM\PrePersist
     * @ORM\PreUpdate
     * @param float $altoLateralReal
     * @return Muebles
     */
    public function setAltoLateralReal()
    {
        $altoreal= $this->getAlto() - ($this->getMaterialTapa()->getEspesor()/10) - ($this->getMaterialFondo()->getEspesor()/10);
        $this->altoLateralReal = $altoreal;
    }

    /**
     * Get altoLateralReal
     *
     * @return float 
     */
    public function getAltoLateralReal()
    {
        return $this->altoLateralReal;
    }

    /**
     * Set anchoLateralReal
     * @ORM\PrePersist
     * @ORM\PreUpdate
     * @param float $anchoLateralReal
     * @return Muebles
     */
    public function setAnchoLateralReal()
    {
        $anchoreal = $this->getProfundidad();
        $this->anchoLateralReal = $anchoreal;

        return $this;
    }

    /**
     * Get anchoLateralReal
     *
     * @return float 
     */
    public function getAnchoLateralReal()
    {
        return $this->anchoLateralReal;
    }

    /**
     * Set anchoTechoReal
     * @ORM\PrePersist
     * @ORM\PreUpdate
     * @param float $anchoTechoReal
     * @return Muebles
     */
    public function setAnchoTechoReal()
    {
        $anchoreal = $this->getAncho();
        $this->anchoTechoReal = $anchoreal;

    }

    /**
     * Get anchoTechoReal
     *
     * @return float 
     */
    public function getAnchoTechoReal()
    {
        return $this->anchoTechoReal;
    }

    /**
     * Set profundidadTechoReal
     * @ORM\PrePersist
     * @ORM\PreUpdate
     * @param float $profundidadTechoReal
     * @return Muebles
     */
    public function setProfundidadTechoReal()
    {
        
        $this->profundidadTechoReal = $this->getProfundidad();

    }

    /**
     * Get profundidadTechoReal
     *
     * @return float 
     */
    public function getProfundidadTechoReal()
    {
        return $this->profundidadTechoReal;
    }


 /**
     * Set cantidadLateral
     * @param float $cantidadLateral
     * @return Muebles
     */
    public function setCantidadLateral($cantidadLateral)
    {
       $this->cantidadLateral = $cantidadLateral;

        return $this;
        
    }

    /**
     * Get cantidadLateral
     *
     * @return float 
     */
    public function getCantidadLateral()
    {
        return $this->cantidadLateral;
    }


 /**
     * Set valorLateral
     * @param float $valorLateral
     * @return Muebles
     */
    public function setValorLateral($valorLateral)
    {
       $this->valorLateral = $valorLateral;

        return $this;
        
    }

    /**
     * Get valorLateral
     *
     * @return float 
     */
    public function getValorLateral()
    {
        return $this->valorLateral;
    }


  /**
     * Set cantidadTecho
     * @param float $cantidadTecho
     * @return Muebles
     */
    public function setCantidadTecho($cantidadTecho)
    {
       $this->cantidadTecho = $cantidadTecho;

        return $this;
        
    }

    /**
     * Get cantidadTecho
     *
     * @return float 
     */
    public function getCantidadTecho()
    {
        return $this->cantidadTecho;
    }

    /**
     * Set valorTecho
     * @param float $valorTecho
     * @return Muebles
     */
    public function setValorTecho($valorTecho)
    {
       $this->valorTecho = $valorTecho;

        return $this;
        
    }

    /**
     * Get valorTecho
     *
     * @return float 
     */
    public function getValorTecho()
    {
        return $this->valorTecho;
    }

/**
     * Set cantidadFondo
     * @param float $cantidadFondo
     * @return Muebles
     */
    public function setCantidadFondo($cantidadFondo)
    {
       $this->cantidadFondo = $cantidadFondo;

        return $this;
        
    }

    /**
     * Get cantidadFondo
     *
     * @return float 
     */
    public function getCantidadFondo()
    {
        return $this->cantidadFondo;
    }

    /**
     * Set valorFondo
     * @param float $valorFondo
     * @return Muebles
     */
    public function setValorFondo($valorFondo)
    {
       $this->valorFondo = $valorFondo;

        return $this;
        
    }

    /**
     * Get valorFondo
     *
     * @return float 
     */
    public function getValorFondo()
    {
        return $this->valorFondo;
    }

/**
     * Set cantidadTrasera
     * @param float $cantidadTrasera
     * @return Muebles
     */
    public function setCantidadTrasera($cantidadTrasera)
    {
       $this->cantidadTrasera = $cantidadTrasera;

        return $this;
        
    }

    /**
     * Get cantidadTrasera
     *
     * @return float 
     */
    public function getCantidadTrasera()
    {
        return $this->cantidadTrasera;
    }

    /**
     * Set valorTrasera
     * @param float $valorTrasera
     * @return Muebles
     */
    public function setValorTrasera($valorTrasera)
    {
       $this->valorTrasera = $valorTrasera;

        return $this;
        
    }

    /**
     * Get valorTrasera
     *
     * @return float 
     */
    public function getValorTrasera()
    {
        return $this->valorTrasera;
    }


    /**
     * Set anchoFondoReal
     * @ORM\PrePersist
     * @ORM\PreUpdate
     * @param float $anchoFondoReal
     * @return Muebles
     */
    public function setAnchoFondoReal()
    {
        $this->anchoFondoReal = $this->getAncho();


    }

    /**
     * Get anchoFondoReal
     *
     * @return float 
     */
    public function getAnchoFondoReal()
    {
        return $this->anchoFondoReal;
    }

    /**
     * Set profundidadFondoReal
     * @ORM\PrePersist
     * @ORM\PreUpdate
     * @param float $profundidadFondoReal
     * @return Muebles
     */
    public function setProfundidadFondoReal()
    {
        $this->profundidadFondoReal = $this->getProfundidad();

        return $this;
    }

    /**
     * Get profundidadFondoReal
     *
     * @return float 
     */
    public function getProfundidadFondoReal()
    {
        return $this->profundidadFondoReal;
    }

    /**
     * Set anchoTraseraReal
     * @ORM\PrePersist
     * @ORM\PreUpdate
     * @param float $anchoTraseraReal
     * @return Muebles
     */
    public function setAnchoTraseraReal()
    {
        $this->anchoTraseraReal = $this->getAncho();

    }

    /**
     * Get anchoTraseraReal
     *
     * @return float 
     */
    public function getAnchoTraseraReal()
    {
        return $this->anchoTraseraReal;
    }

    /**
     * Set altoTraseraReal
     * @ORM\PrePersist
     * @ORM\PreUpdate
     * @param float $altoTraseraReal
     * @return Muebles
     */
    public function setAltoTraseraReal()
    {
        $this->altoTraseraReal = $this->getAlto();
    }

    /**
     * Get altoTraseraReal
     *
     * @return float 
     */
    public function getAltoTraseraReal()
    {
        return $this->altoTraseraReal;
    }

 

    /**
     * Add partes
     *
     * @param \Admin\AdminBundle\Entity\MueblesPartes $partes
     * @return Muebles
     */
    public function addParte(\Admin\AdminBundle\Entity\MueblesPartes $partes)
    {
        $this->partes[] = $partes;

        return $this;
    }

    /**
     * Remove partes
     *
     * @param \Admin\AdminBundle\Entity\MueblesPartes $partes
     */
    public function removeParte(\Admin\AdminBundle\Entity\MueblesPartes $partes)
    {
        $this->partes->removeElement($partes);
    }

    /**
     * Add accesorios
     *
     * @param \Admin\AdminBundle\Entity\MueblesAccesorios $accesorios
     * @return Muebles
     */
    public function addAccesorio(\Admin\AdminBundle\Entity\MueblesAccesorios $accesorios)
    {
        $this->accesorios[] = $accesorios;

        return $this;
    }

    /**
     * Remove accesorios
     *
     * @param \Admin\AdminBundle\Entity\MueblesAccesorios $accesorios
     */
    public function removeAccesorio(\Admin\AdminBundle\Entity\MueblesAccesorios $accesorios)
    {
        $this->accesorios->removeElement($accesorios);
    }

    /**
     * Set flejeLateral
     *
     * @param \Admin\AdminBundle\Entity\Fleje $flejeLateral
     * @return Muebles
     */
    public function setFlejeLateral(\Admin\AdminBundle\Entity\Fleje $flejeLateral = null)
    {
        $this->flejeLateral = $flejeLateral;

        return $this;
    }

    /**
     * Get flejeLateral
     *
     * @return \Admin\AdminBundle\Entity\Fleje 
     */
    public function getFlejeLateral()
    {
        return $this->flejeLateral;
    }


    /**
     * @ORM\prePersist
     *
     */
     public function calcularFlejes()
     {
         
        $cantidadFlejeLateral = $this->getAltoLateralReal()*2;
        $this->setMetrosFlejeLateral($cantidadFlejeLateral);
        //$this->setMetrosFlejeTrasera();
        //$this->setMetrosFlejeFondo($this->get);
        
        $cantidadFlejeTapa = $this->getAncho()+(2*$this->getProfundidad());
        $this->setMetrosFlejeTapa($cantidadFlejeTapa);

        $cantidadFlejePiso = $this->getAncho()+(2*$this->getProfundidad());       
        $this->setMetrosFlejeFondo($cantidadFlejePiso);
     }


    /**
     * @ORM\prePersist
     * @ORM\PreUpdate
     * metodo que calcula el Valor Inicial
     */
    public function calculaValor()
    {
        $areaTotal = ($this->getAlto()/100 )* ($this->getAncho()/100);
        $valorManoObra = $areaTotal * $this->getManoObraMetroCuadrado();
        $this->setManoObra($valorManoObra);

        //////////////////////////////////////////////////////////////////////////////////////////////////////////////

        $valorFlejeLateral = 2 * $this->getAltoLateralReal() * $this->getFlejeLateral()->getValorMetrosLineal()/100;
        $valorFlejeTapa = (2*$this->getProfundidad()+$this->getAncho())*$this->getFlejeTapa()->getValorMetrosLineal()/100;
        //$valorMetroFlejeMaterial = $this->getFlejeLateral()->getValorMetrosLineal();
        $valorFlejePiso = (2*$this->getProfundidad()+$this->getAncho())*$this->getFlejeFondo()->getValorMetrosLineal()/100;


        /////////////// RELACION DE ASPECTO  MATERIAL LATERAL ////////////////////////////////////////////////////////
    

        $anchoMaterialLateral = $this->getMaterialLateral()->getAncho();
        $altoMaterialLateral  = $this->getMaterialLateral()->getAlto();

        $relacionAnchoMaterialAnchoLateral = (int) ($anchoMaterialLateral / $this->getAnchoLateralReal());
        $relacionAltoMaterialAltoLateral = (int) ($altoMaterialLateral / $this->getAltoLateralReal());
        
        $producto7 = $relacionAnchoMaterialAnchoLateral * $relacionAltoMaterialAltoLateral;

        $relacionAnchoMaterialAltoLateral = (int) ($anchoMaterialLateral / $this->getAltoLateralReal());
        $relacionAltoMaterialAnchoLateral = (int) ($altoMaterialLateral / $this->getAnchoLateralReal());

        $producto8 = $relacionAnchoMaterialAltoLateral * $relacionAltoMaterialAnchoLateral;

        $valorMaterialLateral = $this->getMaterialLateral()->getValor();

        

        $valorcmlateral = $this->getMaterialLateral()->getValorCm();
        $areaLateralReal = 2*$this->getAnchoLateralReal() * $this->getAltoLateralReal();
        $valorLateralReal = $valorcmlateral * $areaLateralReal; 
        $this->setValorLateral($valorLateralReal);




        if($producto7 > $producto8){

            $this->setCantidadLateral($producto7);

            $areaTrabajarLateral = $producto7 *  $this->getAnchoLateralReal() *  $this->getAltoLateralReal();
            
            $valorMaterialCmLateral = $valorMaterialLateral/$areaTrabajarLateral;

            $areaLateral = 2*$this->getAnchoLateralReal() * $this->getAltoLateralReal() * $valorMaterialCmLateral;
            
            $this->setCantidadLateral($producto7);
            //$this->setValorLateral($areaLateral);

        }else{
            
            $this->setCantidadLateral($producto8);

            $areaTrabajarLateral = $producto8 *  $this->getAnchoLateralReal() *  $this->getAltoLateralReal();
            
            if($areaTrabajarLateral != 0){
                $valorMaterialCmLateral = $valorMaterialLateral/$areaTrabajarLateral;
            }else{
                $valorMaterialCmLateral = 0;
            }
            

            $areaLateral = 2*$this->getAnchoLateralReal() * $this->getAltoLateralReal() * $valorMaterialCmLateral;
            
            $this->setCantidadLateral($producto8);
            //$this->setValorLateral($areaLateral);
        }

        //////////////////////// FIN RELACION //////////////////////////////////////////////////////


       
        /////////////// RELACION DE ASPECTO  MATERIAL TECHO////////////////////////////////////////////////////////
    
        $anchoMaterialTecho = $this->getMaterialTapa()->getAncho();
        $altoMaterialTecho  = $this->getMaterialTapa()->getAlto();

        $relacionAnchoMaterialAnchoTecho = (int) ($anchoMaterialTecho / $this->getAnchoTechoReal());
        $relacionAltoMaterialAltoTecho = (int) ($altoMaterialTecho / $this->getProfundidadTechoReal());
        
        $producto = $relacionAnchoMaterialAnchoTecho * $relacionAltoMaterialAltoTecho;

        $relacionAnchoMaterialAltoTecho = (int) ($anchoMaterialTecho / $this->getProfundidadTechoReal());
        $relacionAltoMaterialAnchoTecho = (int) ($altoMaterialTecho / $this->getAnchoTechoReal());

        $producto2 = $relacionAnchoMaterialAltoTecho * $relacionAltoMaterialAnchoTecho;

        $valorMaterial = $this->getMaterialTapa()->getValor();


        $valorcmtecho = $this->getMaterialTapa()->getValorCm();
        $areaTechoReal = $this->getAnchoTechoReal() * $this->getProfundidadTechoReal();
        $valorTechoReal = $valorcmtecho * $areaTechoReal; 
        $this->setValorTecho($valorTechoReal);

        if($producto > $producto2){

            $this->setCantidadTecho($producto);

            $areaTrabajar = $producto *  $this->getAnchoTechoReal() *  $this->getProfundidadTechoReal();
            
            $valorMaterialCm = $valorMaterial/$areaTrabajar;

            $areaTecho = $this->getAnchoTechoReal() * $this->getProfundidadTechoReal() * $valorMaterialCm;
            
            $this->setCantidadTecho($producto);
           // $this->setValorTecho($areaTecho);

        }else{
            
            $areaTrabajar = $producto2 *  $this->getAnchoTechoReal() *  $this->getProfundidadTechoReal();
           
           if($areaTrabajar != 0){
                 $valorMaterialCm = $valorMaterial/$areaTrabajar;
            }else{
                $valorMaterialCm = 0;
            }
            $areaTecho = $this->getAnchoTechoReal() * $this->getProfundidadTechoReal() * $valorMaterialCm;

            $this->setCantidadTecho($producto2);
            //$this->setValorTecho($areaTecho);
        }

        //////////////////////// FIN RELACION //////////////////////////////////////////////////////

        /////////////// RELACION DE ASPECTO  MATERIAL TRASERA ////////////////////////////////////////////////////////
    
        $anchoMaterialTrasera = $this->getMaterialTrasera()->getAncho();
        $altoMaterialTrasera  = $this->getMaterialTrasera()->getAlto();

        $relacionAnchoMaterialAnchoTrasera = (int) ($anchoMaterialTrasera / $this->getAnchoTraseraReal());
        $relacionAltoMaterialAltoTrasera = (int) ($altoMaterialTrasera / $this->getAltoTraseraReal());
        
        $producto3 = $relacionAnchoMaterialAnchoTrasera * $relacionAltoMaterialAltoTrasera;

        $relacionAnchoMaterialAltoTrasera = (int) ($anchoMaterialTrasera / $this->getAltoTraseraReal());
        $relacionAltoMaterialAnchoTrasera = (int) ($altoMaterialTecho / $this->getAnchoTraseraReal());

        $producto4 = $relacionAnchoMaterialAltoTrasera * $relacionAltoMaterialAnchoTrasera;

        $valorMaterialTrasera = $this->getMaterialTrasera()->getValor();

        $valorcmtrasera = $this->getMaterialTrasera()->getValorCm();
        $areaTraseraReal = $this->getAnchoTraseraReal() * $this->getAltoTraseraReal();
        $valorTraseraReal = $valorcmtrasera * $areaTraseraReal; 
        $this->setValorTrasera($valorTraseraReal);


        if($producto3 > $producto4){

            $this->setCantidadTrasera($producto3);

            $areaTrabajarTrasera = $producto3 *  $this->getAnchoTraseraReal() *  $this->getAltoTraseraReal();
            
            $valorMaterialCmTrasera = $valorMaterialTrasera/$areaTrabajarTrasera;

            $areaTrasera = $this->getAnchoTraseraReal() * $this->getAltoTraseraReal() * $valorMaterialCmTrasera;
            
            $this->setCantidadTrasera($producto3);
            //$this->setValorTrasera($areaTrasera);

        }else{
            
            $this->setCantidadTrasera($producto4);

            $areaTrabajarTrasera = $producto4 *  $this->getAnchoTraseraReal() *  $this->getAltoTraseraReal();
            
            if($areaTrabajarTrasera != 0){
                $valorMaterialCmTrasera = $valorMaterialTrasera/$areaTrabajarTrasera;
            }else{
                $valorMaterialCmTrasera = 0;
            }

            $areaTrasera = $this->getAnchoTraseraReal() * $this->getAltoTraseraReal() * $valorMaterialCmTrasera;
            
            $this->setCantidadTrasera($producto4);
            //$this->setValorTrasera($areaTrasera);
        }

        //////////////////////// FIN RELACION //////////////////////////////////////////////////////

    
        /////////////// RELACION DE ASPECTO  MATERIAL FONDO ////////////////////////////////////////////////////////
    
        $anchoMaterialFondo = $this->getMaterialFondo()->getAncho();
        $altoMaterialFondo  = $this->getMaterialfondo()->getAlto();

        $relacionAnchoMaterialAnchoFondo = (int) ($anchoMaterialFondo / $this->getAnchoFondoReal());
        $relacionAltoMaterialAltoFondo = (int) ($altoMaterialFondo / $this->getProfundidadFondoReal());
        
        $producto5 = $relacionAnchoMaterialAnchoFondo * $relacionAltoMaterialAltoFondo;

        $relacionAnchoMaterialAltoFondo = (int) ($anchoMaterialFondo / $this->getProfundidadFondoReal());
        $relacionAltoMaterialAnchoFondo = (int) ($altoMaterialFondo / $this->getAnchoFondoReal());

        $producto6 = $relacionAnchoMaterialAltoFondo * $relacionAltoMaterialAnchoFondo;

        $valorMaterialFondo = $this->getMaterialFondo()->getValor();


        $valorcmfondo = $this->getMaterialFondo()->getValorCm();
        $areaFondoReal =$this->getAnchoFondoReal() * $this->getProfundidadFondoReal();
        $valorFondoReal = $valorcmfondo * $areaFondoReal; 
        $this->setValorFondo($valorFondoReal);


        if($producto5 > $producto6){

            $this->setCantidadFondo($producto5);

            $areaTrabajarFondo = $producto5 *  $this->getAnchoFondoReal() *  $this->getProfundidadFondoReal();
            
            $valorMaterialCmFondo = $valorMaterialFondo/$areaTrabajarFondo;

            $areaFondo = $this->getAnchoFondoReal() * $this->getProfundidadFondoReal() * $valorMaterialCmFondo;
            
            $this->setCantidadFondo($producto5);
           // $this->setValorFondo($areaFondo);

        }else{
            
            $this->setCantidadFondo($producto6);

            $areaTrabajarFondo = $producto6 *  $this->getAnchoFondoReal() *  $this->getProfundidadFondoReal();
            
            if($areaTrabajarFondo != 0){
                $valorMaterialCmFondo = $valorMaterialFondo/$areaTrabajarFondo;    
            }else{
                $valorMaterialCmFondo = 0;
            }
            

            $areaFondo = $this->getAnchoFondoReal() * $this->getProfundidadFondoReal() * $valorMaterialCmFondo;
            
            $this->setCantidadFondo($producto6);
            //$this->setValorFondo($areaFondo);
        }

        //////////////////////// FIN RELACION //////////////////////////////////////////////////////

       
        $valortotal = $valorLateralReal+$valorTechoReal+$valorTraseraReal+$valorFondoReal+$valorManoObra + $valorFlejeLateral + $valorFlejeTapa + $valorFlejePiso;
        
        $this->setValor($valortotal);
             
        return true;           
    }


  /**
     * Set metrosFlejeLateral
     * @param float $metrosFlejeLateral
     * @return Muebles
     */
    public function setMetrosFlejeLateral($metrosFlejeLateral)
    {
       $this->metrosFlejeLateral = $metrosFlejeLateral;

        return $this;
        
    }

    /**
     * Get metrosFlejeLateral
     *
     * @return float 
     */
    public function getMetrosFlejeLateral()
    {
        return $this->metrosFlejeLateral;
    }



    /**
     * Set flejeTrasera
     *
     * @param \Admin\AdminBundle\Entity\Fleje $flejeTrasera
     * @return Muebles
     */
    public function setFlejeTrasera(\Admin\AdminBundle\Entity\Fleje $flejeTrasera = null)
    {
        $this->flejeTrasera = $flejeTrasera;

        return $this;
    }

    /**
     * Get flejeTrasera
     *
     * @return \Admin\AdminBundle\Entity\Fleje 
     */
    public function getFlejeTrasera()
    {
        return $this->flejeTrasera;
    }

    /**
     * Set metrosFlejeTrasera
     * @param float $metrosFlejeTrasera
     * @return Muebles
     */
    public function setMetrosFlejeTrasera($metrosFlejeTrasera)
    {
       $this->metrosFlejeTrasera = $metrosFlejeTrasera;

        return $this;
        
    }

    /**
     * Get metrosFlejeTrasera
     *
     * @return float 
     */
    public function getMetrosFlejeTrasera()
    {
        return $this->metrosFlejeTrasera;
    }

    /**
     * Set flejeFondo
     *
     * @param \Admin\AdminBundle\Entity\Fleje $flejeFondo
     * @return Muebles
     */
    public function setFlejeFondo(\Admin\AdminBundle\Entity\Fleje $flejeFondo = null)
    {
        $this->flejeFondo = $flejeFondo;

        return $this;
    }

    /**
     * Get flejeFondo
     *
     * @return \Admin\AdminBundle\Entity\Fleje 
     */
    public function getFlejeFondo()
    {
        return $this->flejeFondo;
    }

/**
     * Set metrosFlejeFondo
     * @param float $metrosFlejeFondo
     * @return Muebles
     */
    public function setMetrosFlejeFondo($metrosFlejeFondo)
    {
       $this->metrosFlejeFondo = $metrosFlejeFondo;

        return $this;
        
    }

    /**
     * Get metrosFlejeFondo
     *
     * @return float 
     */
    public function getMetrosFlejeFondo()
    {
        return $this->metrosFlejeFondo;
    }


    /**
     * Set flejeTapa
     *
     * @param \Admin\AdminBundle\Entity\Fleje $flejeTapa
     * @return Muebles
     */
    public function setFlejeTapa(\Admin\AdminBundle\Entity\Fleje $flejeTapa = null)
    {
        $this->flejeTapa = $flejeTapa;

        return $this;
    }

    /**
     * Get flejeTapa
     *
     * @return \Admin\AdminBundle\Entity\Fleje 
     */
    public function getFlejeTapa()
    {
        return $this->flejeTapa;
    }

    /**
     * Set metrosFlejeTapa
     * @param float $metrosFlejeTapa
     * @return Muebles
     */
    public function setMetrosFlejeTapa($metrosFlejeTapa)
    {
       $this->metrosFlejeTapa= $metrosFlejeTapa;

        return $this;
        
    }

    /**
     * Get metrosFlejeTapa
     *
     * @return float 
     */
    public function getMetrosFlejeTapa()
    {
        return $this->metrosFlejeTapa;
    }

   

    /**
     * Set valorTotal
     *
     * @param float $valorTotal
     * @return Muebles
     */
    public function setValorTotal($valorTotal)
    {
        $this->valorTotal = $valorTotal;

        return $this;
    }

    /**
     * Get valorTotal
     *
     * @return float 
     */
    public function getValorTotal()
    {
        return $this->valorTotal;
    }

    /**
     * Set valorTotalAccesorios
     *
     * @param float $valorTotalAccesorios
     * @return Muebles
     */
    public function setValorTotalAccesorios($valorTotalAccesorios)
    {
        $this->valorTotalAccesorios = $valorTotalAccesorios;

        return $this;
    }

    /**
     * Get valorTotalAccesorios
     *
     * @return float 
     */
    public function getValorTotalAccesorios()
    {
        return $this->valorTotalAccesorios;
    }

    /**
     * Set valorTotalPartes
     *
     * @param float $valorTotalPartes
     * @return Muebles
     */
    public function setValorTotalPartes($valorTotalPartes)
    {
        $this->valorTotalPartes = $valorTotalPartes;

        return $this;
    }

    /**
     * Get valorTotalPartes
     *
     * @return float 
     */
    public function getValorTotalPartes()
    {
        return $this->valorTotalPartes;
    }

    /**
     * Set fechaCreacion
     *
     * @param \DateTime $fechaCreacion
     * @return Muebles
     */
    public function setFechaCreacion($fechaCreacion)
    {
        $this->fechaCreacion = $fechaCreacion;

        return $this;
    }

    /**
     * Get fechaCreacion
     *
     * @return \DateTime 
     */
    public function getFechaCreacion()
    {
        return $this->fechaCreacion;
    }

    /**
     * Set fechaUltimaEdicion
     *
     * @param \DateTime $fechaUltimaEdicion
     * @return Muebles
     */
    public function setFechaUltimaEdicion($fechaUltimaEdicion)
    {
        $this->fechaUltimaEdicion = $fechaUltimaEdicion;

        return $this;
    }

    /**
     * Get fechaUltimaEdicion
     *
     * @return \DateTime 
     */
    public function getFechaUltimaEdicion()
    {
        return $this->fechaUltimaEdicion;
    }

    /**
     * Set usuarioCreador
     *
     * @param \Twinpeaks\UserBundle\Entity\User $usuarioCreador
     * @return Muebles
     */
    public function setUsuarioCreador(\Twinpeaks\UserBundle\Entity\User $usuarioCreador = null)
    {
        $this->usuarioCreador = $usuarioCreador;

        return $this;
    }

    /**
     * Get usuarioCreador
     *
     * @return \Twinpeaks\UserBundle\Entity\User 
     */
    public function getUsuarioCreador()
    {
        return $this->usuarioCreador;
    }

    /**
     * Set UsuarioUltimaModificacion
     *
     * @param \Twinpeaks\UserBundle\Entity\User $usuarioUltimaModificacion
     * @return Muebles
     */
    public function setUsuarioUltimaModificacion(\Twinpeaks\UserBundle\Entity\User $usuarioUltimaModificacion = null)
    {
        $this->UsuarioUltimaModificacion = $usuarioUltimaModificacion;

        return $this;
    }

    /**
     * Get UsuarioUltimaModificacion
     *
     * @return \Twinpeaks\UserBundle\Entity\User 
     */
    public function getUsuarioUltimaModificacion()
    {
        return $this->UsuarioUltimaModificacion;
    }

      /**
     * @ORM\PrePersist
     */
    public function setCreatedValue()
    {
        $this->setFechaCreacion(new \DateTime());
        $this->setFechaUltimaEdicion(new \DateTime());
    }

    /**
     * @ORM\PreUpdate
     */
    public function setUpdateValue()
    {
        $this->setFechaUltimaEdicion(new \DateTime());
    }

    /**
     * Set estado
     *
     * @param boolean $estado
     * @return Muebles
     */
    public function setEstado($estado)
    {
        $this->estado = $estado;

        return $this;
    }

    /**
     * Get estado
     *
     * @return boolean 
     */
    public function getEstado()
    {
        return $this->estado;
    }
}
