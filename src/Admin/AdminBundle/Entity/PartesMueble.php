<?php

namespace Admin\AdminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * PartesMueble
 *
 * @ORM\Table()
 * @ORM\HasLifecycleCallbacks()
 * @ORM\Entity(repositoryClass="Admin\AdminBundle\Entity\PartesMuebleRepository")
 */
class PartesMueble
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\ManyToOne(targetEntity="Admin\AdminBundle\Entity\TipoParte")
     */
    private $tipoParte;

    /**
     * @var string
     *
     * @ORM\ManyToOne(targetEntity="Admin\AdminBundle\Entity\TipoLamina")
     */
    private $materialFrente; 
    
    /**
     * @var float
     *
     * @ORM\Column(name="anchofrente", type="float")
     */
    private $anchoFrente;

    /**
     * @var float
     *
     * @ORM\Column(name="altofrente", type="float")
     */
    private $altoFrente;
    
    /**
     * @var string
     *
     * @ORM\ManyToOne(targetEntity="Admin\AdminBundle\Entity\Fleje")
     */
    private $flejeFrente;

    /**
     * @var float
     *
     * @ORM\Column(name="cmFlejeFrente", type="float", nullable=true)
     */
    private $cmFlejeFrente;

    /**
     * @var string
     *
     * @ORM\ManyToOne(targetEntity="Admin\AdminBundle\Entity\TipoLamina")
     */
    private $materialLateral;

    /**
     * @var float
     *
     * @ORM\Column(name="ancholateral", type="float")
     */
    private $anchoLateral;
    
    
    /**
     * @var float
     *
     * @ORM\Column(name="altoLateral", type="float")
     */
    private $altoLateral;

    /**
     * @var string
     *
     * @ORM\ManyToOne(targetEntity="Admin\AdminBundle\Entity\Fleje")
     */
    private $flejeLateral;

    /**
     * @var float
     *
     * @ORM\Column(name="cmFlejeLateral", type="float", nullable=true)
     */
    private $cmFlejeLateral;

 /**
     * @var float
     *
     * @ORM\Column(name="valorFlejeLateral", type="float", nullable=true)
     */
    private $valorFlejeLateral;

     /**
     * @var float
     *
     * @ORM\Column(name="valorFlejeTrasera", type="float", nullable=true)
     */
    private $valorFlejeTrasera;

     /**
     * @var float
     *
     * @ORM\Column(name="valorFlejeFrente", type="float", nullable=true)
     */
    private $valorFlejeFrente;

      /**
     * @var string
     *
     * @ORM\ManyToOne(targetEntity="Admin\AdminBundle\Entity\TipoLamina")
     */
    private $materialTrasera;
    
    /**
     * @var float
     *
     * @ORM\Column(name="anchoTrasera", type="float")
     */
    private $anchoTrasera;
    
    /**
     * @var float
     *
     * @ORM\Column(name="altoTrasera", type="float")
     */
    private $altoTrasera;
    
    /**
     * @var string
     *
     * @ORM\ManyToOne(targetEntity="Admin\AdminBundle\Entity\Fleje")
     */
    private $flejeTrasera;

    /**
     * @var float
     *
     * @ORM\Column(name="cmFlejeTrasera", type="float", nullable=true)
     */
    private $cmFlejeTrasera;


      /**
     * @var string
     *
     * @ORM\ManyToOne(targetEntity="Admin\AdminBundle\Entity\TipoLamina")
     */
    private $materialFondo;
    
    /**
     * @var string
     *
     * @ORM\OneToMany(targetEntity="Admin\AdminBundle\Entity\accesoriosPartes", mappedBy="partesMueble", cascade={"persist","remove"})
     * @Assert\Valid()
     * @Assert\NotNull() 
     */
    private $accesorios;

    /**
     * @var float
     * @ORM\Column(name="valorAccesorios", type="float", nullable=true)
     */
    private $valorAccesorios;

    /**
     * @var string
     *
     * @ORM\Column(name="descripcion", type="text")
     */
    private $descripcion;
    
    /**
     * @var float
     *
     * @ORM\Column(name="valor", type="float")
     */
    private $valor;

    /**
     * @var float
     *
     * @ORM\Column(name="valormanoObra", type="float")
     */
    private $valorManoObra;

    /**
     * @var \DateTime
     * @ORM\Column(name="fechaCreacion", type="datetime")
     */
    private $fechaCreacion;

    /**
     * @var \DateTime
     * @ORM\Column(name="fechaEdicion", type="datetime")
     */
    private $fechaEdicion;

    /**
     * @var float
     *
     * @ORM\Column(name="cantidadLateral", type="float")
     */
    private $cantidadLateral;

        /**
     * @var float
     *
     * @ORM\Column(name="valorLateral", type="float")
     */
    private $valorLateral;

 /**
     * @var float
     *
     * @ORM\Column(name="cantidadFrente", type="float")
     */
    private $cantidadFrente;

        /**
     * @var float
     *
     * @ORM\Column(name="valorFrente", type="float")
     */
    private $valorFrente;


 /**
     * @var float
     *
     * @ORM\Column(name="cantidadTrasera", type="float")
     */
    private $cantidadTrasera;

        /**
     * @var float
     *
     * @ORM\Column(name="valorTrasera", type="float")
     */
    private $valorTrasera;

 /**
     * @var float
     *
     * @ORM\Column(name="cantidadFondo", type="float")
     */
    private $cantidadFondo;

        /**
     * @var float
     *
     * @ORM\Column(name="valorFondo", type="float")
     */
    private $valorFondo;

   /**
    * @var string
    *
    * @ORM\ManyToOne(targetEntity="Twinpeaks\UserBundle\Entity\User")
    */
    private $usuarioCreador;

    /**
    * @var string
    *
    * @ORM\ManyToOne(targetEntity="Twinpeaks\UserBundle\Entity\User")
    */
    private $UsuarioUltimaModificacion;


    /**
     * @var boolean
     *
     * @ORM\Column(name="isActive", type="boolean", nullable=true)
     */
    private $isActive = true;

    /**
     * Array's construcction for the Entity
     */
    public function __construct() 
    {
        $this->accesorios = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set tipoParte
     *
     * @param string $tipoParte
     * @return PartesMueble
     */
    public function setTipoParte(\Admin\AdminBundle\Entity\TipoParte $tipoParte)
    {
        $this->tipoParte = $tipoParte;

        return $this;
    }

    /**
     * Get tipoParte
     *
     * @return string 
     */
    public function getTipoParte()
    {
        return $this->tipoParte;
    }

    /**
     * Set anchoFrente
     *
     * @param float $anchoFrente
     * @return PartesMueble
     */
    public function setAnchoFrente($anchoFrente)
    {
        $this->anchoFrente = $anchoFrente;

        return $this;
    }

    /**
     * Get anchoFrente
     *
     * @return float 
     */
    public function getAnchoFrente()
    {
        return $this->anchoFrente;
    }

    /**
     * Set altoFrente
     *
     * @param float $altoFrente
     * @return PartesMueble
     */
    public function setAltoFrente($altoFrente)
    {
        $this->altoFrente = $altoFrente;

        return $this;
    }

    /**
     * Get altoFrente
     *
     * @return float 
     */
    public function getAltoFrente()
    {
        return $this->altoFrente;
    }

     /**
     * Set anchoLateral
     *
     * @param float $anchoLateral
     * @return PartesMueble
     */
    public function setAnchoLateral($anchoLateral)
    {
        $this->anchoLateral = $anchoLateral;

        return $this;
    }

    /**
     * Get anchoLateral
     *
     * @return float 
     */
    public function getAnchoLateral()
    {
        return $this->anchoLateral;
    }
    
    
    /**
     * Set altoLateral
     *
     * @param float $altoLateral
     * @return PartesMueble
     */
    public function setAltoLateral($altoLateral)
    {
        $this->altoLateral = $altoLateral;

        return $this;
    }

    /**
     * Get altoLateral
     *
     * @return float 
     */
    public function getAltoLateral()
    {
        return $this->altoLateral;
    }

        /**
     * Set altoTrasera
     *
     * @param float $altoTrasera
     * @return PartesMueble
     */
    public function setAltoTrasera($altoTrasera)
    {
        $this->altoTrasera = $altoTrasera;

        return $this;
    }

    /**
     * Get altoTrasera
     *
     * @return float 
     */
    public function getAltoTrasera()
    {
        return $this->altoTrasera;
    }
    
    /**
     * Set anchoTrasera
     *
     * @param float $altoTrasera
     * @return PartesMueble
     */
    public function setAnchoTrasera($anchoTrasera)
    {
        $this->anchoTrasera = $anchoTrasera;

        return $this;
    }

    /**
     * Get anchoTrasera
     *
     * @return float 
     */
    public function getAnchoTrasera()
    {
        return $this->anchoTrasera;
    }
    
    /**
     * Set accesorios
     * @return \Admin\AdminBundle\Entity\accesoriosPartes
     */
    public function setAccesorios(\Doctrine\Common\Collections\Collection $accesorios)
    {
        $this->accesorios = $accesorios;
        foreach ($accesorios as $accesorio){
            $accesorio->setPartesMueble($this);
        }
    }

    /**
     * get accesorios
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getAccesorios()
    {
        return $this->accesorios;
    }

    /**
     * Set descripcion
     *
     * @param string $descripcion
     * @return PartesMueble
     */
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    /**
     * Get descripcion
     *
     * @return string 
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }

    /**
     * Set fechaCreacion
     * @ORM\PrePersist
     * @param \DateTime $fechaCreacion
     * @return PartesMueble
     */
    public function setFechaCreacion()
    {
        $this->fechaCreacion = new \DateTime();

    }

    /**
     * Get fechaCreacion
     *
     * @return \DateTime 
     */
    public function getFechaCreacion()
    {
        return $this->fechaCreacion;
    }

    /**
     * @ORM\PrePersist
     * @ORM\PreUpdate
     * @return PartesMueble
     */
    public function setFechaEdicion()
    {
        $this->fechaEdicion = new \DateTime();
    }

    /**
     * Get fechaEdicion
     *
     * @return \DateTime 
     */
    public function getFechaEdicion()
    {
        return $this->fechaEdicion;
    }
    
     public function __toString() {
        return $this->getDescripcion();
    }

    /**
     * Set materialFrente
     *
     * @param \Admin\AdminBundle\Entity\TipoLamina $materialFrente
     * @return PartesMueble
     */
    public function setMaterialFrente(\Admin\AdminBundle\Entity\TipoLamina $materialFrente = null)
    {
        $this->materialFrente = $materialFrente;

        return $this;
    }

    /**
     * Get materialFrente
     *
     * @return \Admin\AdminBundle\Entity\TipoLamina 
     */
    public function getMaterialFrente()
    {
        return $this->materialFrente;
    }

    /**
     * Set materialLateral
     *
     * @param \Admin\AdminBundle\Entity\TipoLamina $materialLateral
     * @return PartesMueble
     */
    public function setMaterialLateral(\Admin\AdminBundle\Entity\TipoLamina $materialLateral = null)
    {
        $this->materialLateral = $materialLateral;

        return $this;
    }

    /**
     * Get materialLateral
     *
     * @return \Admin\AdminBundle\Entity\TipoLamina 
     */
    public function getMaterialLateral()
    {
        return $this->materialLateral;
    }

    /**
     * Set materialTrasera
     *
     * @param \Admin\AdminBundle\Entity\TipoLamina $materialTrasera
     * @return PartesMueble
     */
    public function setMaterialTrasera(\Admin\AdminBundle\Entity\TipoLamina $materialTrasera = null)
    {
        $this->materialTrasera = $materialTrasera;

        return $this;
    }

    /**
     * Get materialTrasera
     *
     * @return \Admin\AdminBundle\Entity\TipoLamina 
     */
    public function getMaterialTrasera()
    {
        return $this->materialTrasera;
    }

    /**
     * Set materialFondo
     *
     * @param \Admin\AdminBundle\Entity\TipoLamina $materialFondo
     * @return PartesMueble
     */
    public function setMaterialFondo(\Admin\AdminBundle\Entity\TipoLamina $materialFondo = null)
    {
        $this->materialFondo = $materialFondo;

        return $this;
    }

    /**
     * Get materialFondo
     *
     * @return \Admin\AdminBundle\Entity\TipoLamina 
     */
    public function getMaterialFondo()
    {
        return $this->materialFondo;
    }

   
 

     /**
     * Set cantidadLateral
     *
     * @param float $cantidadLateral
     * @return PartesMueble
     */
    public function setCantidadLateral($cantidadLateral)
    {
        $this->cantidadLateral = $cantidadLateral;

        return $this;
    }

    /**
     * Get cantidadLateral
     *
     * @return float 
     */
    public function getCantidadLateral()
    {
        return $this->cantidadLateral;
    }

       /**
     * Set valorLateral
     *
     * @param float $valorLateral
     * @return PartesMueble
     */
    public function setValorLateral($valorLateral)
    {
        $this->valorLateral = $valorLateral;

        return $this;
    }

    /**
     * Get valorLateral
     *
     * @return float 
     */
    public function getValorLateral()
    {
        return $this->valorLateral;
    }

      /**
     * Set cantidadTrasera
     *
     * @param float $cantidadTrasera
     * @return PartesMueble
     */
    public function setCantidadTrasera($cantidadTrasera)
    {
        $this->cantidadTrasera = $cantidadTrasera;

        return $this;
    }

    /**
     * Get cantidadTrasera
     *
     * @return float 
     */
    public function getCantidadTrasera()
    {
        return $this->cantidadTrasera;
    }

       /**
     * Set valorTrasera
     *
     * @param float $valorTrasera
     * @return PartesMueble
     */
    public function setValorTrasera($valorTrasera)
    {
        $this->valorTrasera = $valorTrasera;

        return $this;
    }

    /**
     * Get valorTrasera
     *
     * @return float 
     */
    public function getValorTrasera()
    {
        return $this->valorTrasera;
    }
    

        /**
     * Set cantidadFondo
     *
     * @param float $cantidadFondo
     * @return PartesMueble
     */
    public function setCantidadFondo($cantidadFondo)
    {
        $this->cantidadFondo = $cantidadFondo;

        return $this;
    }

    /**
     * Get cantidadFondo
     *
     * @return float 
     */
    public function getCantidadFondo()
    {
        return $this->cantidadFondo;
    }

       /**
     * Set valorFondo
     *
     * @param float $valorFondo
     * @return PartesMueble
     */
    public function setValorFondo($valorFondo)
    {
        $this->valorFondo = $valorFondo;

        return $this;
    }

    /**
     * Get valorFondo
     *
     * @return float 
     */
    public function getValorFondo()
    {
        return $this->valorFondo;
    }


        /**
     * Set cantidadFrente
     *
     * @param float $cantidadFrente
     * @return PartesMueble
     */
    public function setCantidadFrente($cantidadFrente)
    {
        $this->cantidadFrente = $cantidadFrente;

        return $this;
    }

    /**
     * Get cantidadFrente
     *
     * @return float 
     */
    public function getCantidadFrente()
    {
        return $this->cantidadFrente;
    }

       /**
     * Set valorFrente
     *
     * @param float $valorFrente
     * @return PartesMueble
     */
    public function setValorFrente($valorFrente)
    {
        $this->valorFrente = $valorFrente;

        return $this;
    }

    /**
     * Get valorFrente
     *
     * @return float 
     */
    public function getValorFrente()
    {
        return $this->valorFrente;
    }

    /**
    * @ORM\PrePersist
    * @ORM\preUpdate
    */
    public function calcularFlejes()
    {
        $cantidadFlejeFrente = $this->getAnchoFrente() + 2*$this->getAltoFrente();
        $cantidadFlejeLateral = 4 * $this->getAnchoLateral();
        $cantidadFlejeTrasera = $this->getAnchoTrasera();

        $this->setCmFlejeFrente($cantidadFlejeFrente);
        $this->setCmFlejeLateral($cantidadFlejeLateral);
        $this->setCmFlejeTrasera($cantidadFlejeTrasera);

    }



    /**
     * Get valor
     *
     * @return float 
     */
    public function getValor()
    {
        return $this->valor;
    }


    /**
     * Set valorManoObra
     *
     * @param float $valorManoObra
     * @return PartesMueble
     */
    public function setValorManoObra($valorManoObra)
    {
        $this->valorManoObra = $valorManoObra;

        return $this;
    }

    /**
     * Get valorManoObra
     *
     * @return float 
     */
    public function getValorManoObra()
    {
        return $this->valorManoObra;
    }


    /**
     * Set cmFlejeFrente
     *
     * @param float $cmFlejeFrente
     * @return PartesMueble
     */
    public function setCmFlejeFrente($cmFlejeFrente)
    {
        $this->cmFlejeFrente = $cmFlejeFrente;

        return $this;
    }

    /**
     * Get cmFlejeFrente
     *
     * @return float 
     */
    public function getCmFlejeFrente()
    {
        return $this->cmFlejeFrente;
    }

    /**
     * Set cmFlejeLateral
     *
     * @param float $cmFlejeLateral
     * @return PartesMueble
     */
    public function setCmFlejeLateral($cmFlejeLateral)
    {
        $this->cmFlejeLateral = $cmFlejeLateral;

        return $this;
    }

    /**
     * Get cmFlejeLateral
     *
     * @return float 
     */
    public function getCmFlejeLateral()
    {
        return $this->cmFlejeLateral;
    }

    /**
     * Set cmFlejeTrasera
     *
     * @param float $cmFlejeTrasera
     * @return PartesMueble
     */
    public function setCmFlejeTrasera($cmFlejeTrasera)
    {
        $this->cmFlejeTrasera = $cmFlejeTrasera;

        return $this;
    }

    /**
     * Get cmFlejeTrasera
     *
     * @return float 
     */
    public function getCmFlejeTrasera()
    {
        return $this->cmFlejeTrasera;
    }

    /**
     * Set flejeFrente
     *
     * @param \Admin\AdminBundle\Entity\Fleje $flejeFrente
     * @return PartesMueble
     */
    public function setFlejeFrente(\Admin\AdminBundle\Entity\Fleje $flejeFrente = null)
    {
        $this->flejeFrente = $flejeFrente;

        return $this;
    }

    /**
     * Get flejeFrente
     *
     * @return \Admin\AdminBundle\Entity\Fleje 
     */
    public function getFlejeFrente()
    {
        return $this->flejeFrente;
    }

    /**
     * Set flejeLateral
     *
     * @param \Admin\AdminBundle\Entity\Fleje $flejeLateral
     * @return PartesMueble
     */
    public function setFlejeLateral(\Admin\AdminBundle\Entity\Fleje $flejeLateral = null)
    {
        $this->flejeLateral = $flejeLateral;

        return $this;
    }

    /**
     * Get flejeLateral
     *
     * @return \Admin\AdminBundle\Entity\Fleje 
     */
    public function getFlejeLateral()
    {
        return $this->flejeLateral;
    }

    /**
     * Set flejeTrasera
     *
     * @param \Admin\AdminBundle\Entity\Fleje $flejeTrasera
     * @return PartesMueble
     */
    public function setFlejeTrasera(\Admin\AdminBundle\Entity\Fleje $flejeTrasera = null)
    {
        $this->flejeTrasera = $flejeTrasera;

        return $this;
    }

    /**
     * Get flejeTrasera
     *
     * @return \Admin\AdminBundle\Entity\Fleje 
     */
    public function getFlejeTrasera()
    {
        return $this->flejeTrasera;
    }

    /**
     * Set valorAccesorios
     * @param float $valorAccesorios
     * @return PartesMueble
     */
    public function setValorAccesorios($valorAccesorios)
    {
       
        $this->valorAccesorios = $valorAccesorios;

        return $this;
    }

    /**
     * Get valorAccesorios
     *
     * @return float 
     */
    public function getValorAccesorios()
    {
        return $this->valorAccesorios;
    }

    /**
     * Set valorFlejeLateral
     *
     * @param float $valorFlejeLateral
     * @return PartesMueble
     */
    public function setValorFlejeLateral($valorFlejeLateral)
    {
        $this->valorFlejeLateral = $valorFlejeLateral;

        return $this;
    }

    /**
     * Get valorFlejeLateral
     *
     * @return float 
     */
    public function getValorFlejeLateral()
    {
        return $this->valorFlejeLateral;
    }

    /**
     * Set valorFlejeTrasera
     *
     * @param float $valorFlejeTrasera
     * @return PartesMueble
     */
    public function setValorFlejeTrasera($valorFlejeTrasera)
    {
        $this->valorFlejeTrasera = $valorFlejeTrasera;

        return $this;
    }

    /**
     * Get valorFlejeTrasera
     *
     * @return float 
     */
    public function getValorFlejeTrasera()
    {
        return $this->valorFlejeTrasera;
    }

    /**
     * Set valorFlejeFrente
     *
     * @param float $valorFlejeFrente
     * @return PartesMueble
     */
    public function setValorFlejeFrente($valorFlejeFrente)
    {
        $this->valorFlejeFrente = $valorFlejeFrente;

        return $this;
    }

    /**
     * Get valorFlejeFrente
     *
     * @return float 
     */
    public function getValorFlejeFrente()
    {
        return $this->valorFlejeFrente;
    }

       /**
     * Set valor
     * @ORM\PrePersist
     * @ORM\preUpdate
     * @param float $valor
     * @return PartesMueble
     */
    public function setValor()
    {
        $cantidadFlejeFrente = $this->getAnchoFrente() + 2*$this->getAltoFrente();
        $cantidadFlejeLateral = 4 * $this->getAnchoLateral();
        $cantidadFlejeTrasera = $this->getAnchoTrasera();

        $valorFlejeFrente = $cantidadFlejeFrente * $this->getFlejeFrente()->getValorMetrosLineal()/100;
        $valorFlejeLateral = $cantidadFlejeLateral * $this->getFlejeLateral()->getValorMetrosLineal()/100;
        $valorFlejeTrasera = $cantidadFlejeTrasera * $this->getFlejeTrasera()->getValorMetrosLineal()/100;

        $this->setValorFlejeLateral($valorFlejeLateral);
        $this->setValorFlejeFrente($valorFlejeFrente);
        $this->setValorFlejeTrasera($valorFlejeTrasera);

/////////////////// calculo del frente ///////////////////////////////////////////////////////////////
        
        $anchoMaterialFrente = $this->getMaterialFrente()->getAncho();
        $altoMaterialFrente  = $this->getMaterialFrente()->getAlto();



        $relacionAnchoMaterialAnchoFrente = (int) ($anchoMaterialFrente / $this->getAnchoFrente());
        $relacionAltoMaterialAltoFrente = (int) ($altoMaterialFrente / $this->getAltoFrente());

        $productoFrente1 = $relacionAnchoMaterialAnchoFrente * $relacionAltoMaterialAltoFrente;

        $relacionAnchoMaterialAltoFrente = (int) ($anchoMaterialFrente / $this->getAltoFrente());
        $relacionAltoMaterialAnchoFrente = (int) ($altoMaterialFrente / $this->getAnchoFrente());

        $productoFrente2 = $relacionAnchoMaterialAltoFrente * $relacionAltoMaterialAnchoFrente;

        $areaFrente  = $this->getAnchoFrente() * $this->getAltoFrente();


        $valorcmfrente = $this->getMaterialFrente()->getValorCm();
        $areaFrenteReal = $this->getAnchoFrente() * $this->getAltoFrente();
        $valorFrenteReal = $valorcmfrente * $areaFrenteReal; 
        $this->setValorFrente($valorFrenteReal);


        if($productoFrente1 > $productoFrente2){

            $this->setCantidadFrente($productoFrente1);
            $areaMaterialFrente = $productoFrente1*$areaFrente;
            $valorMaterialFrente = $this->getMaterialFrente()->getValor();
            $valorCmMaterialFrente = $valorMaterialFrente / $areaMaterialFrente;

            $valorFrente = $areaFrente * $valorCmMaterialFrente;
            //$this->setValorFrente($valorFrente);

        }else{

            $this->setCantidadFrente($productoFrente2);
            $areaMaterialFrente = $productoFrente2*$areaFrente;
            $valorMaterialFrente = $this->getMaterialFrente()->getValor();
            $valorCmMaterialFrente = $valorMaterialFrente / $areaMaterialFrente;

            $valorFrente = $areaFrente * $valorCmMaterialFrente;
            //$this->setValorFrente($valorFrente);

        }
///////////////////////////// fin calculo frente /////////////////////////////////////////////////////
        
/////////////////// Calculo del Lateral ///////////////////////////////////////////////////////////////
        
        $anchoMaterialLateral = $this->getMaterialLateral()->getAncho();
        $altoMaterialLateral  = $this->getMaterialLateral()->getAlto();
        $anchoLateralCalculo  = $this->getAnchoLateral(); 
        $altoLateralCalculo   = $this->getAltoLateral();

        $areaLateral1  = $this->getAnchoLateral() * $this->getAltoLateral();
        $areaLateral = $areaLateral1;
        if(  $anchoLateralCalculo == 0 || $altoLateralCalculo == 0 ){
            $valorLateral =0;
            $this->setValorLateral(0);
        }else{
             $relacionAnchoMaterialAnchoLateral = (int) ($anchoMaterialLateral / $anchoLateralCalculo);
        $relacionAltoMaterialAltoLateral = (int) ($altoMaterialLateral / $altoLateralCalculo );

        $productoLateral1 = $relacionAnchoMaterialAnchoLateral * $relacionAltoMaterialAltoLateral;

        $relacionAnchoMaterialAltoLateral = (int) ($anchoMaterialLateral / $altoLateralCalculo );
        $relacionAltoMaterialAnchoLateral = (int) ($altoMaterialLateral / $anchoLateralCalculo);

        $productoLateral2 = $relacionAnchoMaterialAltoLateral* $relacionAltoMaterialAnchoLateral;


        $valorcmlateral = $this->getMaterialLateral()->getValorCm();
        $arealateralReal = $this->getAnchoLateral() * $this->getAltoLateral();
        $valorLateralReal = $valorcmlateral * $arealateralReal; 
        $this->setValorLateral(2*$valorLateralReal);

        if($productoLateral1 > $productoLateral2){
            $this->setCantidadLateral($productoLateral1);
            $areaMaterialLateral = $productoLateral1*$areaLateral;
            $valorMaterialLateral= $this->getMaterialLateral()->getValor();
            $valorCmMaterialLateral = $valorMaterialLateral / $areaMaterialLateral;

            //$valorLateral = $areaLateral* $valorCmMaterialLateral;
            
        }else{
            $this->setCantidadLateral($productoLateral2);
            $areaMaterialLateral = $productoLateral2*$areaLateral;
            $valorMaterialLateral = $this->getMaterialLateral()->getValor();
            $valorCmMaterialLateral = $valorMaterialLateral / $areaMaterialLateral;

            $valorLateral= $areaLateral * $valorCmMaterialLateral;
            //$this->setValorLateral($valorLateral);

        }
        }
      
       
///////////////////////////// Fin calculo Lateral /////////////////////////////////////////////////////
 
       // $areaTrasera = $this->getAnchoTrasera() * $this->getAltoTrasera();
       // $valorTrasera = $areaLateral * $this->getMaterialTrasera()->getValorCm();

/////////////////// Calculo del Trasera ///////////////////////////////////////////////////////////////

    if($this->getAnchoTrasera() == 0 || $this->getAltoTrasera() == 0){
        $valorTrasera =0;
        $this->setValorTrasera(0);
    }else{        
        $anchoMaterialTrasera = $this->getMaterialTrasera()->getAncho();
        $altoMaterialTrasera  = $this->getMaterialTrasera()->getAlto();


        $relacionAnchoMaterialAnchoTrasera = (int) ($anchoMaterialTrasera / $this->getAnchoTrasera());
        $relacionAltoMaterialAltoTrasera = (int) ($altoMaterialTrasera / $this->getAltoTrasera());

        $productoTrasera1 = $relacionAnchoMaterialAnchoTrasera * $relacionAltoMaterialAltoTrasera;

        $relacionAnchoMaterialAltoTrasera = (int) ($anchoMaterialTrasera / $this->getAltoTrasera());
        $relacionAltoMaterialAnchoTrasera = (int) ($altoMaterialTrasera/ $this->getAnchoTrasera());

        $productoTrasera2 = $relacionAnchoMaterialAltoTrasera* $relacionAltoMaterialAnchoTrasera;

        $areaTrasera  = $this->getAnchoTrasera() * $this->getAltoTrasera();

        $valorcmTrasera = $this->getMaterialTrasera()->getValorCm();
        $areaTraseraReal = $areaTrasera;
        $valorTraseraReal = $valorcmTrasera * $areaTraseraReal; 
        $this->setValorTrasera($valorTraseraReal);

        if($productoTrasera1 > $productoTrasera2){
            $this->setCantidadTrasera($productoTrasera1);
            $areaMaterialTrasera = $productoTrasera1*$areaTrasera;
            $valorMaterialTrasera = $this->getMaterialTrasera()->getValor();
            $valorCmMaterialTrasera = $valorMaterialTrasera / $areaMaterialLateral;

            $valorTrasera = $areaTrasera * $valorCmMaterialTrasera;
            //$this->setValorTrasera($valorTrasera);
        }else{
            $this->setCantidadTrasera($productoTrasera2);
            $areaMaterialTrasera = $productoTrasera2*$areaTrasera;
            $valorMaterialTrasera = $this->getMaterialTrasera()->getValor();
            $valorCmMaterialTrasera = $valorMaterialTrasera / $areaMaterialTrasera;

            $valorTrasera= $areaTrasera * $valorCmMaterialTrasera;
            //$this->setValorTrasera($valorTrasera);

        }
    }
///////////////////////////// Fin calculo Trasera /////////////////////////////////////////////////////

        $perimetroFondo = $this->getAnchoFrente()+  $this->getAnchoLateral()*2 + $this->getAnchoTrasera();
/////////////////// Calculo del Fondo ///////////////////////////////////////////////////////////////
        
        $anchoMaterialFondo= $this->getMaterialFondo()->getAncho();
        $altoMaterialFondo = $this->getMaterialFondo()->getAlto();

        $altoFondo = $this->getAnchoLateral();
        $anchoFondo = $this->getAnchoTrasera();

     if($altoFondo == 0 || $anchoFondo == 0){
        $valorFondo =0;
        $this->setValorFondo(0);
     }else{
        $relacionAnchoMaterialAnchoFondo = (int) ($anchoMaterialFondo / $anchoFondo);
        $relacionAltoMaterialAltoFondo = (int) ($altoMaterialFondo / $altoFondo);

        $productoFondo1 = $relacionAnchoMaterialAnchoFondo * $relacionAltoMaterialAltoFondo;

        $relacionAnchoMaterialAltoFondo = (int) ($anchoMaterialFondo / $altoFondo);
        $relacionAltoMaterialAnchoFondo = (int) ($altoMaterialFondo/ $anchoFondo);

        $productoFondo2 = $relacionAnchoMaterialAltoFondo* $relacionAltoMaterialAnchoFondo;

        $areaFondo  = $altoFondo * $anchoFondo;

       $valorcmFondo = $this->getMaterialFondo()->getValorCm();
        $areaFondoReal = $areaFondo;
        $valorFondoReal = $valorcmFondo * $areaFondoReal; 
        $this->setValorFondo($valorFondoReal);

        if($productoFondo1 > $productoFondo2){
            $this->setCantidadFondo($productoFondo1);
            $areaMaterialFondo = $productoFondo1*$areaFondo;
            $valorMaterialFondo = $this->getMaterialFondo()->getValor();
            $valorCmMaterialFondo = $valorMaterialFondo / $areaMaterialFondo;

            $valorFondo = $areaFondo * $valorCmMaterialFondo;
            //$this->setValorFondo($valorFondo);
        }else{
            $this->setCantidadFondo($productoFondo2);
            $areaMaterialFondo = $productoFondo2*$areaFondo;
            $valorMaterialFondo = $this->getMaterialFondo()->getValor();
            $valorCmMaterialFondo = 0;
            if($areaMaterialFondo != 0){
               $valorCmMaterialFondo = $valorMaterialFondo / $areaMaterialFondo; 
            }
            $valorFondo= $areaFondo * $valorCmMaterialFondo;
           // $this->setValorFondo($valorFondo);

        }
    }
///////////////////////////// Fin calculo Fondo /////////////////////////////////////////////////////
        $valorAccesorios = $this->getValorAccesorios();
     
        $valormanoObra = $this->getValorManoObra();
        $valortotal = $valormanoObra+$valorFrenteReal+ (2*$valorLateralReal)+$valorTraseraReal+$valorFondoReal+$valorAccesorios + $valorFlejeFrente + $valorFlejeLateral + $valorFlejeTrasera;
        
        
        $this->valor = $valortotal;
        
        return $this;
    }


    /**
     * Set isActive
     *
     * @param boolean $isActive
     * @return PartesMueble
     */
    public function setIsActive($isActive)
    {
        $this->isActive = $isActive;

        return $this;
    }

    /**
     * Get isActive
     *
     * @return boolean 
     */
    public function getIsActive()
    {
        return $this->isActive;
    }

    /**
     * Add accesorios
     *
     * @param \Admin\AdminBundle\Entity\accesoriosPartes $accesorios
     * @return PartesMueble
     */
    public function addAccesorio(\Admin\AdminBundle\Entity\accesoriosPartes $accesorios)
    {
        $this->accesorios[] = $accesorios;

        return $this;
    }

    /**
     * Remove accesorios
     *
     * @param \Admin\AdminBundle\Entity\accesoriosPartes $accesorios
     */
    public function removeAccesorio(\Admin\AdminBundle\Entity\accesoriosPartes $accesorios)
    {
        $this->accesorios->removeElement($accesorios);
    }

    /**
     * Set usuarioCreador
     *
     * @param \Twinpeaks\UserBundle\Entity\User $usuarioCreador
     * @return PartesMueble
     */
    public function setUsuarioCreador(\Twinpeaks\UserBundle\Entity\User $usuarioCreador = null)
    {
        $this->usuarioCreador = $usuarioCreador;

        return $this;
    }

    /**
     * Get usuarioCreador
     *
     * @return \Twinpeaks\UserBundle\Entity\User 
     */
    public function getUsuarioCreador()
    {
        return $this->usuarioCreador;
    }

    /**
     * Set UsuarioUltimaModificacion
     *
     * @param \Twinpeaks\UserBundle\Entity\User $usuarioUltimaModificacion
     * @return PartesMueble
     */
    public function setUsuarioUltimaModificacion(\Twinpeaks\UserBundle\Entity\User $usuarioUltimaModificacion = null)
    {
        $this->UsuarioUltimaModificacion = $usuarioUltimaModificacion;

        return $this;
    }

    /**
     * Get UsuarioUltimaModificacion
     *
     * @return \Twinpeaks\UserBundle\Entity\User 
     */
    public function getUsuarioUltimaModificacion()
    {
        return $this->UsuarioUltimaModificacion;
    }

      /**
     * @ORM\PrePersist
     */
    public function setCreatedValue()
    {
        $this->setFechaCreacion(new \DateTime());
        $this->setFechaEdicion(new \DateTime());
    }

    /**
     * @ORM\PreUpdate
     */
    public function setUpdateValue()
    {
        $this->setFechaEdicion(new \DateTime());
    }
}
