<?php

namespace Admin\AdminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * TipoParte
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Admin\AdminBundle\Entity\TipoParteRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class TipoParte
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="ref", type="string", length=255)
     */
    private $ref;

    /**
     * @var string
     *
     * @ORM\Column(name="descripcion", type="text")
     */
    private $descripcion;

    /**
     * @var float
     *
     * @ORM\Column(name="valor", type="float")
     */
    private $valor;

  /**
     * @var boolean
     *
     * @ORM\Column(name="frente", type="boolean", nullable=true)
     */
    private $frente = true;

  /**
     * @var boolean
     *
     * @ORM\Column(name="trasera", type="boolean", nullable=true)
     */
    private $trasera = false;

  /**
     * @var boolean
     *
     * @ORM\Column(name="lateral", type="boolean", nullable=true)
     */
    private $lateral = false;

  /**
     * @var boolean
     *
     * @ORM\Column(name="fondo", type="boolean", nullable=true)
     */
    private $fondo = false;

      /**
     * @var boolean
     *
     * @ORM\Column(name="tapa", type="boolean", nullable=true)
     */
    private $tapa = false;

  /**
    * @var datetime
    * @ORM\Column(name="fechacreacion", type="datetime", nullable=true)
    */
    private $fechaCreacion;

    /**
    * @var string
    *
    * @ORM\ManyToOne(targetEntity="Twinpeaks\UserBundle\Entity\User")
    */
    private $usuarioCreador;

    /**
    * @var string
    *
    * @ORM\ManyToOne(targetEntity="Twinpeaks\UserBundle\Entity\User")
    */
    private $UsuarioUltimaModificacion;

   /**
    * @var datetime
    * @ORM\Column(name="fechaultimaedicion", type="datetime", nullable=true)
    */
    private $fechaUltimaEdicion;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set ref
     *
     * @param string $ref
     * @return TipoParte
     */
    public function setRef($ref)
    {
        $this->ref = $ref;

        return $this;
    }

    /**
     * Get ref
     *
     * @return string 
     */
    public function getRef()
    {
        return $this->ref;
    }

    /**
     * Set descripcion
     *
     * @param string $descripcion
     * @return TipoParte
     */
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    /**
     * Get descripcion
     *
     * @return string 
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }
    
     public function __toString() {
        return $this->getRef()." ".$this->getDescripcion();
    }

    /**
     * Set valor
     *
     * @param float $valor
     * @return TipoParte
     */
    public function setValor($valor)
    {
        $this->valor = $valor;

        return $this;
    }

    /**
     * Get valor
     *
     * @return float 
     */
    public function getValor()
    {
        return $this->valor;
    }

    /**
     * Set frente
     *
     * @param boolean $frente
     * @return TipoParte
     */
    public function setFrente($frente)
    {
        $this->frente = $frente;

        return $this;
    }

    /**
     * Get frente
     *
     * @return boolean 
     */
    public function getFrente()
    {
        return $this->frente;
    }

    /**
     * Set trasera
     *
     * @param boolean $trasera
     * @return TipoParte
     */
    public function setTrasera($trasera)
    {
        $this->trasera = $trasera;

        return $this;
    }

    /**
     * Get trasera
     *
     * @return boolean 
     */
    public function getTrasera()
    {
        return $this->trasera;
    }

    /**
     * Set lateral
     *
     * @param boolean $lateral
     * @return TipoParte
     */
    public function setLateral($lateral)
    {
        $this->lateral = $lateral;

        return $this;
    }

    /**
     * Get lateral
     *
     * @return boolean 
     */
    public function getLateral()
    {
        return $this->lateral;
    }

    /**
     * Set fondo
     *
     * @param boolean $fondo
     * @return TipoParte
     */
    public function setFondo($fondo)
    {
        $this->fondo = $fondo;

        return $this;
    }

    /**
     * Get fondo
     *
     * @return boolean 
     */
    public function getFondo()
    {
        return $this->fondo;
    }

    /**
     * Set tapa
     *
     * @param boolean $tapa
     * @return TipoParte
     */
    public function setTapa($tapa)
    {
        $this->tapa = $tapa;

        return $this;
    }

    /**
     * Get tapa
     *
     * @return boolean 
     */
    public function getTapa()
    {
        return $this->tapa;
    }

    /**
     * Set fechaCreacion
     *
     * @param \DateTime $fechaCreacion
     * @return TipoParte
     */
    public function setFechaCreacion($fechaCreacion)
    {
        $this->fechaCreacion = $fechaCreacion;

        return $this;
    }

    /**
     * Get fechaCreacion
     *
     * @return \DateTime 
     */
    public function getFechaCreacion()
    {
        return $this->fechaCreacion;
    }

    /**
     * Set fechaUltimaEdicion
     *
     * @param \DateTime $fechaUltimaEdicion
     * @return TipoParte
     */
    public function setFechaUltimaEdicion($fechaUltimaEdicion)
    {
        $this->fechaUltimaEdicion = $fechaUltimaEdicion;

        return $this;
    }

    /**
     * Get fechaUltimaEdicion
     *
     * @return \DateTime 
     */
    public function getFechaUltimaEdicion()
    {
        return $this->fechaUltimaEdicion;
    }

    /**
     * Set usuarioCreador
     *
     * @param \Twinpeaks\UserBundle\Entity\User $usuarioCreador
     * @return TipoParte
     */
    public function setUsuarioCreador(\Twinpeaks\UserBundle\Entity\User $usuarioCreador = null)
    {
        $this->usuarioCreador = $usuarioCreador;

        return $this;
    }

    /**
     * Get usuarioCreador
     *
     * @return \Twinpeaks\UserBundle\Entity\User 
     */
    public function getUsuarioCreador()
    {
        return $this->usuarioCreador;
    }

    /**
     * Set UsuarioUltimaModificacion
     *
     * @param \Twinpeaks\UserBundle\Entity\User $usuarioUltimaModificacion
     * @return TipoParte
     */
    public function setUsuarioUltimaModificacion(\Twinpeaks\UserBundle\Entity\User $usuarioUltimaModificacion = null)
    {
        $this->UsuarioUltimaModificacion = $usuarioUltimaModificacion;

        return $this;
    }

    /**
     * Get UsuarioUltimaModificacion
     *
     * @return \Twinpeaks\UserBundle\Entity\User 
     */
    public function getUsuarioUltimaModificacion()
    {
        return $this->UsuarioUltimaModificacion;
    }

      /**
     * @ORM\PrePersist
     */
    public function setCreatedValue()
    {
        $this->setFechaCreacion(new \DateTime());
        $this->setFechaUltimaEdicion(new \DateTime());
    }

    /**
     * @ORM\PreUpdate
     */
    public function setUpdateValue()
    {
        $this->setFechaUltimaEdicion(new \DateTime());
    }
}
