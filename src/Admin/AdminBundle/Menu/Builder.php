<?php
// src/Acme/DemoBundle/Menu/Builder.php
namespace Admin\AdminBundle\Menu;

use Knp\Menu\FactoryInterface;
use Symfony\Component\DependencyInjection\ContainerAware;

class Builder extends ContainerAware
{
    public function mainMenu(FactoryInterface $factory, array $options)
    {
        $menu = $factory->createItem('root');
        $menu->setChildrenAttribute('class', 'nav  nav-list');

        $menu->addChild('Inicio', array('route' => 'admin_homepage'))->setAttribute('icon', 'fa fa-dashboard');
        $menu->addChild('Usuarios', array('route' => 'tp_user' ))->setAttribute('icon', 'fa fa-user')->setAttribute('span', 'hidden-minibar');
        $menu->addChild('Tipo de Mueble', array('route' => 'tipomueble' ))->setAttribute('icon', 'fa fa-tasks')->setAttribute('span', 'hidden-minibar');
        $menu->addChild('Muebles', array('route' => 'muebles' ))->setAttribute('icon', 'fa fa-file-o')->setAttribute('span', 'hidden-minibar');
        //$menu->addChild('Crear Muebles', array('route' => 'creacion_muebles' ))->setAttribute('icon', 'fa fa-plus-square')->setAttribute('span', 'hidden-minibar');
        $menu->addChild('Accesorios', array('route' => 'accesorios' ))->setAttribute('icon', 'fa fa-truck')->setAttribute('span', 'hidden-minibar');
        $menu->addChild('Tipos de partes', array('route' => 'tipoparte' ))->setAttribute('icon', 'fa fa-tags')->setAttribute('span', 'hidden-minibar');
        $menu->addChild('Partes mueble', array('route' => 'partesmueble' ))->setAttribute('icon', 'fa fa-user')->setAttribute('span', 'hidden-minibar');
        $menu->addChild('Tipo Lamina', array('route' => 'tipolamina' ))->setAttribute('icon', 'fa fa-rss')->setAttribute('span', 'hidden-minibar');
        $menu->addChild('Tipos de flejes', array('route' => 'fleje' ))->setAttribute('icon', 'fa fa-info-circle')->setAttribute('span', 'hidden-minibar');
        $menu->addChild('Clientes', array('route' => 'clientes' ))->setAttribute('icon', 'fa fa-group')->setAttribute('span', 'hidden-minibar');
        $menu->addChild('Configuraciones', array('route' => 'admin_configuraciones' ))->setAttribute('icon', 'fa fa-cogs')->setAttribute('span', 'hidden-minibar');
        $menu->addChild('Soporte', array('route' => 'soporte' ))->setAttribute('icon', 'fa fa-comments-o')->setAttribute('span', 'hidden-minibar');
        // ... add more children

        return $menu;
    }

     public function superMenu(FactoryInterface $factory, array $options)
    {
        $menu = $factory->createItem('root');
        $menu->setChildrenAttribute('class', 'nav  nav-list');

        $menu->addChild('Super Crud', array('route' => 'admin' ))->setAttribute('icon', 'fa fa-desktop')->setAttribute('span', 'hidden-minibar');
     
        return $menu;
    }
}